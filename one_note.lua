-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "One_Note"

require("scripts/one_note") ; require("scripts/lilypond_creation_tables")

local pitch_table = {} ; local frequency_table = {} ; local velocity_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {}

local midi_ins_number = 0

-- local command = "" -- full interactive mode
-- local command = "r" -- random, non-interactive
local command = "Pythagoras"

local generate_log = "yes"

local generate_lilypond = "no"
local generate_feldman = "no"
local generate_pattern_15 = "no"
local generate_pattern_35 = "no" ; local enforce_nine = "no"
local wave = "no" ; local flac = "no" ; local ogg = "no" ; local ogg_tag = "yes"
local generate_one_note_score = "yes"

local command_line_options = {...}
command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac = get_args(command_line_options,command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac)

create_log(generate_log)
header.user_data,header.dedication = get_input(command)

local number_of_notes = 1000000
local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"  
local instrument = "Piano"
local algorithm = header.algorithm
local style = "" -- 
local style_algorithm_author = "David Bellows"

local key = "c-Panchromatic"
local tuning_name = "c-100000/EDO"--"c-Western Standard"
local system = "western iso"  
header.reference_pitch,header.reference_pitch_name = reference_pitch(system)

local tempo_word = "andante" 
local tempo_number = tempo_value(tempo_word) 

do -- Generate audio frequencies
   audio_freq_table,ratio_table,header.tuning_octave,low_freq,high_freq = generate_audio_frequency_table(tuning_name)
end

do -- Calculate degrees for tuning
   calculated_scale_degrees = generate_relative_scale_degrees(ratio_table,key)
end

local scale_octave = get_scale_octave(key,calculated_scale_degrees) 
midi_ins_number,header.lowrange,header.highrange = instrument_range(instrument,audio_freq_table,low_freq,high_freq) 

do -- Pitch Table
   local base_table = {}
   base_table = preparse_pitches(audio_freq_table,calculated_scale_degrees, key, scale_octave)
   pitch_table = quantize("pitch_table",base_table,number_of_notes)
end

do -- Velocity Table
   
   local velocity_recipe = "all"
   
   if string.sub(velocity_recipe,1,1) == "#" then
      custom_velocity = "yes" 
      velocity_recipe = string.sub(velocity_recipe,2) 
   end       
   
   local base_table = {}
   base_table = preparse_velocities(velocity_recipe)
   
   velocity_table = quantize("velocity_table",base_table,number_of_notes)
end

do -- Duration Table
   local duration_recipe = "all"
   
   local base_table = {}
   base_table = preparse_durations(duration_recipe)
   
   duration_table = quantize("duration_table",base_table,number_of_notes)
end

pitch_table,duration_table,velocity_table,score,latex_name = one_note_om(pitch_table,duration_table,velocity_table,instrument,number_of_notes,tuning_name)
number_of_notes = 1

for counter = 1,number_of_notes do 
   frequency_table[counter] = audio_freq_table[pitch_table[counter]]
   csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440)
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table = generate_start_table(duration_table)

do -- Create Csound
   do -- Csound preamble
      csound_preamble = create_csound_preamble(algorithm,style,style_algorithm_author,time_of_creation)
      if sound_font ~= "" then
	 csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
      end	     
   end

   local start_of_table = 1
   ins_num_table[1] = 1
   
   do -- create specific Orchestra instrument
      orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)
   end
   
   csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, number_of_notes, ins_num_table, ins_name_table, velocity_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

   csound_score_ins = finish_csound_score_ins(csound_score_ins)

   csound_orchestra = csound_orchestra1 

   csound_score = csound_score_function1 .. csound_score_function1 .. "\n" .. csound_score_ins
   
   local orchestra_finish = create_finish_orchestra(tempo_number)
   csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
   csound_file_name = create_csound_file(csound_file,algorithm,style,instrument)
end

do -- Create Audio
   create_csound_audio_file(csound_file_name,style,style_algorithm_author,wave,flac,ogg,ogg_tag)
end

if generate_one_note_score == "yes" then
   local latex_name = (instrument.."_One_Note_Score")
   create_latex(score,latex_name)
end

if generate_lilypond == "yes" then -- Generate Lilypond score
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local adapter = style_algorithm_author ; local pme = "Platonic Music Engine" ; local original_composer = ""
   local original_title = "" 

   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local use_ragged_right = "no" ; local no_indent = "no"
   local rest_or_space = "s" local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"
   
   local header = lilypond2header(instrument,tuning_name,adapter,pme,original_composer,original_title,additional_info,algorithm,header.reference_pitch_name)

   local lily_note_table_1,lily_note_table_3,lily_key = generate_lilypond_notes_from_frequencies(instrument,number_of_notes,tuning_name,key,calculated_scale_degrees,pitch_table,frequency_table,duration_table,velocity_table,rest_or_space,avoid_collisions)

   local lilypond_score,score_info = lilypond2variables(instrument,lily_key,time,tempo_word,tempo_number,lily_note_table_1,lily_note_table_3)
   
   local footer = lilypond2footer(score_info,instrument,voices,lyrics,use_ragged_right,no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(instrument,complete_lilypond_score,algorithm)
end

if generate_feldman == "yes" then -- Feldman Graphic notation
   require("scripts/feldman_notation")
   local volume = "yes" 
   
   local feldman_score,latexname = midi2feldman(pitch_table,duration_table,velocity_table,instrument,number_of_notes,volume,tuning_name,system,tempo_number)
   create_latex(feldman_score,latexname)
end

if generate_pattern_15 == "yes" then -- Pattern 15 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_15") 
   
   local midinotes = shallowcopy(platonic_future) ; local mididuration = shallowcopy(platonic_duration)

   pattern_15_lilypond_score,latexname = 
      midi2pattern_15(pitch_table,duration_table,instrument,number_of_notes)

   create_latex(pattern_15_lilypond_score,latexname)
end

if generate_pattern_35 == "yes" then -- Pattern 35 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_35")

   pattern_35_score,latexname = midi2pattern_35(pitch_table,duration_table,velocity_table,number_of_notes,instrument)
   
   create_latex(pattern_35_score,latexname)
end


