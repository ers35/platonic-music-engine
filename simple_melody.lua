-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Simple Melody"

-- Declare some variables and tables. No need to edit anything here
local pitch_table = {} ; local frequency_table = {} ; local velocity_table = {} ; local amplitude_table = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {} ; local start_table = {}
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {} 

local midi_ins_number = 0

-- You can start editing things here. See the Tutorial or Reference Manual if something doesn't make sense.

local command = "" -- full interactive mode
-- local command = "r" -- random, non-interactive
-- local command = "Pythagoras" -- specific string, non-interactive

local generate_log = "yes" -- "yes" or "no"

-- Change these to "yes" if you want to generate those kinds of files (sheet music, graphic notation, various audio formats).
-- Note, you can also indicate these on the command line. Type: "lua simple_melody.lua -h" for a list of commands.

local generate_lilypond = "no"
local generate_feldman = "no"
local generate_pattern_15 = "no"
local generate_pattern_35 = "no" 
local wave = "no" ; local flac = "no" ; local ogg = "no" ; local ogg_tag = "yes"

-- Do not edit the following function calls and declarations
local command_line_options = {...}
command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac = get_args(command_line_options,command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac)

create_log(generate_log)
header.user_data,header.dedication = get_input(command)

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"
local algorithm = header.algorithm 
local style = "" -- 
local style_algorithm_author = "David Bellows"

-- End of function calls and declarations

-- You can begin editing again

local number_of_notes = 24

local instrument = "Piano"

local key = "c-Panchromatic"
-- local key = "c-major"
-- local key = "c-chromatic"
-- local key = "#0,0,0,2,4,5,5,7,7,9,11:12" -- c-major scale with emphasis -- CURRENTLY UNSUPPORTED; NOT SURE IF IT WILL BE!!! 9/9/16
-- local key = "#0,2,4:5" -- Maija scale, repeats this pattern at every fifth semitone -- DOESN'T WORK! 9/9/16

local tuning_name = "c-Western Standard"
-- local tuning_name = "c-Pythagorean" -- Pythagorean tuning
-- local tuning_name = "c-5 limit JI" -- 5-limit Just Intonation
-- local tuning_name = "c-3/EDO" -- 3-equal divisions of the octave. This can be any number. Just "ED" or "EDO" defaults to "ED:P8"
-- local tuning_name = "c-"..math.pi .. "/!ED:m3" -- pi-equal divisions of the minor third while preserving the 2:1 octave
-- local tuning_name = "c-Harry Partch"
-- local tuning_name = "c-Gamma"
-- local tuning_name = "c-88/CET" -- 88-cET: equal division of the octenave into cents. This can be any number.
-- local tuning_name = "c-Reinhard 128"

local system = "western iso" -- A4 = 440Hz
-- local system = "verdi" -- A4 = 432Hz
-- local system = "432.5" -- set A4 to 432.5Hz

header.reference_pitch,header.reference_pitch_name = reference_pitch(system)

local tempo_word = "andante"
-- local tempo_word = "120.5"

local tempo_number = tempo_value(tempo_word) 

-- Leave the following alone
do -- Generate audio frequencies
   audio_freq_table,ratio_table,header.tuning_octave,low_freq,high_freq = generate_audio_frequency_table(tuning_name)
end

do -- Calculate degrees for tuning
   calculated_scale_degrees = generate_relative_scale_degrees(ratio_table,key)
end

local scale_octave = get_scale_octave(key,calculated_scale_degrees) 
midi_ins_number,header.lowrange,header.highrange = instrument_range(instrument,audio_freq_table,low_freq,high_freq) 

-- Safe to edit again

do -- Pitch Table
   local scale_recipe = "all"
   -- local scale_recipe = "P1:4,P5:2"
   -- local scale_recipe = "tonic:3,dominant:2"
   -- local scale_recipe = "0:3,7:2"
   -- local scale_recipe = "P1:3,7:2"

   local scale_fill = 0
   -- local scale_fill = 1

   local octave_recipe = "all"
   -- local octave_recipe = "C3:1,C4:1"

   local base_table = {}
   base_table = preparse_pitches(audio_freq_table,calculated_scale_degrees, key, scale_octave, octave_recipe, scale_recipe, scale_fill)
   pitch_table = quantize("pitch_table",base_table,number_of_notes)
end
do -- Velocity Table
   
   local velocity_recipe = "all"
   -- local velocity_recipe = "rest,p,mf:2,ff,ff"
   -- local velocity_recipe = "p,mf:2,ff,ff"
   -- local velocity_recipe = "87,ff,37.2222:2,mf" 
   -- local velocity_recipe = "rest,p:2,ff"

   local base_table = {}
   base_table = preparse_velocities(velocity_recipe)

   velocity_table = quantize("velocity_table",base_table,number_of_notes)
end

do -- Duration Table
   local duration_recipe = "all"
   -- local duration_recipe = "16th,8th:2,quarter,crotchet"
   -- local duration_recipe = "1.1:2,2:1" 

   local base_table = {}
   base_table = preparse_durations(duration_recipe)

   duration_table = quantize("duration_table",base_table,number_of_notes)
end

do -- Bel Canto style algorithm -- smooths out the melody to use leaps less than a perfect fifth (if possible)
   -- local normalize_note_to_middle = "yes"
   -- pitch_table = belcanto(pitch_table,calculated_scale_degrees,scale_octave,number_of_notes,normalize_note_to_middle)
end

do -- panchromatic scale -- utility to produce panchromatic scales (useful for testing out tunings, for example)
   -- local range = "full"
   -- local range = "octave"
   -- pitch_table,velocity_table,duration_table,number_of_notes = generate_panchromatic_scale(range,number_of_notes)
end

do -- Create information to be used especially for Csound but useful elsewhere. 
   for counter = 1,number_of_notes do 
      frequency_table[counter] = audio_freq_table[pitch_table[counter]]
      csound_frequency_table[counter] = frequency_table[counter] * (header.reference_pitch/440)
      pan_table[counter] = .5
      ins_num_table[counter] = 1
      midi_ins_num_table[counter] = midi_ins_number
   end
   start_table = generate_start_table(duration_table)
end

do -- Create Csound
   do -- Csound preamble
      csound_preamble = create_csound_preamble(algorithm,style,style_algorithm_author,time_of_creation)
      if sound_font ~= "" then
	 csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
      end	     
   end

   local start_of_table = 1
   ins_num_table[1] = 1
   
   do -- create specific Orchestra instrument
      orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)
   end
   
   csound_score_ins = create_csound_score_ins(start_of_table, orchestra_instrument, number_of_notes, ins_num_table, ins_name_table, velocity_table, csound_frequency_table, duration_table, start_table, pan_table, midi_ins_num_table)

   csound_score_ins = finish_csound_score_ins(csound_score_ins)

   csound_orchestra = csound_orchestra1 

   csound_score = csound_score_function1 .. csound_score_function1 .. "\n" .. csound_score_ins
   
   local orchestra_finish = create_finish_orchestra(tempo_number)
   csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
   csound_file_name = create_csound_file(csound_file,algorithm,style,instrument)
end

do -- Create Audio
   create_csound_audio_file(csound_file_name,style,style_algorithm_author,wave,flac,ogg,ogg_tag)
end

if generate_lilypond == "yes" then -- Generate Lilypond notation
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local adapter = style_algorithm_author ; local pme = "Platonic Music Engine" ; local original_composer = ""
   local original_title = "" ; local voices = 1 ; local lyrics = {}
   
   local time = "4/4" ; local no_indent = "no"
   local rest_or_space = "r" ; local use_ragged_right = "no" ; local avoid_collisions = "no"
   
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"

   local header = lilypond2header(instrument,tuning_name,adapter,pme,original_composer,original_title,additional_info,algorithm,header.reference_pitch_name)

   local lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(instrument,number_of_notes,tuning_name,key,calculated_scale_degrees,pitch_table,frequency_table,duration_table,velocity_table,rest_or_space,avoid_collisions)

   local lilypond_score,score_info = lilypond2variables(instrument,lily_key,time,tempo_word,tempo_number,lily_note_table_1,lily_note_table_2)
   
   local footer = lilypond2footer(score_info,instrument,voices,lyrics,use_ragged_right,no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(instrument,complete_lilypond_score,algorithm)
end      

if generate_feldman == "yes" then -- Feldman Graph Notation
   require("scripts/feldman_notation")
   local volume = "yes" 

   local feldman_score,latexname = midi2feldman(pitch_table,duration_table,velocity_table,instrument,number_of_notes,volume,tuning_name,system,tempo_number)
   create_latex(feldman_score,latexname)
end

if generate_pattern_15 == "yes" then -- Pattern 15 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_15") 
   
   pattern_15_lilypond_score,latexname = 
      midi2pattern_15(pitch_table,duration_table,instrument,number_of_notes)

   create_latex(pattern_15_lilypond_score,latexname)
end      

if generate_pattern_35 == "yes" then -- Pattern 35 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_35")
   pattern_35_score,latexname = midi2pattern_35(pitch_table,duration_table,velocity_table,number_of_notes,instrument)
   
   create_latex(pattern_35_score,latexname)
end

