    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This function gets the latitude and longitude from the user.
-- If the decimal places are less than 13 then they are padded randomly.
-- Or the user can enter "r" at any time to have the computer choose a value randomly
-- All of these options can be passed with the "command" parameter so passing "r"
-- or two numbers separated by spaces will cause the function to compute values
-- without interacting with the user. Only if command = "" will there be an actual
-- interaction.

function get_input(command)
   math.randomseed(os.time())
   local character_table = {}
   local   string_character = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, "
   
   character_table = string.split(string_character,",")

   local input_data = ""
      
   if command == "" then
      io.write("Enter text (letters, numbers, or punctuation) ")
      input_data = io.read()    
      -- input_data = string.gsub(input_data,"%W|%S","") -- Is this actually needed?
   end
   
   if command == "r" or input_data == "r" then input_data = ""	 
      for counter = 1,math.random(24) do	    
	 input_data = input_data .. character_table[math.random(#character_table)]
      end
   end

   if command ~= "r" and command ~= "" then
      input_data = command
   end

   dedication = input_data
   if #dedication > 24 then dedication = string.sub(dedication,1,24) .. " ..." end
  
   write_log("get_input.lua","user_input",input_data)
   write_log("get_input.lua","dedication",dedication)

   header.time_of_creation = os.date() 
   write_log("get_input.lua","time_of_creation",header.time_of_creation)


   return input_data,dedication

end -- function Get_User_Input
  
