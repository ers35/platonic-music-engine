    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This file contains functions for generating data
-- for the Platonic Score

platonic_generator = function(name,low,high,num_voices,round)
   random = require("scripts/pcg")
   local result = {}
   local seed = header.user_data .. name
   local counter = 0
   local highnum,lownum = 0,0
   do -- Random number generator
      -- math.randomseed(hash(seed))
      random.seed(hash(seed),hashstream(seed))
      for counter = 1,num_voices do
	 if low == -1 then
	    result[counter] = random.randomDouble() + .5
	 else 
	    result[counter] = random.random(low,high)
	    if round == "no" then
	       result[counter] = result[counter] + random.randomDouble()
	    end	    
	 end
      end
   end
   return result
end


