    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Scale step numbers
scale_key={}
scale_key={
   ["major"] = {pattern = "P1,M2,M3,P4,P5,M6,M7",repeat_point = "P8"},
   ["minor"] = {pattern = "P1,M2,m3,P4,P5,m6,m7",repeat_point = "P8"},
   ["chromatic"] = {pattern = "P1,m2,M2,m3,M3,P4,P5,TT,m6,M6,m7,M7",repeat_point = "P8"},
   ["harmonic minor"] = {pattern = "P1,M2,m3,P4,P5,m6,M7",repeat_point = "P8"},
   ["greek chromatic"] = {pattern = "P1,m2,M2,P4,P5,m6,M7",repeat_point="P8"},
   ["greek diatonic"] = {pattern = "P1,m2,m3,P4,P5,m6,m7",repeat_point = "P8"},
   ["dorian"] = {pattern = "P1,M2,m3,P4,P5,M6,m7",repeat_point = "P8"},
   ["ionian"] = {pattern = "P1,M2,M3,P4,P5,M6,M7",repeat_point = "P8"},
   ["phrygian"] = {pattern = "P1,m2,m3,P4,P5,m6,m7",repeat_point = "P8"},
   ["lydian"] = {pattern = "P1,M2,M3,TT,P5,M6,M7",repeat_point = "P8"},
   ["mixolydian"] = {pattern = "P1,M2,M3,P4,P5,M6,m7",repeat_point = "P8"},
   ["aeolian"] = {pattern = "P1,M2,m3,P4,P5,m6,m7",repeat_point = "P8"},
   ["locrian"] = {pattern = "P1,m2,m3,P4,TT,m6,m7",repeat_point="P8"},
   ["ultra locrian"] = {pattern = "P1,m2,m3,M3,TT,m6,M6",repeat_point = "P8"},
   ["whole"] = {pattern = "P1,M2,M3,TT,m6,m7",repeat_point = "P8"},
   ["major pentatonic"] = {pattern = "P1,M2,M3,P5,M6",repeat_point = "P8"},
   ["minor pentatonic"] = {pattern = "P1,m3,P4,P5,m7",repeat_point = "P8"},
   ["major blues"] = {pattern = "P1,M2,m3,M3,P5,M6",repeat_point="P8"},
   ["minor blues"] = {pattern = "P1,m3,P4,TT,P5,m7",repeat_point="P8"},
   ["acoustic"] = {pattern = "P1,M2,M3,TT,P5,M6,m7",repeat_point = "P8"},
   ["enigmatic"] = {pattern = "P1,m2,M3,TT,m6,m7,M7",repeat_point = "P8"},
   ["romanian"] = {pattern = "P1,M2,m3,TT,P5,M6,m7",repeat_point = "P8"},
   ["hungarian"] = {pattern = "P1,M2,m3,TT,P5,m6,m7",repeat_point = "P8"},
   ["bebop major"] = {pattern = "P1,M2,M3,P4,P5,m6,M6,M7",repeat_point = "P8"},
   ["bebop dominant"] = {pattern = "P1,M2,M3,P4,P5,M6,m7,M7",repeat_point = "P8"},
   ["maija"] = {pattern = "P1,M2,M3",repeat_point = "P4"},
   ["algerian"] = {pattern = "0,2,3,6,7,8,11,12,14,15,17,19,20,23",repeat_point = 24},
   ["altered"] = {pattern = "P1,m2,m3,M3,TT,m6,m7",repeat_point = "P8"},
   ["augmented"] = {pattern = "P1,m3,M3,P5,m6,M7",repeat_point = "P8"},
   ["prometheus"] = {pattern = "P1,M2,M3,TT,M6,m7",repeat_point = "P8"},
   ["tritone"] = {pattern = "P1,m2,M3,TT,P5,m7",repeat_point = "P8"},
   ["yo"] = {pattern = "P1,M2,P4,P5,M6",repeat_point = "P8"},
   ["octatonic"] = {pattern = "P1,M2,m3,P4,TT,m6,M6,M7",repeat_point = "P8"},
   ["test"] = {pattern = "0,6,13,19",repeat_point = 24},
}  
   
major_scale_degrees = {}
major_scale_degrees = {
   ["tonic"] = "P1",
   ["supertonic"] = "M2",
   ["mediant"] = "M3",
   ["subdominant"] = "P4",
   ["dominant"] = "P5",
   ["submediant"] = "M6",
   ["leadingtone"] = "M7",
   ["leadingnote"] = "M7",
   ["subtonic"] = "M7",
}

minor_scale_degrees = {}
minor_scale_degrees = {
   ["tonic"] = "P1",
   ["supertonic"] = "M2",
   ["mediant"] = "m3",
   ["subdominant"] = "P4",
   ["dominant"] = "P5",
   ["submediant"] = "m6",
   ["subtonic"] = "m7",
   ["leading tone"] = "M7",
   ["leading note"] = "M7",
}


-- Dynamic markings to Velocity
velocity_dynamic_list={}
velocity_dynamic_list={
   ["rest"] = 0,
   ["ppppp"] = 5,
   ["pppp"] = 10,
   ["ppp"] = 17,
   ["pp"] = 24,
   ["p"] = 32,
   ["mp"] = 40,
   ["mf"] = 50,
   ["f"] = 60,
   ["ff"] = 72,
   ["fff"] = 88,
   ["ffff"] = 107,
   ["fffff"] = 127,
}

-- Durations to seconds
durations2seconds = {}
durations2seconds = {
   ["double whole"] = 8,
   ["dotted whole"] = 6,
   ["whole"] = 4,
   ["semibreve"] = 4,
   ["triple dotted half"] = 3.75,
   ["double dotted half"] = 3.5,
   ["dotted minim"] = 3,
   ["dotted half"] = 3,
   ["half"] = 2,
   ["minim"] = 2,
   ["double dotted crotchet"] = 1.75,
   ["double dotted quarter"] = 1.75,
   ["dotted quarter"] = 1.5,
   ["dotted crotchet"] = 1.5,
   ["quarter"] = 1,--4096,
   ["crotchet"] = 1,
   ["eighth"] = .5,
   ["8th"] = .5,--2048,
   ["quaver"] = .5,
   ["sixteenth"] = .25,
   ["16th"] = .25, --1024,
   ["semiquaver"] = .25,
   ["32nd"] = .125, --512,
   ["demisemiquaver"] = .125,
   ["64th"] = .0625, --256,
   ["hemidemisemiquaver"] = .0625,
   ["128th"] = .03125, --128,
}
  
-- Tempo markings to MIDI numbers
tempo_ticks_list = {}
tempo_ticks_list = {
   ["larghissimo"] = 15,
   ["grave"] = 30,
   ["very slow"] = 30, -- English
   ["lento"] = 40,
   ["lent"] = 40, -- French
   ["slow"] = 40, -- English
   ["langsam"] = 40, -- German
   ["largo"] = 45,
   ["larghetto"] = 50,
   ["adagio"] = 60,
   ["stately"] = 60, -- English
   ["adagietto"] = 70,
   ["andantino"] = 80,
   ["andante"] = 90,
   ["walking pace"] = 90, -- English
   ["andante moderato"] = 95,
   ["moderato"] = 110,
   ["modere"] = 110, -- French
   ["moderate"] = 110, -- English
   ["mäßig"] = 110, -- German
   ["allegro moderato"] = 115,
   ["allegretto"] = 120,
   ["moderately fast"] = 120, -- English
   ["moins"] = 120, -- French
   ["rasch"] = 120, -- German
   ["allegro"] = 140,
   ["fast"] = 140, -- English
   ["vif"] = 140, -- French
   ["schnell"] = 140, -- German
   ["vivace"] = 135,
   ["rapide"] = 135, -- French
   ["vivacissimo"] = 145,
   ["very fast"] = 145, -- English
   ["bewegt"] = 145, -- German
   ["allegrissimo"] = 170,
   ["presto"] = 190,
   ["extremely fast"] = 190, -- English
   ["vite"] = 190, -- French
   ["prestissimo"] = 200,
   ["crazy fast"] = 6000,
   ["crazy slow"] = 2000,
}

-- Octave names
octave_name_list={}
octave_name_list={
   ["doublecontra"] = -1,
   ["subcontra"] = 0,
   ["contra"] =1,
   ["great"] = 2,
   ["small"] = 3,
   ["oneline"] = 4,
   ["twoline"] = 5,
   ["threeline"] = 6,
   ["fourline"] = 7,
   ["fiveline"] = 8,
   ["sixline"] = 9,

   ["C-1"] = -1,
   ["C0"] = 0,
   ["C1"] = 1,
   ["C2"] = 2,
   ["C3"] = 3,
   ["C4"] = 4,
   ["C5"] = 5,
   ["C6"] = 6,
   ["C7"] = 7,
   ["C8"] = 8,
   ["C9"] = 9,
}
