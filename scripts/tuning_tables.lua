    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Concert pitch tuning system
concert_pitch_system_list = {}
concert_pitch_system_list = {
      ["western iso"] = {tuning=440.000,midi=69,name="Western ISO 16 (A=440)",pitch="a"},
      ["bach organ"] = {tuning=480.000,midi=69,name="Bach organ (A=480000)",pitch="a"},
      ["handel early"] = {tuning=422.500,midi=69,name="Handel 1740 (A=422.5)",pitch="a"},
      ["handel late"] = {tuning=409.000,midi=69,name="Handel 1780 (A=409)",pitch="a"},
      ["verdi"] = {tuning=432.000,midi=69,name="Verdi (A=432)",pitch="a"},
      ["ny and boston"] = {tuning=442.000,midi=69,name="New York Philharmonic and Boston Symphony Orchestra (A=442)",pitch="a"},
      ["continental"] = {tuning=443.000,midi=69,name="Continental Europe Orchestras (A=443)",pitch="a"},
      ["baroque"] = {tuning=415.000,midi=69,name="Modern Baroque (A=415)",pitch="a"},
      ["chorton"] = {tuning=466.000,midi=69,name="Chorton: Baroque church (A=466)",pitch="a"},
      ["classical"] = {tuning=432.000,midi=69,name="Modern Classical (A=432)",pitch="a"},
      ["test"] = {tuning=110.000,midi=69,name="Dave's Test",pitch="c"},
}

tuning_ratio_list={}
tuning_ratio_list={
   ["test"] = {lua = "1100 cents,2"},
   ["Western Standard"] = {lua="2^(1/12),2^(2/12),2^(3/12),2^(4/12),2^(5/12),2^(6/12),2^(7/12),2^(8/12),2^(9/12),2^(10/12),2^(11/12),2^(12/12)",latex="\\sqrt[12]{2}^1,\\sqrt[12]{2}^2,\\sqrt[12]{2}^3,\\sqrt[12]{2}^4,\\sqrt[12]{2}^5,\\sqrt[12]{2}^6,\\sqrt[12]{2}^7,\\sqrt[12]{2}^8,\\sqrt[12]{2}^9,\\sqrt[12]{2}^{10},\\sqrt[12]{2}^{11},\\sqrt[12]{2}^{12}",pretty_name="Western Standard Tuning (12 EDO)"},
   ["Pythagorean"] = {lua="256/243,9/8,32/27,81/64,4/3,729/512,3/2,128/81,27/16,16/9,243/128,2/1",latex="\\displaystyle\\slashfrac{256}{243},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{32}{27},\\displaystyle\\slashfrac{81}{64},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{729}{512},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{128}{81},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{16}{9},\\displaystyle\\slashfrac{243}{128},\\displaystyle\\slashfrac{2}{1}",pretty_name="Pythagorean Tuning"}, -- https://en.wikipedia.org/wiki/Pythagorean_tuning
   ["5 limit JI"] = {lua="16/15,9/8,6/5,5/4,4/3,45/32,3/2,8/5,5/3,16/9,15/8,2/1",latex="\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{16}{9},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Just Intonation (5 limit symmetric 1)"}, -- https://en.wikipedia.org/wiki/Five-limit_tuning
   ["5 limit JI alt 1"] = {lua="16/15,10/9,6/5,5/4,4/3,45/32,3/2,8/5,5/3,9/5,15/8,2/1",latex="\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{10}{9},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Just Intonation (5 limit symmetric 2)"}, -- https://en.wikipedia.org/wiki/Five-limit_tuning
   ["5 limit JI alt 2"] = {lua="16/15,9/8,6/5,5/4,4/3,45/32,3/2,8/5,5/3,9/5,15/8,2/1",latex="\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Just Intonation (5 limit asymmetric standard)"}, -- https://en.wikipedia.org/wiki/Five-limit_tuning
   ["5 limit JI alt 3"] = {lua="16/15,9/8,6/5,5/4,4/3,25/18,3/2,8/5,5/3,9/5,15/8,2/1",latex="\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{25}{18},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Just Intonation (5 limit asymmetric extended)"}, -- https://en.wikipedia.org/wiki/Five-limit_tuning
   ["7 limit JI"] = {lua="8/7,7/6,6/5,5/4,4/3,7/5,10/7,3/2,8/5,5/3,12/7,7/4,15/8,2/1",latex="\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{8}{7},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{7}{5},\\displaystyle\\slashfrac{10}{7},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Just Intonation (7 limit)"}, -- https://en.wikipedia.org/wiki/7-limit_tuning
   ["La Monte Young Well Tuned Piano"] = {lua="567/512,9/8,147/128,1323/1024,21/16,189/128,3/2,49/32,441/256,7/4,63/32,2/1",latex="\\displaystyle\\slashfrac{567}{512},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{147}{128},\\displaystyle\\slashfrac{21}{16},\\displaystyle\\slashfrac{1323}{1024},\\displaystyle\\slashfrac{189}{128},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{49}{32},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{441}{256},\\displaystyle\\slashfrac{63}{32},\\displaystyle\\slashfrac{2}{1}",pretty_name="La Monte Young's tuning system"}, -- http://xenharmonic.wikispaces.com/young-lm_piano fix thanks to http://www.kylegann.com/wtp.html
   ["Ben Johnston Otonal Utonal"] = {lua="17/16,9/8,19/16,5/4,21/16,11/8,3/2,13/8,27/16,7/4,15/8,2/1",latex="\\displaystyle\\slashfrac{17}{16},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{19}{16},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{21}{16},\\displaystyle\\slashfrac{11}{8},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{13}{8},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Ben Johnston's tuning system"}, -- http://xenharmonic.wikispaces.com/johnston
   ["Bicycle"] = {lua="13/12,9/8,7/6,5/4,4/3,11/8,3/2,13/8,5/3,7/4,11/6,2/1",latex="\\displaystyle\\slashfrac{13}{12},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{7}{6},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{11}{8},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{13}{8},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{11}{6},\\displaystyle\\slashfrac{2}{1}",pretty_name="Bicycle tuning"}, 
   ["George Secor Miracle Temperament"] = {lua = "116.7155941 cents,233.4311882 cents, 350.1467823 cents, 466.8623764 cents, 583.5779705 cents, 700.2935646 cents, 817.0091587 cents, 933.7247528 cents, 1050.4403469 cents, 1167.155941 cents, 1283.8715351 cents"}, -- http://www.anaphoria.com/SecorMiracle.pdf
   ["Werckmeister 1"] = {lua="256/243,(64/81)*math.sqrt(2),32/27,(256/243)*(2^(1/4)),4/3,1024/729,8/9*(8^(1/4)),128/81,1024/729*(2^(1/4)),16/9,128/81*(2^(1/4)),2/1",latex="\\displaystyle\\slashfrac{256}{243},\\displaystyle\\slashfrac{64}{81}\\times\\sqrt{2},\\displaystyle\\slashfrac{32}{27},\\displaystyle\\slashfrac{256}{243}\\times\\sqrt[4]{2},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{1024}{729},\\displaystyle\\slashfrac{8}{9}\\times\\sqrt[4]{8},\\displaystyle\\slashfrac{128}{81},\\displaystyle\\slashfrac{1024}{729}\\times\\sqrt[4]{2},\\displaystyle\\slashfrac{16}{9},\\displaystyle\\slashfrac{128}{81}\\times\\sqrt[4]{2},\\displaystyle\\slashfrac{2}{1}",pretty_name="Werckmeister 1 tuning system"}, -- https://en.wikipedia.org/wiki/Werckmeister_temperament
   ["Werckmeister 2"] = {lua="16384/19683*(2^(1/3)),8/9*(2^(1/3)),32/27,64/81*(4^(1/3)),4/3,1024/729,32/27*(2^(1/3)),8192/6561*(2^(1/3)),256/243*(4^(1/3)),9/(4*(2^(1/3))),4096/2187,2/1",latex="\\displaystyle\\slashfrac{16384}{19683}\\times\\sqrt[3]{2},\\displaystyle\\slashfrac{8}{9}\\times\\sqrt[3]{2},\\displaystyle\\slashfrac{32}{27},\\displaystyle\\slashfrac{64}{81}\\times\\sqrt[3]{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{1024}{729},\\displaystyle\\slashfrac{32}{27}\\times\\sqrt[3]{2},\\displaystyle\\slashfrac{8192}{6561}\\times\\sqrt[3]{2},\\displaystyle\\slashfrac{256}{243}\\times\\sqrt[3]{4},\\frac{9}{4\\sqrt[3]{2}},\\displaystyle\\slashfrac{4096}{2187},\\displaystyle\\slashfrac{2}{1}",pretty_name="Werckmeister 2 tuning system"}, -- https://en.wikipedia.org/wiki/Werckmeister_temperament
   ["Werckmeister 3"] = {lua="8/9*(2^(1/4)),9/8,2^(1/4),8/9*(2^(1/2)),9/8*(2^(1/4)),2^(1/2),3/2,128/81,8^(1/4),3/(8^(1/4)),4/3*(2^(1/2)),2/1",latex="\\frac{8}{9\\sqrt[4]{2}},\\displaystyle\\slashfrac{9}{8},\\sqrt[4]{2},\\frac{8}{9\\sqrt{2}},\\frac{9}{8\\sqrt[4]{2}},\\sqrt{2},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{128}{81},\\sqrt[4]{8},\\frac{3}{\\sqrt[4]{8}},\\frac{4}{3\\sqrt{2}},\\displaystyle\\slashfrac{2}{1}",pretty_name="Werckmeister 3 tuning system"}, -- https://en.wikipedia.org/wiki/Werckmeister_temperament
   ["Werckmeister 4"] = {lua = "98/93,49/44,196/165,49/39,4/3,196/139,196/131,49/31,196/117,98/55,49/26,2/1"}, -- https://en.wikipedia.org/wiki/Werckmeister_temperament
   ["Quarter Comma Meantone"] = {lua="1.07,1.1180,1.1963,1.25,1.3375,1.3975,1.4953,1.6,1.6719,1.7889,1.8692,2",latex="1.07,1.1180,1.1963,1.25,1.3375,1.3975,1.4953,1.6,1.6719,1.7889,1.8692,2",pretty_name="Quarter Comma Meantone tuning system"},
   ["Harry Partch"] = {lua="81/80,33/32,21/20,16/15,12/11,11/10,10/9,9/8,8/7,7/6,32/27,6/5,11/9,5/4,14/11,9/7,21/16,4/3,27/20,11/8,7/5,10/7,16/11,40/27,3/2,32/21,14/9,11/7,8/5,18/11,5/3,27/16,12/7,7/4,16/9,9/5,20/11,11/6,15/8,40/21,64/33,160/81,2/1",latex="\\displaystyle\\slashfrac{81}{80},\\displaystyle\\slashfrac{33}{32},\\displaystyle\\slashfrac{21}{20},\\displaystyle\\slashfrac{16}{15},\\displaystyle\\slashfrac{12}{11},\\displaystyle\\slashfrac{11}{10},\\displaystyle\\slashfrac{10}{9},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{8}{7},\\displaystyle\\slashfrac{7}{6},\\displaystyle\\slashfrac{32}{27},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{11}{9},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{14}{11},\\displaystyle\\slashfrac{9}{7},\\displaystyle\\slashfrac{21}{16},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{27}{20},\\displaystyle\\slashfrac{11}{8},\\displaystyle\\slashfrac{7}{5},\\displaystyle\\slashfrac{10}{7},\\displaystyle\\slashfrac{16}{11},\\displaystyle\\slashfrac{40}{27},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{32}{21},\\displaystyle\\slashfrac{14}{9},\\displaystyle\\slashfrac{11}{7},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{18}{11},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{12}{7},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{16}{9},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{20}{11},\\displaystyle\\slashfrac{11}{6},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{40}{21},\\displaystyle\\slashfrac{64}{33},\\displaystyle\\slashfrac{160}{81},\\displaystyle\\slashfrac{2}{1}",pretty_name="Harry Partch's 43 Tone tuning system"},
   ["Vicentino"] = {lua="40/39,21/20,14/13,13/12,10/9,8/7,7/6,6/5,39/32,5/4,50/39,13/10,4/3,160/117,45/32,75/52,117/80,3/2,20/13,39/25,8/5,64/39,5/3,12/7,7/4,9/5,24/13,13/7,40/21,39/20,2/1",latex="\\displaystyle\\slashfrac{40}{39},\\displaystyle\\slashfrac{21}{20},\\displaystyle\\slashfrac{21}{20},\\displaystyle\\slashfrac{14}{13},\\displaystyle\\slashfrac{13}{12},\\displaystyle\\slashfrac{10}{9},\\displaystyle\\slashfrac{8}{7},\\displaystyle\\slashfrac{7}{6},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{39}{32},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{50}{39},\\displaystyle\\slashfrac{13}{10},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{160}{117},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{75}{52},\\displaystyle\\slashfrac{117}{80},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{20}{13},\\displaystyle\\slashfrac{39}{25},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{64}{39},\\displaystyle\\slashfrac{5}{3},\\displaystyle\\slashfrac{12}{7},\\displaystyle\\slashfrac{7}{4},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{24}{13},\\displaystyle\\slashfrac{13}{7},\\displaystyle\\slashfrac{40}{21},\\displaystyle\\slashfrac{39}{20},\\displaystyle\\slashfrac{2}{1}",pretty_name="Nicola Vicentino's 31 tone tuning system"}, --[[officially 21/20 is there twice but I don't think we need it?]] 
   ["Alpha"] = {lua="78 cents,156 cents,234 cents,312 cents,390 cents,468 cents,546 cents,624 cents,702 cents,780 cents,858 cents,936 cents,1014 cents,1092 cents,1170 cents,1248 cents",pretty_name="Wendy Carlos's Alpha tuning system"},
   ["Beta"] = {lua="63.8 cents,127.6 cents,191.4 cents,255.2 cents,319 cents,382.8 cents,446.6 cents,510.4 cents,574.2 cents,638 cents,701.8 cents,765.6 cents,829.4 cents,893.2 cents,957 cents,1020.8 cents,1084.6 cents,1148.4 cents,1212.2 cents",latex="1.03766,1.076738,1.116642,1.158694,1.20233,1.24761,1.294594,1.343348,1.393133,1.445598,1.500039,1.55653,1.615148,1.675006,1.738087,1.803542,1.871463,1.941942,2.013911",pretty_name="Wendy Carlos's Beta tuning system"},
   ["Gamma"] = {lua="35.1 cents,70.2 cents,105.3 cents,140.4 cents,175.5 cents,210.6 cents,245.7 cents,280.8 cents,315.9 cents,351.0 cents,386.1 cents,421.2 cents,456.3 cents,491.4 cents,526.5 cents,561.6 cents,596.7 cents,631.8 cents,666.9 cents,702.0 cents,737.1 cents,772.2 cents,807.3 cents,842.4 cents,877.5 cents,912.6 cents,947.7 cents,982.8 cents,1017.9 cents,1053.0 cents,1088.1 cents,1123.2 cents,1158.3 cents,1193.4 cents,1228.5 cents",latex="1.0205,1.0414,1.0627,1.0844,1.1067,1.1294,1.1525,1.1761,1.2002,1.2248,1.2498,1.2754,1.3016,1.3282,1.3554,1.3832,1.4115,1.4404,1.4699,1.5,1.5308,1.5621,1.5941,1.6268,1.6601,1.6941,1.7288,1.7642,1.8003,1.8372,1.8748,1.9132,1.9524,1.9924,2.0031",pretty_name="Wendy Carlos's Gamma tuning system"},
   ["Bohlen Pierce"] = {lua="3^(1/13),3^(2/13),3^(3/13),3^(4/13),3^(5/13),3^(6/13),3^(7/13),3^(8/13),3^(9/13)",latex="\\sqrt[13]{3},\\sqrt[13]{3}^2,\\sqrt[13]{3}^{3},\\sqrt[13]{3}^{4},\\sqrt[13]{3}^{5},\\sqrt[13]{3}^{6},\\sqrt[13]{3}^{7},\\sqrt[13]{3}^{8},\\sqrt[13]{3}^{9}",pretty_name="Bohlen-Pierce tuning"},
   -- ["bohlen-pierce"] = "3^(1/13),3^(2/13),3^(3/13),3^(4/13),3^(5/13),3^(6/13),3^(7/13),3^(8/13),3^(9/13),3^(10/13),3^(11/13),3^(12/13),3^(13/13)",
   ["bp"] = "3^(1/13),3^(2/13),3^(3/13),3^(4/13),3^(5/13),3^(6/13),3^(7/13),3^(8/13),3^(9/13)", -- technically the tuning looks as above but I think this is all we need and it produces the correct sounding MIDI
   ["Agricola Monochord"] = {lua = "135/128,9/8,1215/1024,81/64,4/3,45/32,3/2,405/256,27/16,16/9,243/128,2/1",latex="\\displaystyle\\slashfrac{135}{128},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{1215}{1024},\\displaystyle\\slashfrac{81}{64},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{405}{256},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{16}{9},\\displaystyle\\slashfrac{243}{128},\\displaystyle\\slashfrac{2}{1}",pretty_name="Agricola's Monochord"},
   ["Agricola Pythagorean Monochord"] = {lua = "1.0654620197557,9/8,1.1986447779505,81/64,4/3,1.4206160270511,3/2,1.5981930380662,27/16,1.797967166027,243/128,2/1",latex="1.0654620197557,\\displaystyle\\slashfrac{9}{8},1.1986447779505,\\displaystyle\\slashfrac{81}{64},\\displaystyle\\slashfrac{4}{3},1.4206160270511,\\displaystyle\\slashfrac{3}{2},1.5981930380662,\\displaystyle\\slashfrac{27}{16},1.797967166027,\\displaystyle\\slashfrac{243}{128},\\displaystyle\\slashfrac{2}{1}",pretty_name="Agricola's Pythagorean-type Monochord"},
   ["Kepler 1"] = {lua = "135/128,9/8,6/5,5/4,4/3,45/32,3/2,405/256,27/16,9/5,15/8,2/1",latex="\\displaystyle\\slashfrac{135}{128},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{405}{256},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Kepler's Monochord 1"},
   ["Kepler 2"] = {lua = "135/128,9/8,6/5,5/4,4/3,45/32,3/2,8/5,27/16,9/5,15/8,2/1",latex="\\displaystyle\\slashfrac{135}{128},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{8}{5},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{15}{8},\\displaystyle\\slashfrac{2}{1}",pretty_name="Kepler's Monochord 2"},
   ["Kepler Choice"] = {lua = "135/128,9/8,6/5,5/4,4/3,45/32,3/2,405/256,27/16,9/5,243,128,2/1",latex="\\displaystyle\\slashfrac{135}{128},\\displaystyle\\slashfrac{9}{8},\\displaystyle\\slashfrac{6}{5},\\displaystyle\\slashfrac{5}{4},\\displaystyle\\slashfrac{4}{3},\\displaystyle\\slashfrac{45}{32},\\displaystyle\\slashfrac{3}{2},\\displaystyle\\slashfrac{405}{256},\\displaystyle\\slashfrac{27}{16},\\displaystyle\\slashfrac{9}{5},\\displaystyle\\slashfrac{243}{128},\\displaystyle\\slashfrac{2}{1}",pretty_name="Kepler's Choice System"},
   ["Reinhard 128"] = {lua="13 cents,27 cents,40 cents,53 cents,66 cents,79 cents,92 cents,105 cents,118 cents,130 cents,143 cents,155 cents,167 cents,180 cents,192 cents,204 cents,216 cents,228 cents,240 cents,251 cents,263 cents,275 cents,286 cents,298 cents,309 cents,320 cents,331 cents,342 cents,354 cents,365 cents,375 cents,386 cents,397 cents,408 cents,418 cents,429 cents,440 cents,450 cents,460 cents,471 cents,481 cents,491 cents,501 cents,512 cents,522 cents,532 cents,541 cents,551 cents,561 cents,571 cents,581 cents,590 cents,600 cents,609 cents,619 cents,628 cents,638 cents,647 cents,656 cents,666 cents,675 cents,684 cents,693 cents,702 cents,711 cents,720 cents,729 cents,738 cents,746 cents,755 cents,764 cents,773 cents,781 cents,790 cents,798 cents,807 cents,815 cents,824 cents,832 cents,841 cents,849 cents,857 cents,865 cents,874 cents,882 cents,890 cents,898 cents,906 cents,914 cents,922 cents,930 cents,938 cents,945 cents,953 cents,961 cents,969 cents,977 cents,984 cents,992 cents,999 cents,1007 cents,1015 cents,1022 cents,1030 cents,1037 cents,1044 cents,1052 cents,1059 cents,1066 cents,1074 cents,1081 cents,1088 cents,1095 cents,1103 cents,1110 cents,1117 cents,1124 cents,1131 cents,1138 cents,1145 cents,1152 cents,1159 cents,1166 cents,1173 cents,1180 cents,1186 cents,1193 cents,1200 cents",latex = "13,27,40,53,66,79,92,105,118,130,143,155,167,180,192,204,216,228,240,251,263,275,286,298,309,320,331,342,354,365,375,386,397,408,418,429,440,450,460,471,481,491,501,512,522,532,541,551,561,571,581,590,600,609,619,628,638,647,656,666,675,684,693,702,711,720,729,738,746,755,764,773,781,790,798,807,815,824,832,841,849,857,865,874,882,890,898,906,914,922,930,938,945,953,961,969,977,984,992,999,1007,1015,1022,1030,1037,1044,1052,1059,1066,1074,1081,1088,1095,1103,1110,1117,1124,1131,1138,1145,1152,1159,1166,1173,1180,1186,1193,1200", pretty_name = "Johnny Reinhard 128 Tuning"},
   ["Silver"] = {lua = "118.86332c,274.17612c,376.04828c,510.73163c,589.91591c,1200c",latex = "118.86332,274.17612,376.04828,510.73163,589.91591",pretty_name = "Silver"},
   ["Phi"] = {lua = "149.35258c,235.77436c,366.89853c,560.06655c,733.89867c,824.47283c,1200c",latex = "149.35258,235.77436,366.89853,560.06655,733.89867,824.47283,1200",pretty_name = "Phi"},
   ["Shruti"] = {lua = "256/243,16/15,10/9,9/8,32/27,6/5,5/4,81/64,4/3,27/20,45/32,729/512,3/2,128/81,8/5,5/3,27/16,16/9,9/5,15/8,243/128,2",pretty_name = "Classical Indian 22-Shruti Tuning"},
   ["Ptolemy Intense Diatonic"] = {lua = "9/8,5/4,4/3,3/2,5/3,15/8,2/1"},
   ["n-cet"] = {lua="",latex="",pretty_name=""},
   ["n-edo"] = {lua="",latex="",pretty_name=""},
}

platonic_degree = {} -- Just Intonation 5 limit scale 1 symmetric from Wikipedia
platonic_degree = {
   ["P1"] = 1,
   ["m2"] = 1.06666666,
   ["M2"] = 1.125,
   ["m3"] = 1.2,
   ["M3"] = 1.25, 
   ["P4"] = 1.33333333, 
   ["TT"] = 1.41423611, -- Average of 45/32 and 64/45 (aug 4th and dim 5th)
   ["P5"] = 1.5,
   ["m6"] = 1.6, 
   ["M6"] = 1.66666666, 
   ["m7"] = 1.777777778, 
   ["M7"] = 1.875, 
   ["P8"] = 2,
   -- ["m9"] = 2.133333333,
   -- ["M9"] = 2.25,
   -- ["m10"] = 2.4,
   -- ["M10"] = 2.5,
   -- ["P11"] = 2.666666666,
   -- ["TT2"] = 2.8284722,
   -- ["P12"] = 3,
   -- ["m13"] = 3.2,
   -- ["M13"] = 3.333333333,
   -- ["m14"] = 3.555555555,
   -- ["M14"] = 3.75,
   -- ["P15"] = 4,      
}

distance_of_A_to_key = {}
distance_of_A_to_key = {
   ["a"] = "P1",
   ["as"] = "m2",
   ["bf"] = "m2",
   ["b"] = "M2",
   ["bs"] = "m3",
   ["c"] = "m3",
   ["cs"] = "M3",
   ["df"] = "M3",
   ["d"] = "P4",
   ["ds"] = "TT",
   ["ef"] = "TT",
   ["e"] = "P5",
   ["es"] = "m6",
   ["f"] = "m6",
   ["fs"] = "M6",
   ["gf"] = "M6",
   ["g"] = "m7",
   ["gs"] = "M7",
   ["af"] = "M7",
}

distance_of_key_to_A = {}
distance_of_key_to_A = {
   ["a"] = "P8", --"P1" formerly 10/2/16 needs testing
   ["as"] = "M7",
   ["bf"] = "M7",
   ["b"] = "m7",
   ["bs"] = "M6",
   ["c"] = "M6",
   ["cs"] = "m6",
   ["df"] = "m6",
   ["d"] = "P5",
   ["ds"] = "TT",
   ["ef"] = "TT",
   ["e"] = "P4",
   ["es"] = "M3",
   ["f"] = "M3",
   ["fs"] = "m3",
   ["gf"] = "m3",
   ["g"] = "M2",
   ["gs"] = "m2",
   ["af"] = "m2",
}

distance_of_C_to_key = {}
distance_of_C_to_key = {
   ["a"] = "M6",
   ["as"] = "m7",
   ["bf"] = "m7",
   ["b"] = "M7",
   ["bs"] = "P1",
   ["c"] = "P1",
   ["cs"] = "m2",
   ["df"] = "m2",
   ["d"] = "M2",
   ["ds"] = "m3",
   ["ef"] = "m3",
   ["e"] = "M3",
   ["es"] = "P4",
   ["f"] = "P4",
   ["fs"] = "TT",
   ["gf"] = "TT",
   ["g"] = "P5",
   ["gs"] = "m6",
   ["af"] = "m6",
}

equal_base_frequencies = {}
equal_base_frequencies = {
   ["a"] = 440,--440,--220.0, --440.000,
   ["as"] = 466.1637615181,--233.081880759, --466.1637615181,
   ["bf"] = 466.1637615181,--233.081880759, --466.1637615181,
   ["b"] = 493.88330125613,--246.941650628, --493.88330125613,
   ["c"] = 523.251130602,--261.6255653006,
   ["cs"] = 554.365261954,--277.1826309769,
   ["df"] = 554.365261954,--277.1826309769,
   ["d"] = 587.329535834,--293.6647679174,
   ["ds"] = 622.253967444,--311.1269837221,
   ["ef"] = 622.253967444,--311.1269837221,
   ["e"] = 659.255113826,--329.6275569129,
   ["f"] = 698.456462866,--349.2282314330,
   ["fs"] = 739.988845424,--369.9944227116,
   ["gf"] = 739.988845424,--369.9944227116,
   ["g"] = 783.990871964,--391.9954359817,
   ["gs"] = 830.60939516,--415.3046975799,
   ["af"] = 830.60939516,--415.3046975799,
}
