    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2016 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

generate_prelude = function(generate_scale_degrees,pitch_table,velocity_table,duration_table,num_1,num_2,num_3)
   local pitch_table_1 = {} ; local pitch_table_2 = {} ; local pitch_table_3 = {}
   local velocity_table_1 = {} ; local velocity_table_2 = {} ; local velocity_table_3 = {}
   local duration_table_1 = {} ; local duration_table_2 = {} ; local duration_table_3 = {}

   local tuning_octave = header.tuning_octave
   local first = velocity_table[1]
   local second = velocity_table[2]
   local average = math.ceil((header.lowrange + header.highrange) /2)

   local base = calculated_scale_degrees.TT
   for metacounter = 0,num_1 - 5 + 39,5 do --466,5 do
      for counter = 1,5 do
	 while pitch_table[counter+metacounter] > average + base do
	    pitch_table[counter+metacounter] = pitch_table[counter+metacounter] - tuning_octave
	 end
	 while pitch_table[counter+metacounter] < average - base do
	    pitch_table[counter+metacounter] = pitch_table[counter+metacounter] + tuning_octave
	 end
      end
   end

   for counter = 0,num_1,14 do
      pitch_table_1[1+counter] = pitch_table[1] + tuning_octave
      pitch_table_1[2+counter] = pitch_table[3+counter] 
      pitch_table_1[3+counter] = pitch_table[4+counter]
      while pitch_table_1[3+counter] <= pitch_table_1[2+counter] do
	 pitch_table_1[3+counter] = pitch_table_1[3+counter] + tuning_octave
	 if pitch_table_1[3+counter] == pitch_table_1[3+counter] - tuning_octave then break end -- Needed for 0-edo
      end

      pitch_table_1[4+counter] = pitch_table[5+counter]
      while pitch_table_1[4+counter] <= pitch_table_1[3+counter] do
	 pitch_table_1[4+counter] = pitch_table_1[4+counter] + tuning_octave
	 if pitch_table_1[3+counter] == pitch_table_1[3+counter] - tuning_octave then break end -- Needed for 0-edo
      end
      -- if pitch_table_1[4+counter] > header.highrange then pitch_table_1[4+counter] = pitch_table_1[4+counter] - tuning_octave end
      -- if pitch_table_1[4+counter] < header.lowrange then pitch_table_1[4+counter] = pitch_table_1[4+counter] + tuning_octave end
      pitch_table_1[5+counter] = pitch_table_1[2+counter]
      pitch_table_1[6+counter] = pitch_table_1[3+counter]
      pitch_table_1[7+counter] = pitch_table_1[4+counter]

      velocity_table_1[1+counter] = 0
      velocity_table_1[2+counter] = first
      velocity_table_1[3+counter] = first
      velocity_table_1[4+counter] = first
      velocity_table_1[5+counter] = first
      velocity_table_1[6+counter] = first
      velocity_table_1[7+counter] = first

      duration_table_1[1+counter] = .5
      duration_table_1[2+counter] = .25      
      duration_table_1[3+counter] = .25
      duration_table_1[4+counter] = .25
      duration_table_1[5+counter] = .25
      duration_table_1[6+counter] = .25
      duration_table_1[7+counter] = .25

      pitch_table_1[8+counter] = pitch_table_1[1+counter]
      pitch_table_1[9+counter] = pitch_table_1[2+counter] 
      pitch_table_1[10+counter] = pitch_table_1[3+counter]
      pitch_table_1[11+counter] = pitch_table_1[4+counter]
      pitch_table_1[12+counter] = pitch_table_1[5+counter]
      pitch_table_1[13+counter] = pitch_table_1[6+counter]
      pitch_table_1[14+counter] = pitch_table_1[7+counter]

      velocity_table_1[8+counter] = 0
      velocity_table_1[9+counter] = second
      velocity_table_1[10+counter] = second
      velocity_table_1[11+counter] = second
      velocity_table_1[12+counter] = second
      velocity_table_1[13+counter] = second
      velocity_table_1[14+counter] = second

      duration_table_1[8+counter] = .5
      duration_table_1[9+counter] = .25      
      duration_table_1[10+counter] = .25
      duration_table_1[11+counter] = .25
      duration_table_1[12+counter] = .25
      duration_table_1[13+counter] = .25
      duration_table_1[14+counter] = .25

   end

   for counter = 0, num_2,4 do
      pitch_table_2[1+counter] = pitch_table[1] - tuning_octave
      pitch_table_2[2+counter] = pitch_table[2+counter] - tuning_octave
      pitch_table_2[3+counter] = pitch_table[1] - tuning_octave
      pitch_table_2[4+counter] = pitch_table[2+counter] - tuning_octave

      velocity_table_2[1+counter] = 0
      velocity_table_2[2+counter] = first
      velocity_table_2[3+counter] = 0
      velocity_table_2[4+counter] = second

      duration_table_2[1+counter] = .25
      duration_table_2[2+counter] = 1.75
      duration_table_2[3+counter] = .25
      duration_table_2[4+counter] = 1.75
      
   end

   for counter = 0,num_3,2 do
      pitch_table_3[1+counter] = pitch_table[1+counter] - (tuning_octave * 2)
      pitch_table_3[2+counter] = pitch_table_3[1+counter]
      
      velocity_table_3[1+counter] = first
      velocity_table_3[2+counter] = second

      duration_table_3[1+counter] = 2
      duration_table_3[2+counter] = 2
   end

   for counter = num_1-14,num_1 do
      pitch_table_1[counter] = pitch_table[2+counter] 
      velocity_table_1[counter] = first
      duration_table_1[counter] = .25
   end

   velocity_table_1[num_1-14] = 0
   duration_table_1[num_1-14] = .5

   pitch_table_2[num_2-1] = pitch_table[1+num_1] - tuning_octave
   velocity_table_2[num_2-1] = first

   duration_table_2[num_2-1] = 3.75

   pitch_table_3[num_3-1] = pitch_table[num_1+1] - (tuning_octave * 2)
   velocity_table_3[num_3-1] = first
   duration_table_3[num_3-1] = 4

   pitch_table_3[num_3] = pitch_table[#pitch_table_1 + 1] - (tuning_octave * 2) 
   velocity_table_3[num_3] = first
   duration_table_3[num_3] = 4

   pitch_table_2[num_2] = pitch_table[#pitch_table_1 + 2] - (tuning_octave * 1) 
   velocity_table_2[num_2] = first
   duration_table_2[num_2] = 4

   pitch_table_1[num_1 + 1] = pitch_table[#pitch_table_1 + 3]
   pitch_table_1[num_1 + 2] = pitch_table[#pitch_table_1 + 4]
   pitch_table_1[num_1 + 3] = pitch_table[#pitch_table_1 + 5]

   velocity_table_1[num_1 +1] = first
   velocity_table_1[num_1 +2] = first
   velocity_table_1[num_1 +3] = first
   
   duration_table_1[num_1 +1] = 4
   duration_table_1[num_1 +2] = "."
   duration_table_1[num_1 +3] = "."

   
   for counter = 1,#pitch_table_1 do
      while pitch_table_1[counter] > header.highrange do
	 pitch_table_1[counter] = pitch_table_1[counter] - tuning_octave
      end
   end

   for counter = 1,#pitch_table_3 do
      while pitch_table_3[counter] < header.lowrange do
	 pitch_table_3[counter] = pitch_table_3[counter] + tuning_octave
      end
   end

   for counter = 1,#pitch_table_2 do
      while pitch_table_2[counter] < header.lowrange do
	 pitch_table_2[counter] = pitch_table_2[counter] + tuning_octave
      end
   end
   
   return pitch_table_1,pitch_table_2,pitch_table_3,velocity_table_1,velocity_table_2,velocity_table_3,duration_table_1,duration_table_2,duration_table_3
end
      
