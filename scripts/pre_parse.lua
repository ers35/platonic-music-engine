    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Preparsers for pitch/velocity/duration/tempo recipes

preparse_pitches = function(audio_freq_table,calculated_degrees,scale_name,scale_octave,octave_recipe,scale_recipe,fill)
   local pitch_table = {} -- This is the table we're generating
   local scale_table = {}
   local degree_value = 0
   local scale_recipe_table = {}
   local octave_table = {}
   local octave_pattern_table = {}
   scale_octave = math.floor(scale_octave)
   local low = header.lowrange
   local high = header.highrange
   local tuning_octave = header.tuning_octave
   
   -- The following is for 0-EDO these values are 1 when using .001-EDO so it seems reasonable
   if scale_octave == 0 then scale_octave = 1 tuning_octave = 1 end
   
   do -- Create scale_table that contains the pattern for the scale and degrees emphasized
      if not scale_recipe then scale_recipe = "all" end
      if scale_recipe == "all" then fill = 1 end -- Sane assumption regardless of what user enters
      
      local key,mode = bifurcate(scale_name,"-") 
      local rotate_degree = distance_of_A_to_key[key] 
      local rotate_value = calculated_degrees[rotate_degree] -- 0-EDO odd, .001-EDO reports 0 but 0-EDO reports 1

      if mode == "Panchromatic" then -- This works for 0-EDO
	 for counter = 0,scale_octave -1 do
	    scale_table[counter] = fill
	 end

      else   	 
	 for counter = 0,scale_octave-1 do scale_table[counter] = 0 end
	 local tmp_scale_pattern = (scale_key[mode].pattern)
	 local scale_pattern_table = tmp_scale_pattern:split(",")
	 for i,value in ipairs(scale_pattern_table) do  
	    local degree_value = calculated_degrees[value] 
	    local tmp_value = (degree_value + rotate_value) 
	    if tmp_value >= scale_octave then tmp_value = tmp_value - scale_octave end -- the fact that we can't use modulo for the same effect is troublesome
	    scale_table[(tmp_value)] = fill 
	 end     
      end
      
      write_log("pre_parse","scale_table",scale_table)

      scale_recipe_table = scale_recipe:split(",")     
      
      for i,value in ipairs(scale_recipe_table) do  
	 local degree,multiplier = bifurcate(value,":") 
	 
	 if degree ~= "all" then 
	    if not multiplier then multiplier = 1 end
	    if tonumber(degree) then 
	       degree_value = (tonumber(degree) + rotate_value) % scale_octave 
	    else
	       degree_value = (calculated_degrees[degree]) 
	       local tmp_value = degree_value + rotate_value 
	       if tmp_value >= tonumber(math.ceil(scale_octave)) then tmp_value = tmp_value - scale_octave end
	       degree_value = tonumber(tmp_value) 
	    end

	    if tonumber(scale_table[degree_value]) ~= tonumber(fill) then
	       scale_table[degree_value] = scale_table[degree_value] + multiplier
	    else
	       scale_table[degree_value] = multiplier
	    end
	    
	 end  
      end
      write_log("pre_parse","scale_table (with scale degrees)",scale_table)
   end -- Create scale_table that contains the pattern for the scale and degrees emphasized  
  
   do -- Create octave_table that contains the pattern for the octave and octaves emphasized
      local tmp_table = {}
      if not octave_recipe then octave_recipe = "all" end
      local octave_recipe_table = octave_recipe:split(",")
      for counter = -1,9 do octave_table[counter] = 0 end
      
      for i,value in ipairs(octave_recipe_table) do
	 local octave,multiplier = bifurcate(value,":")

	 if octave ~= "all" then
	    local tmp_octave_number = octave_name_list[octave] 
	    octave_table[tmp_octave_number] = multiplier
	 else
	    for counter = -1,9 do octave_table[counter] = 1 end
	 end 
       end      
      write_log("pre_parse","octave_table",octave_table)
   end -- Create octave_table
      
   do -- now put it all together           
      local middle_a = 69 -- + calculated_degrees.m3 - tuning_octave 
      local octave_low = middle_a - (tuning_octave * 5)  
      local octave_high = middle_a + (tuning_octave * 5) 

      local increment = 1
      for counter = -1,9 do
	 for subcounter = 1,tonumber(octave_table[counter]) do
	    for subsubcounter = 0,scale_octave-1 do 
	       if scale_table[subsubcounter] ~= 0 then
		  for subsubsubcounter = 1,scale_table[subsubcounter] do 
		     local pitch = math.floor(((counter + 1)  * tuning_octave) + subsubcounter + octave_low)
		     if pitch >= low and pitch <= high then
			pitch_table[increment] = pitch 
			increment = increment + 1
		     end
		  end
	       end		  
	    end
	 end	      
      end
   end -- put it all together      
   
   write_log("pre_parse","pitch_table",pitch_table)
   return pitch_table
	 
end -- pre_parse pitches

-- Dynamic markings to MIDI velocity numbers
preparse_velocities = function(basestring)
   local newstring = {} ; local tmp_table = {} ; local increment = 1 
   if basestring == "all" then 
      newstring = get_keys_from_table(velocity_dynamic_list)     
   else
      newstring = basestring:split(",") 
   end
   local resultstring = "" 

   for counter = 1,#newstring do
      local vel,number = bifurcate(newstring[counter],":")
      if number == nil then number = 1 end 
      for subcounter = 1,number do
	 local tmp_velocity = velocity_dynamic_list[vel]
	 if not tmp_velocity then tmp_velocity = vel end
	 tmp_table[increment] = tmp_velocity 
	 increment = increment + 1
      end
   end
   return(tmp_table)
end 

-- Preparse Durations 
preparse_durations = function(basestring)
   local newstring = {} ; local tmp_table = {} ; local increment = 1
   if basestring == "all" then
      newstring = get_keys_from_table(durations2seconds) 
   else
      newstring = basestring:split(",") 
   end
   local resultstring = "" ; local tmp_table = {}
   for counter = 1,#newstring do
      local duration,number = bifurcate(newstring[counter],":")
      if number == nil then number = 1 end
      for subcounter = 1,number do
	 local tmp_duration = durations2seconds[duration] 
	 if not tmp_duration then tmp_duration = duration end 
	 tmp_table[increment] = tmp_duration 
	 increment = increment + 1
      end
   end
   return tmp_table
end

-- Tempo markings to number
tempo_value = function(basestring)
   if tonumber(basestring) then 
      tempo_number = tonumber(basestring) 
   else tempo_number = tempo_ticks_list[basestring]
   end

   return tempo_number
end

