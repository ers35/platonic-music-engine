    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Pattern 15 from Robert Kirkpatrick's *The Book of Musical Patterns*
-- http://www.spiralcage.com/hollowearthrecordings/bomp/bompDownloads.html

    -- The Book of Musical Patterns
    -- All scores composed by Robert j Kirkpatrick 2006-2008
    -- Usage Restrictions
    -- All of these scores can be freely performed, recorded or studied without any licensing, notification or permissions
    -- so long as the composer is cited.
    -- Individual scores, up to the entire book of scores, can be reproduced for individual, group or educational purposes.
    -- No scores from the book, or the book itself, can be sold or published in other works without citation.
    -- Individual scores can be reproduced in whole or in part for album covers, liner notes,  web pages,
    -- essays, articles or books as long as citation of the composer is included.
    -- In general I encourage any and all uses of these scores; I want them performed, recorded and heard.
    -- Notification of performances, recording and the like would be appreciated.
    -- Please address any questions or notification of recordings, performance or other pertinent information to 
    -- patterns@hollowearthrecordings.com
    -- Additionally I will publish any and all recordings of these scores on the Hollow Earth Recordings website. Details at 
    -- http://www.hollowearthrecordings.com/bomp/

midi2pattern_15 = function(notes,duration,pattern_15_instrument,pattern_15_number_of_notes)
   local title = "Music" ; local dedication = header.dedication
   local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
   local subtitle = "" ; local subsubtitle = "" ; local algorithm = "Pattern 15"
   local poet = "composed by Robert Kirkpatrick"
   local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z") ; local paper_size="letter"
   local pattern_15_instrument = pattern_15_instrument:gsub("%a", string.upper, 1) 
   local font = "newcent" ; local meter = "for one sound source of varying magnitude" ; local piece = ""
   local temperament_name = ""
   local pretty_system = nil
   
   local latex_name = (pattern_15_instrument.."_Book_of_Patterns_no_15")
   latex_name = latex_name:gsub("%s+", "_")

   local preamble = latex_preamble(font)
   
   local title = latex_title(dedication,title,pattern_15_instrument,poet,composer,arranger,algorithm,meter,piece,temperament_name,pretty_system) 

   local pic_environment = "\n\n\\begin{tikzpicture}\n"

   pattern_15_lilypond_score = preamble..title..pic_environment

   local horizontal = 0 ; local new_duration = {}

   for counter = 1,pattern_15_number_of_notes do notes[counter] = ((notes[counter]*(128/header.highrange))/10) end
   local subcounter = 0

   for counter = 1,pattern_15_number_of_notes do
      new_duration[counter] = duration[counter] * ((math.log(header.highrange,10))*4000)
   end
   
   for counter = 1,pattern_15_number_of_notes do 
      local y_position = 1 
      local radius = notes[counter] 
      horizontal = horizontal + radius 
      pattern_15_lilypond_score = pattern_15_lilypond_score.."\\draw[fill=black] ("..horizontal.."pt,"..y_position..") circle [radius = "..radius.."pt];\n"
      
      horizontal = horizontal + radius + (new_duration[counter]/100)
      
      if horizontal > 350 then --460 then --500 then 
	 horizontal = 0 --new_duration[counter]/300
	 pattern_15_lilypond_score = pattern_15_lilypond_score .."\n\\end{tikzpicture}\\\\\n\\begin{tikzpicture}\n\\hskip"..(new_duration[counter]/300) .."pt "
	 
      end
   end

   pattern_15_lilypond_score = pattern_15_lilypond_score .. "\n\\end{tikzpicture}\n\n\\bigskip\\begin{center}\nNearby sounds can be clustered at the performer's discretion.\n\nThe meaning and application of ``magnitude\'' is up to the performer."

   local footer = latex_footer()
   pattern_15_lilypond_score = pattern_15_lilypond_score ..footer .."\n\\end{center}\n\\end{document}"

   return pattern_15_lilypond_score,latex_name

end -- function
