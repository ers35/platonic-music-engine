    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Pattern 35 from Robert Kirkpatrick's *The Book of Musical Patterns*
-- http://www.spiralcage.com/hollowearthrecordings/bomp/bompDownloads.html
-- Note! This is my homage to his work and does not reflect his methods!

    -- The Book of Musical Patterns
    -- All scores composed by Robert j Kirkpatrick 2006-2008
    -- Usage Restrictions
    -- All of these scores can be freely performed, recorded or studied without any licensing, notification or permissions
    -- so long as the composer is cited.
    -- Individual scores, up to the entire book of scores, can be reproduced for individual, group or educational purposes.
    -- No scores from the book, or the book itself, can be sold or published in other works without citation.
    -- Individual scores can be reproduced in whole or in part for album covers, liner notes,  web pages,
    -- essays, articles or books as long as citation of the composer is included.
    -- In general I encourage any and all uses of these scores; I want them performed, recorded and heard.
    -- Notification of performances, recording and the like would be appreciated.
    -- Please address any questions or notification of recordings, performance or other pertinent information to 
    -- patterns@hollowearthrecordings.com
    -- Additionally I will publish any and all recordings of these scores on the Hollow Earth Recordings website. Details at 
    -- http://www.hollowearthrecordings.com/bomp/

midi2pattern_35 = function(notes,duration,velocity,number_of_notes,pattern_35_instrument)
   -- local low = header.lowrange
   -- local high = header.lowrange
   
   local title = "Music" ; local dedication = header.dedication
   local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
   local subtitle = "" ; local subsubtitle = "" ; local algorithm = "Pattern 35"
   local poet = "composed by Robert Kirkpatrick" ; local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z")
   local paper_size="letter" 
   local instrument = pattern_35_instrument:gsub("%a", string.upper, 1) 
   local font = "newcent" ; local meter = "for "..number_of_notes.." pools of sound" ; local piece = ""
   local temperament_name = ""
   local pretty_system = nil
   local wave_color = {}
   local solid_circle_color = {}
   
   local latex_name = (pattern_35_instrument.."_Book_of_Patterns_no_35") ; latex_name = latex_name:gsub("%s+", "_")

   local preamble = latex_preamble(font)

   local latex_title = latex_title(dedication,title,instrument,poet,composer,arranger,algorithm,meter,piece,temperament_name,pretty_system) 
   
   local pic_environment = "\n\n\\begin{tikzpicture}\n"

   local pattern_35_lilypond_score = preamble..latex_title..pic_environment

   local base_table = table.pack("white","white","white","white","white","white","red","blue","black!50!green")
   local solid_circle_color = quantize("solid_circle_color",base_table,number_of_notes)

   -- local base_table = table.pack("red","blue","black!50!green")
   -- local wave_color = quantize("wave_color",base_table,number_of_notes)

   -- for counter = 1, number_of_notes do
   --    if notes[counter] < (low + high)*(1/9) then
   -- 	 solid_circle_color[counter] = "red"
   --    elseif notes[counter] < (low + high)*(2/9) then
   -- 	 solid_circle_color[counter] = "blue"
   --    elseif notes[counter] < (low + high)*(3/9) then
   -- 	 solid_circle_color[counter] = "black!50!green"
   --    else
   -- 	 solid_circle_color[counter] = "white"
   --    end
   -- end

   for counter = 1, number_of_notes do
      local tmp = notes[counter] % 3 
      if tmp >= 0 and tmp <= 1 then
	 wave_color[counter] = "red"
      elseif tmp > 1 and tmp <= 2 then
	 wave_color[counter] = "blue"
      elseif tmp > 2 and tmp <= 3 then
	 wave_color[counter] = "black!50!green"
      else os.exit()
      end
   end
   
   -- for counter = 1, number_of_notes do
   --    if notes[counter] < (low + high)*(1/3) then
   -- 	 wave_color[counter] = "red"
   --    elseif notes[counter] < (low + high)*(2/3) then
   -- 	 wave_color[counter] = "blue"
   --    else
   -- 	 wave_color[counter] = "black!50!green"
   --    end
   -- end

   local center_adjustment = platonic_generator("center_adjustment",1,4096,number_of_notes * 6,"yes")
   
   local horizontal = 0 ; metacounter = 1
   for counter = 1,number_of_notes do
      horizontal = horizontal + duration[counter]*10
      y_position = velocity[counter]/128
      radius = 35
      for subcounter = 0,25,5 do 
	 adjustment = center_adjustment[metacounter]/32767 
	 pattern_35_lilypond_score = pattern_35_lilypond_score.."\\draw["..wave_color[counter].."] ("..horizontal+adjustment.."pt,"..y_position+adjustment..") circle [radius = "..radius + subcounter.."pt];\n"
	 metacounter = metacounter + 1
      end
      pattern_35_lilypond_score = pattern_35_lilypond_score.."\\fill[fill = "..solid_circle_color[counter].."]("..horizontal.."pt,"..y_position..") circle [radius = 10pt];\n"
      
      horizontal = horizontal + (2 * radius) + 50

      if counter % 3 == 0 then 
	 horizontal = duration[counter]/300
	 pattern_35_lilypond_score = pattern_35_lilypond_score .."\n\\end{tikzpicture}\\\\\n\\begin{tikzpicture}\n\\hskip"..(duration[counter]/300) .."pt "
      end
      
   end

   pattern_35_lilypond_score = pattern_35_lilypond_score .. "\n\\end{tikzpicture}"

   local footer = latex_footer()
   pattern_35_lilypond_score = pattern_35_lilypond_score .. footer .."\n\\end{document}"

   return pattern_35_lilypond_score,latex_name
end
