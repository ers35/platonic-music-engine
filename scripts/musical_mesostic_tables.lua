    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

musical_mesostic_seed = {}
musical_mesostic_seed = {
   ["Fifth Symphony"] = {motif = "P5,P5,P5,m3",composer = "Ludwig v Beethoven"},
   ["Fifth Symphony (long)"] = {motif = "P5,P5,P5,m3,P4,P4,P4,M2",composer = "Ludwig v Beethoven"},
   ["Eine Kleine Nachtmusik"] = {motif = "P4,P1,P4,P1,P4,P1,P4,M6,P8,m7,P5,m7,P5,m7,P5,M3,P5,P1",composer = "Wolfgang A Mozart"},
   ["Toccata"] = {motif = "m6,TT,m6,TT,M3,m3,m2,P1,m2",composer = "JS Bach"},
}
