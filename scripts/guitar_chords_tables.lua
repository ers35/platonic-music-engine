    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Simple Guitar Chord Lilypond tables
major_degrees = {}
major_degrees = {
   [0] = "major_chords",
   [2] = "minor_chords",
   [4] = "minor_chords",
   [5] = "major_chords",
   [7] = "major_chords",
   [9] = "minor_chords",
   [11] = "diminished_chords",
}

minor_degrees = {}
minor_degrees = {
   [0] = "minor_chords",
   [2] = "diminished_chords",
   [3] = "major_chords",
   [5] = "minor_chords",
   [7] = "minor_chords",
   [8] = "major_chords",
   [10] = "major_chords",
}

harmonic_minor_degrees = {}
harmonic_minor_degrees = {
   [0] = "major_chords",
   [2] = "diminished_chords",
   [3] = "augmented_chords",
   [5] = "minor_chords",
   [7] = "major_chords",
   [8] = "major_chords",
   [11] = "diminished_chords",
}

major_chords = {}
major_chords = {
   ["Simple"] = "",
   ["Jazz"] = ":maj7,:maj9,:maj11,:maj13",
   ["Power"] = ":1.5",
}

minor_chords = {}
minor_chords = {
   ["Simple"] = ":m",
   ["Jazz"] = ":m7,:m9,:m11,:m13",
   ["Power"] = ":1.5",
}

diminished_chords = {}
diminished_chords = {
   ["Simple"] = ":dim",
   ["Jazz"] = ":dim7,:dim9,:dim11,:dim13",
   ["Power"] = ":1.5",
}

augmented_chords = {}
augmented_chords = {
   ["Simple"] = ":aug",
   ["Jazz"] = ":aug7,:aug9,:aug11,:aug13",
   ["Power"] = ":1.5",
}

style_options = {}
style_options = {
   ["Simple"] = 1,
   ["Jazz"] = 4,
   ["Power"] = 1,
}

-- major_table_degree = {}
-- major_table_degree = {
--       [0] = "",
--       [2] = ":m",
--       [4] = ":m",
--       [5] = "",
--       [7] = "",
--       [9] = ":m",
--       [11] = ":dim",
--    }

-- minor_table_degree = {}
-- minor_table_degree = {
--       [0] = ":m",
--       [2] = ":dim",
--       [3] = "",
--       [5] = ":m",
--       [7] = ":m",
--       [8] = "",
--       [10] = "",
--    }

-- harmonic_minor_table_degree = {}
-- harmonic_minor_table_degree = {
--       [0] = ":m",
--       [2] = ":dim",
--       [3] = ":aug",
--       [5] = ":m",
--       [7] = "",
--       [8] = "",
--       [11] = ":dim",
--    }
