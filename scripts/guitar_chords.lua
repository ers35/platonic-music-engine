    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Simple guitar chords. Emphasis on "Simple". Needs to be in major, minor, or harmonic minor.
-- Can be expanded but should it?

-- No longer just "Simple"! Now Jazz and Power chords and it's easy to add the rest.

simple_guitar_lilypond = function(style,key,Instrument,clef,notes,velocity,duration,future,tempo_word,tempo_number,numberofnotes,pretty_system) 
   local degree,mode_lily = table.unpack(key:split(","))
   local lilypond_key = degree.." \\"..mode_lily
   local lilypond_note = {} ; local lilypond_final_score = ""
   local chords = "" ; local simple_chords = "" ; local simple_velocity = {} ; local lilypond_table = {}
   local current_velocity = "" ; local simple_velocity = "" ; local flag = ""
   local chord_number = {}
   if style == "Power" then lilychordmode = "\\powerChords" else lilychordmode = "" end
   local num_of_chords = tonumber(style_options[style]) 
   local pitch_adjust = "pitch_number_"..tostring(baserange)

   tmp_table = {}
   for counter = 1,num_of_chords do
      tmp_table[counter] = counter
   end
   local basestring = table.concat(tmp_table,",") 
   local quantname = shallowcopy(future) ; local quantsize = quantizing_size.future
   chord_number = quantize(basestring,quantname,quantsize,numberofnotes)
      
   pitch_adjust = Omni_Pitch_Number[degree] 
   for counter = 1,numberofnotes do
      local tmp_note = notes[counter] % 12 
      lilypond_note[counter] = tmp_note 
      local adj_tmp_note = math.ceil((tmp_note - pitch_adjust) % 12) 
      local mode_table = mode_lily .. "_degrees" ; mode_table = string.gsub(mode_table," ","_")
      
      base_flag = _G[mode_table][adj_tmp_note] 
      base_flag_string = _G[base_flag][style] 
      base_flag_table = base_flag_string:split(",")
      
      index = tonumber(chord_number[counter])
      
      flag = base_flag_table[index] 
      lilypond_table[counter] = lilypond_note_table[tmp_note]
      
      duration_of_note = math.floor(duration[counter])
      simple_velocity = lilypond_velocity_list[math.floor(velocity[counter])]
      if simple_velocity == current_velocity then simple_velocity = ""
      else current_velocity = simple_velocity
      end
      
      simple_chords = simple_chords .. lilypond_table[counter]
      simple_chords = simple_chords .. lilypond_duration_list[duration_of_note]
      simple_chords = simple_chords .. simple_velocity .." "
      
      chords = chords .. lilypond_table[counter]
      chords = chords .. lilypond_duration_list[duration_of_note]
      chords = chords .. flag .. " "
            
   end 

   chords = chords .. "\\bar \"|.\"\n"
   -- Let's make that Lilypond score now!

   local title = header_info.title ; local dedication = header_info.dedication
   local composer = header_info.composer ; local arranger = header_info.arranger
   local algorithm = style.." Guitar Chords and Rhythm" ; local subsubtitle = ""
   local poet = "" ; local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z")
   local time = "4/4" ; local tempo_word = tempo_word:gsub("%a", string.upper, 1) 
   local piece = "" ; local meter = "" ; local paper_size = header_info.paper

   local degree,mode_lily = table.unpack(key:split(","))
   if mode_lily == "harmonic minor" then mode_lily = "minor" end
   local lilypond_key = degree.." \\"..mode_lily

   local lilypond_header = lilypond2header(Instrument,temperament_name,composer,arranger,poet,meter,piece,algorithm,pretty_system)
   lilypond_header = lilypond_header .. "\n\\include \"predefined-guitar-fretboards.ly\"\n\n"

   local lilypond_score = "\n\nmelody = {\n\t\\key "..lilypond_key.."\n\t\\clef \""..clef.."\"\n\t\\time "..time.."\n\t\\tempo \""..tempo_word.."\" 4 = "..tempo_number.."\n}\n" 

   local lilypond_footer = "\n\n\\paper {#(set-paper-size \""..paper_size.."\")}\\score {\n<<\n\t \\new ChordNames { \n\t\t \\chordmode { "..lilychordmode.." \n\t\t\t"..chords.."\n\t\t} \n\t}\n\t\\new FretBoards {\n\t\t\\chordmode { "..lilychordmode.." \n\t\t\t"..chords.."\n\t\t} \n\t}\n\t\\new Voice \\with {\\consists \"Pitch_squash_engraver\" \\remove \"Forbid_line_break_engraver\"}\n\t{\n\t\\melody\n\t\t\\relative c'' { \n\t\t\t\\improvisationOn \n\t\t\t"..simple_chords.."\n\t\t}\n\t}\n>>\n\t\\layout{ragged-bottom = ##t ragged-right = ##t \\context { \\Voice \\remove \"Note_heads_engraver\" \\consists \"Completion_heads_engraver\" \\remove \"Rest_engraver\" \\consists \"Completion_rest_engraver\"}}\n\t\\midi{}\n}" 

   local lilypond_final_score = lilypond_final_score .. lilypond_header..lilypond_score..lilypond_footer

   return lilypond_final_score
end -- function
