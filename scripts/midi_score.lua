    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

midi2midi_score = function(tuning,note,freq,duration,tempo,velocity,bend,instrument_name,algorithm,number_of_notes,midi_pitch_tuned_to,ratio_list,ratio_table,calculate,pretty_system) 
   local dedication = DEDICATION ; local title = header_info.title
   local poet = "MIDI Score" ; local composer = header_info.composer
   local arranger = header_info.arranger
   local font = "newcent" 
   local meter = "" ; local piece = ""
         
   local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z") ; local paper_size="letter"
   local Midi_instrument = (instrument_name):gsub("%a", string.upper, 1)

   local preamble = latex_preamble(font)

   local latex_name = (Midi_instrument.."_"..algorithm.."_MIDI_Score") ; latex_name = latex_name:gsub("%s+", "_")
   
   local latex_title = latex_title(dedication,title,Midi_instrument,poet,composer,arranger,algorithm,meter,piece,tuning,pretty_system)

   local midi_score = preamble..latex_title.."\n"
   
   midi_score = midi_score .. "\n\\begin{longtable}\n"
   midi_score = midi_score .. "{\n@{}\n>{\\raggedright}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"   
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering\\arraybackslash}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n}"
   midi_score = midi_score .. "\n\\multicolumn{7}{c}{\\textbf{\\Large{Music}}}\\\\"
   midi_score = midi_score .. "\nNumber & \\multicolumn{2}{c}{Pitch} & \\multicolumn{2}{c}{Duration} & Volume & Bend\\\\"
   midi_score = midi_score .. "\n& MIDI & Frequency & Ticks & Seconds && \\endhead"
   midi_score = midi_score .. "\n\\multicolumn{7}{r}{\\textit{cont.}}\\endfoot"
   midi_score = midi_score .. "\n\\multicolumn{7}{c}{}\\endlastfoot"
   midi_score = midi_score .. "\n& $(0-127)$ & $(Hz)$ & $(0-32767)$ && $(0-127)$ & $(0-4095)$"
   for counter = 1,number_of_notes do
      local freq_num = round(note[counter]) 
      freq_numb = freq[freq_num] 
      if not freq_numb then freq_numb=1 end
      freq_numb = math.ceil(round(freq_numb))
      local seconds = round((duration[counter])/(TICKS_PER_BEAT/(60/tempo)),2)
      midi_score = midi_score.."\\\\ \n"
      midi_score = midi_score.."\n $"..counter.."$ "
      midi_score = midi_score.."& $"..math.ceil(note[counter]).."$ & $"..freq_numb.."$ & $"..math.ceil(duration[counter]).."$ & $"..seconds.."$ & $"..math.ceil(velocity[counter]).."$ & $"..math.ceil(bend[counter]).."$ "
   end
   
   midi_score = midi_score .."\n\\end{longtable}\n\\break\n"

   if calculate ~= "no" then
      midi_score = midi_score .. "\n\\begin{longtable}\n"
      midi_score = midi_score .. "{\n@{}\n>{\\centering}p{.1\\textwidth}"
      midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
      midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"   
      midi_score = midi_score .. "\n@{}\n>{\\centering\\arraybackslash}p{.1\\textwidth}"
      midi_score = midi_score .. "\n@{}\n}"
      midi_score = midi_score .. "\n\\multicolumn{4}{c}{\\textbf{\\Large{Scale Degrees}}}\\\\"
      midi_score = midi_score .. "\nDegree & Number & \\% of Just Intonation & \\% of Standard Tuning\\endhead"
      midi_score = midi_score .. "\n\\multicolumn{4}{r}{\\textit{cont.}}\\endfoot"
      midi_score = midi_score .. "\n\\multicolumn{4}{c}{}\\endlastfoot"
      midi_score = midi_score .. "\nTonic & $"..Pitch_Number_Omni.perfect_unison + 1 .."$ & $100.0$ & $100.0$\\\\"
      if Pitch_Number_Omni.minor_second + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.minor_second then midi_score = midi_score .. "\nMinor 2nd & $"..prefix..Pitch_Number_Omni.minor_second + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.minor_second]/platonic_degree.m2),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.minor_second]/standard_tuning_degree.m2),2).."$\\\\" end
if Pitch_Number_Omni.major_second + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.major_second then midi_score = midi_score .. "\nMajor 2nd & $"..prefix..Pitch_Number_Omni.major_second + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_second]/platonic_degree.M2),2).." $ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_second]/standard_tuning_degree.M2),2).."$\\\\" end
      if Pitch_Number_Omni.minor_third + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.minor_third then midi_score = midi_score .. "\nMinor 3rd & $"..prefix..Pitch_Number_Omni.minor_third + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.minor_third]/platonic_degree.m3),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.minor_third]/standard_tuning_degree.m3),2).."$\\\\" end
      if Pitch_Number_Omni.major_third + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.major_third then midi_score = midi_score .. "\nMajor 3rd & $"..prefix..Pitch_Number_Omni.major_third + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_third]/platonic_degree.M3),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_third]/standard_tuning_degree.M3),2).."$\\\\" end
      if Pitch_Number_Omni.perfect_fourth + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.perfect_fourth then midi_score = midi_score .. "\nPerfect 4th & $"..prefix..Pitch_Number_Omni.perfect_fourth + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.perfect_fourth]/platonic_degree.P4),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.perfect_fourth]/standard_tuning_degree.P4),2).."$\\\\" end
      if Pitch_Number_Omni.tri_tone + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.tri_tone then midi_score = midi_score .. "\nTritone & $".. prefix .. Pitch_Number_Omni.tri_tone + 1  .."$ & $".. prefix .. round(100*(ratio_list[Pitch_Number_Omni.tri_tone]/platonic_degree.TT),2).."$ &$".. prefix ..round(100*(ratio_list[Pitch_Number_Omni.tri_tone]/standard_tuning_degree.TT),2).."$\\\\" end
      if Pitch_Number_Omni.perfect_fifth + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.perfect_fifth then midi_score = midi_score .. "\nPerfect 5th & $".. prefix.. Pitch_Number_Omni.perfect_fifth + 1  .."$ & $".. prefix .. round(100*(ratio_list[Pitch_Number_Omni.perfect_fifth]/platonic_degree.P5),2).."$ &$"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.perfect_fifth]/standard_tuning_degree.P5),2).."$\\\\" end
      if Pitch_Number_Omni.minor_sixth + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.minor_sixth then midi_score = midi_score .. "\nMinor 6th & $".. prefix .. Pitch_Number_Omni.minor_sixth + 1  .."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.minor_sixth]/platonic_degree.m6),2).."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.minor_sixth]/standard_tuning_degree.m6),2).."$\\\\" end
      if Pitch_Number_Omni.major_sixth + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.major_sixth then midi_score = midi_score .. "\nMajor 6th & $"..prefix..Pitch_Number_Omni.major_sixth + 1  .."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.major_sixth]/platonic_degree.M6),2).."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.major_sixth]/standard_tuning_degree.M6),2).."$\\\\" end
      if Pitch_Number_Omni.minor_seventh + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.minor_seventh then midi_score = midi_score .. "\nMinor 7th & $"..prefix .. Pitch_Number_Omni.minor_seventh + 1  .."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.minor_seventh]/platonic_degree.m7),2).."$ & $"..prefix .. round(100*(ratio_list[Pitch_Number_Omni.minor_seventh]/standard_tuning_degree.m7),2).."$\\\\" end
      if Pitch_Number_Omni.major_seventh + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.major_seventh then midi_score = midi_score .. "\nMajor 7th & $".. prefix.. Pitch_Number_Omni.major_seventh + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_seventh]/platonic_degree.M7),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.major_seventh]/standard_tuning_degree.M7),2).."$\\\\" end
      if Pitch_Number_Omni.perfect_octave + 1 > 128 then prefix = "\\color{gray}" else prefix = "" end
      if Pitch_Number_Omni.perfect_octave then midi_score = midi_score .. "\nOctave & $"..prefix..Pitch_Number_Omni.perfect_octave + 1  .."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.perfect_octave]/platonic_degree.P8),2).."$ & $"..prefix..round(100*(ratio_list[Pitch_Number_Omni.perfect_octave]/standard_tuning_degree.P8),2).."$\\\\" end
      
      midi_score = midi_score .."\n\\end{longtable}\n" 

   end
   
   local pitch,mode = table.unpack(tuning:split(","))
   if pitch ~= "=" and pitch ~= "$" and just_intonation_ratio_list[mode].ratiotype ~= "cents" then
      midi_score = midi_score.."\n\\centering{\\textbf{\\Large{Tuning Information}}}\\\\"
      midi_score = midi_score.."\n\\centering{"..just_intonation_ratio_list[mode].pretty_name.." built from "..string.upper(pitch).."}\\\\"
      midi_score = midi_score.."\n\\centering{Ratios (\\& Cents) used:}\n\n"
      ratio_table = {}
      ratio_table = just_intonation_ratio_list[mode].latex:split(",")
      midi_score = midi_score.."\n"--\\Large{"
      for counter = 1,#ratio_table do
	 midi_score = midi_score.."{$"..ratio_table[counter].."\\ ("..round((1200*(math.log(ratio_list[counter])/math.log(2))),2)..")"
	 midi_score = midi_score.."$}"
	 if counter < #ratio_table then midi_score = midi_score..", " end
      end
   elseif pitch =="=" then
      midi_score = midi_score.."\n\\centering{\\textbf{\\Large{Tuning Information}}}\\\\"
      midi_score = midi_score.."\n\\centering{"..just_intonation_ratio_list["n-edo"].pretty_name.."}\\\\"
      midi_score = midi_score.."\n\\centering{Ratios (\\& Cents) used:}\n\n"
      ratio_table = {}
      ratio_table = just_intonation_ratio_list["n-edo"].latex:split(",")
      midi_score = midi_score.."\n"--\\Large{"
      for counter = 1,#ratio_table do
	 midi_score = midi_score.."{$"..ratio_table[counter].."\\ ("..round((1200*(math.log(ratio_list[counter])/math.log(2))),2)..")"
	 midi_score = midi_score.."$}"
	 if counter < #ratio_table then midi_score = midi_score..", " end
      end
      
   elseif pitch == "$" then 
      midi_score = midi_score.."\n\\centering{\\textbf{\\Large{Tuning Information}}}\\\\"
      midi_score = midi_score.."\n\\centering{"..just_intonation_ratio_list["n-cet"].pretty_name.."}\\\\"
      midi_score = midi_score.."\n\\centering{Cents (\\& Ratios) used:}\n\n"
      ratio_table = {}
      ratio_table = just_intonation_ratio_list["n-cet"].latex:split(",")
      midi_score = midi_score.."\n"--\\Large{"
      for counter = 1,#ratio_table do
	 midi_score = midi_score.."{$"..ratio_table[counter].."\\ ("..round(2^(ratio_table[counter]/1200),2)..")"
	 midi_score = midi_score.."$}"
	 if counter < #ratio_table then midi_score = midi_score..", " end
      end
   elseif just_intonation_ratio_list[mode].ratiotype == "cents" then
      midi_score = midi_score.."\n\\centering{\\textbf{\\Large{Tuning Information}}}\\\\"
      midi_score = midi_score.."\n\\centering{"..just_intonation_ratio_list[mode].pretty_name.."}\\\\"
      midi_score = midi_score.."\n\\centering{Cents (\\& Ratios) used:}\n\n"
      ratio_table = {}
      ratio_table = just_intonation_ratio_list[mode].latex:split(",")
      midi_score = midi_score.."\n"--\\Large{"
      for counter = 1,#ratio_table do
	 midi_score = midi_score.."{$"..ratio_table[counter].."\\ ("..round(2^(ratio_table[counter]/1200),2)..")"
	 midi_score = midi_score.."$}"
	 if counter < #ratio_table then midi_score = midi_score..", " end
      end
      
   end
   
      
--   midi_score = midi_score.."\\]"
   
   midi_score = midi_score .. "\n\\begin{longtable}\n"
   midi_score = midi_score .. "{\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n>{\\centering\\arraybackslash}p{.1\\textwidth}"
   midi_score = midi_score .. "\n@{}\n}"
   midi_score = midi_score .. "\n\\multicolumn{4}{c}{\\textbf{\\Large{Timidity Tuning Table}}}\\\\"
   midi_score = midi_score .. "\n& MIDI Number & Audio Frequency \\endhead"
   midi_score = midi_score .. "\n\\multicolumn{4}{r}{\\textit{cont.}}\\endfoot"
   midi_score = midi_score .. "\n\\multicolumn{4}{c}{}\\endlastfoot"
   local subcounter = 1
   for counter = 0,127 do
      local freq_num = tonumber(freq[counter])
      freq_num = math.ceil(round(freq_num))
      midi_score = midi_score.."\\\\ \n"
      local prefix = "" ; local suffix = ""
      if midi_pitch_tuned_to == counter then
	 prefix = "\\textbf{"
	 suffix = "}"
      end
      midi_score = midi_score.."\n& $"..prefix..counter..suffix.."$ "
      midi_score = midi_score.."& $"..prefix..freq_num..suffix.."$ & $ $"--..ratio_table[subcounter].."$ "
      subcounter = subcounter + 1
      if subcounter == 13 then subcounter = 1 end
   end

   midi_score = midi_score .."\n\\end{longtable}\n"
   
   local footer = latex_footer()
   local midi_score = midi_score .. footer .. "\n\\end{document}"

   return midi_score,latex_name

end -- function

