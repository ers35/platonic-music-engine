get_args = function(args,input_command,gen_log,gen_lilypond,gen_feldman,gen_pattern_15,gen_pattern_35,gen_ogg,gen_flac)
   for counter = 1,#args do
      if args[counter] == "--log" then gen_log = "yes" end
      if args[counter] == "-l" or args[counter] == "--lilypond" then gen_lilypond = "yes" end
      if args[counter] == "-F" or args[counter] == "--Feldman" then gen_feldman = "yes" end
      if args[counter] == "-p15" or args[counter] == "--pattern15" then gen_pattern_15 = "yes" end
      if args[counter] == "-p35" or args[counter] == "--pattern35" then gen_pattern_35 = "yes" end
      if args[counter] == "-o" or args[counter] == "--ogg" then gen_ogg = "yes" end
      if args[counter] == "-f" or args[counter] == "--flac" then gen_flac = "yes" end
      if args[counter] == "-i" or args[counter] == "--interactive" then input_command = "" end
      if args[counter] == "-r" or args[counter] == "--random" then input_command = "r" end
      if args[counter] == "-u" or args[counter] == "--use" then input_command = args[counter +1] end
      if args[counter] == "-h" or args[counter] == "--help" then
	 print("The following flags are for notation styles")
	 print("-l, --lilypond \t\tgenerate Lilypond file (standard music notation)")
	 print("-F, --Feldman \t\tgenerate Feldman graph notation")
	 print("-p15, --pattern15 \tgenerate Kirkpatrick graphic notation Pattern 15")
	 print("-p35, --pattern35 \tgenerate Kirkpatrick graphic notation Pattern 35")
	 print("----------------------------")
	 print("The following flags are for audio files")
	 print("-o, --ogg \t\tgenerate ogg audio file")
	 print("-f, --flac \t\tgenerate flac audio file")
	 print("----------------------------")
	 print("The following flags are for entering the initial input")
	 print("-i, --interactive \tinteractive mode that asks for initial input")
	 print("-r, --random \t\tuses randomly generated string for initial input")
	 print("-u, --use \"input\" \tuses value in \"input\" for initial input")
	 print("----------------------------")
	 print("--log \t\t\tgenerates log file with interesting information")
	 print("-h, --help \t\tprints out this helpful information")
	 os.exit()
      end
   end
   return input_command,gen_log,gen_lilypond,gen_feldman,gen_pattern_15,gen_pattern_35,gen_ogg,gen_flac
end


