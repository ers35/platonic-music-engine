    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This function gets the latitude and longitude from the user.
-- If the decimal places are less than 13 then they are padded randomly.
-- Or the user can enter "r" at any time to have the computer choose a value randomly
-- All of these options can be passed with the "command" parameter so passing "r"
-- or two numbers separated by spaces will cause the function to compute values
-- without interacting with the user. Only if command = "" will there be an actual
-- interaction.

function get_input(command)
  
   if command == "" then
      mistake = 1 
      while mistake == 1  do
	 io.write("Enter longitude (between -180 and 180 or r): ")
	 long=io.read() checklong = tonumber(long) 
	 if long == "r" then mistake = 0 break end
	 if (checklong >= -180 and checklong <= 180)  then mistake = 0 end
      end
      if not string.find(long,"%.") then long = tostring(long..".") end
      longint,longfrac = unpack(long:split("%.")) ; long = longint
   end

   if command == "r" or long == "r" then
      longint = math.random(-180,180) ; longfrac = tostring(math.random(0,9)) ; long = longint
   end

   if #command > 1 then longitude,latitude = unpack(command:split(" ")) 
      longint,longfrac = unpack(longitude:split("%.")) ; latint,latfrac = unpack(latitude:split("%.")) ; long = longint ; lat = latint
   end

   while (#longfrac) < 13 do
      longfrac = (longfrac .. tostring(math.random(0,9)))
   end

   if command == "" then
      mistake = 1 
      while mistake == 1  do
	 io.write("Enter latitude (between -90 and 90 or r): ")
	 lat=io.read() checklat = tonumber(lat) 
	 if lat == "r" then mistake = 0 break end
	 if (checklat >= -90 and checklat <= 90)  then mistake = 0 end
      end
      if not string.find(lat,"%.") then lat = tostring(lat..".") end
      latint,latfrac = unpack(lat:split("%.")) ; lat = latint
   end

   if command == "r" or lat == "r" then
      latint = math.random(-90,90) ; latfrac = tostring(math.random(0,9)) ; lat = latint
   end

   while (#latfrac) < 13 do
      latfrac = (latfrac .. tostring(math.random(0,9)))
   end

   long = (tostring(longint)..".".. longfrac)
   lat = (tostring(latint).."."..latfrac)
   local lat_long_string = (long.." "..lat)
   
return lat_long_string

end -- function Get_User_Input
