    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Lilypond duration list
lilypond_duration_list = {}
lilypond_duration_list = {
   [8] = "\\breve",
   [6] = "1.",
   [4] = "1",
   [3.75] = "2...",
   [3.5] = "2..",
   [3] = "2.",
   [2] = "2",
   [1.75] = "4..",
   [1.5] = "4.",
   -- [120] = "4~ ",
   [1] = "4",
   [.75] = "8.",
   [.5] = "8",
   [.425] = "16.",
   [.25] = "16",
   [.125] = "32",
   [.0625] = "64",
   [.03125] = "128",
   [-1] = "chord",
}

-- Lilypond velocity table list
lilypond_velocity_list = {}
lilypond_velocity_list = {
   
   [0] = "r",
   [5] = "\\ppppp",
   [10] = "\\pppp",
   [17] = "\\ppp",
   [24] = "\\pp",
   [32] = "\\p",
   [40] = "\\mp",
   [50] = "\\mf",
   [60] = "\\f",
   [72] = "\\ff",
   [88] = "\\fff",
   [107] = "\\ffff",
   [127] = "\\fffff",
}

base_lilypond_ratio_table = {}
base_lilypond_ratio_table = {
   [1] = 1,
   [2] = 1.1224620483094,
   [3] = 1.1892071150027,
   [4] = 1.2599210498949,
   [5] = 1.4142135623731,
   [6] = 1.5874010519682,
   [7] = 1.8877486253634,
}

base_lilypond_note_table = {}
base_lilypond_note_table = {
   [1] = "c",
   [2] = "d",
   [3] = "e",
   [4] = "f",
   [5] = "g",
   [6] = "a",
   [7] = "b",
}

enharmonic_lilypond_ratio_table = {}
enharmonic_lilypond_ratio_table = {
   [1] = 1,
   [2] = 1.0594630943593,
   [3] = 1.1224620483094,
   [4] = 1.1892071150027,
   [5] = 1.2599210498949,
   [6] = 1.33483985417,
   [7] = 1.4142135623731,
   [8] = 1.4983070768767,
   [9] = 1.5874010519682,
   [10] = 1.6817928305074,
   [11] = 1.7817974362807,
   [12] = 1.8877486253634,
   -- [13] = 2,
}

enharmonic_lilypond_note_table = {}
enharmonic_lilypond_note_table = {
   [1] = "c",
   [2] = "cs",
   [3] = "d",
   [4] = "ds",
   [5] = "e",
   [6] = "f",
   [7] = "fs",
   [8] = "g",
   [9] = "gs",
   [10] = "a",
   [11] = "as",
   [12] = "b",
   -- [13] = "c",
}

nonenharmonic_lilypond_ratio_table = {}
nonenharmonic_lilypond_ratio_table = {
[1] = 1,               --c
[2] = 1.0162654963091, --1.0129187947249, --cqs
[3] = 1.03279555899, --1.0260044847069, --cs
[4] = 1.04959449134, -- 1.0392592260317, --ctqs
[5] = 1.0666666666666, --526852026388, -- cx
[6] = 1.08096012385, --1.080059738892, --dtqf
[7] =  1.09544511502, --1.0940128089494, --df
[8] =  1.11012420675, --1.1081461358546, --dqf
[9] = 1.125,--24620483089, --d
[10] = 1.14329868335,
[11] = 1.16189500387,
[12] = 1.18079380277,
[13] = 1.2,--1815991887784,
[14] = 1.21230930281,
[15] = 1.2247448714,
[16] = 1.23730800097,
[17] = 1.25,--99210498938,
[18] = 1.27033187039,
[19] = 1.29099444874,
[20] = 1.31199311418,
[21] = 1.3333333333333,
[22] = 1.35310905988,
[23] = 1.37317809595,
[24] = 1.39354479184,
[25] = 1.41421356239,
[26] = 1.43518888791,
[27] = 1.45647531515,
[28] = 1.47807745832,
[29] = 1.5,
[30] = 1.52439824447,
[31] = 1.54919333849,
[32] = 1.57439173702,
[33] = 1.6,
[34] = 1.61641240374,
[35] = 1.63299316186,
[36] = 1.64974400129,
[37] = 1.666666666666,
[38] = 1.69377582718,
[39] = 1.72132593165,
[40] = 1.74932415224,
[41] = 1.777777777777,
[42] = 1.80160020642,
[43] = 1.82574185837,
[44] = 1.85020701126,
[45] = 1.875,
[46] = 1.89018739427,
[47] = 1.90549780558,
[48] = 1.92093223035,
[49] = 1.93649167309,
[50] = 1.95217714644,
[51] = 1.96798967124,
[52] = 1.9839302766,
}

nonenharmonic_lilypond_note_table = {}
nonenharmonic_lilypond_note_table = {
   [1] = "c",
   [2] = "cqs",
   [3] = "cs",
   [4] = "ctqs",
   [5] = "cx",-- "dtqf",
   [6] = "dff",
   [7] = "dtqf",
   [8] = "df",
   [9] = "d",
   [10] = "dqs",
   [11] = "ds",
   [12] = "dtqs",
   [13] = "dx",
   [14] = "etqf",
   [15] = "ef",
   [16] = "eqf",
   [17] = "e",--"es",
   [18] = "eqs",
   [19] = "es", --"ff",
   [20] = "ff",
   [21] = "f", --"fqf",
   [22] = "fqs",
   [23] = "fs",
   [24] = "ftqs",
   [25] = "fx",
   [26] = "gtqf",
   [27] = "gf",
   [28] = "gqf",
   [29] = "g",--"fx",
   [30] = "gqs",
   [31] = "gs",
   [32] = "gtqs",
   [33] = "gx",
   [34] = "atqf",
   [35] = "af",
   [36] = "aqf",
   [37] = "a",--"aqs",
   [38] = "aqs",
   [39] = "as",
   [40] = "atqs",
   [41] = "ax",
   [42] = "btqf",
   [43] = "bf",
   [44] = "bqf",
   [45] = "b",--"bs",
   [46] = "bqs",
   [47] = "bs",
   [48] = "btqs",
   [49] = "bx",
   [50] = "ctqf",
   [51] = "cf",
   [52] = "cqf",
}

enharmonic_lilypond_tuning_table_rotate = {}
enharmonic_lilypond_tuning_table_rotate = {
   ["c"] = 0,
   ["cs"] = 1,
   ["df"] = 1,
   ["d"] = 2,
   ["ds"] = 3,
   ["ef"] = 3,
   ["e"] = 4,
   ["f"] = 5,
   ["fs"] = 6,
   ["gf"] = 6,
   ["g"] = 7,
   ["gs"] = 8,
   ["af"] = 8,
   ["a"] = 9,
   ["as"] = 10,
   ["bf"] = 10,
   ["b"] = 11,
}

nonenharmonic_lilypond_tuning_table_rotate = {}
nonenharmonic_lilypond_tuning_table_rotate = {
   ["c"] = 0,
   ["cs"] = 3,
   ["df"] = 6,
   ["d"] = 9,
   ["ds"] = 12,
   ["ef"] = 15,
   ["e"] = 17,
   ["f"] = 21,
   ["fs"] = 24,
   ["gf"] = 27,
   ["g"] = 29,
   ["gs"] = 32,
   ["af"] = 35,
   ["a"] = 37,
   ["as"] = 40,
   ["bf"] = 43,
   ["b"] = 45,
}

lilypond_octave_table = {}
lilypond_octave_table = {
   [1] = ",,,,,,",
   [2] = ",,,,,",
   [3] = ",,,,",
   [4] = ",,,",
   [5] = ",,",
   [6] = ",",
   [7] = "",
   [8] = "'",
   [9] = "''",
   [10] = "'''",
   [11] = "''''",
   [12] = "'''''",
}
     
