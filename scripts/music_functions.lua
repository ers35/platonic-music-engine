    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Mini-Super quantize function to REPLACE THEM ALL!!!!
-- quantize = function (basestring,quantname,quantsize,number_of_notes,low)

get_scale_octave = function(key,scale_degrees) -- Get baserange from mode
   local tmp_table={}
   local temperament_octave = header.tuning_octave
   -- if string.sub(key,1,1) == "#" then
   --    tmp_table = key:split(":")
   --    result = tmp_table[2] 
   --    key = tmp_table[1] 
   -- else
   _,mode = bifurcate(key,"-")
   if mode == "Panchromatic" then result = temperament_octave
   else
      local tmp_result = scale_key[mode].repeat_point
      result = scale_degrees[tmp_result] 
   end
   
   if result == nil then result = 12 end -- I wonder why this exists? I mean it's a nice fallback but this information should be in the table
   -- end 
   return result--,key
end  

instrument_range = function(instrument,freq_table,low_freq,high_freq)
   local low_match_value = midi_instrument_list[instrument].low_range
   local high_match_value = midi_instrument_list[instrument].high_range

   lowrange = tonumber(binary_search(low_match_value,low_freq,high_freq,freq_table)) 
   if freq_table[lowrange] < (low_match_value - .00000001) then lowrange = lowrange + 1 end
   highrange = tonumber(binary_search(high_match_value,low_freq,high_freq,freq_table))
   if freq_table[highrange] > (high_match_value + .1) then highrange = highrange -1 end
   
   local midi_num = midi_instrument_list[instrument].midi_instrument
   write_log("Instrument Range","lowrange",lowrange)
   write_log("Instrument Range","highrange",highrange)
   
   return midi_num,lowrange,highrange
end

quantize = function(table_name,base_table,number_of_notes)
   random = require("scripts/pcg")
   local result = {}
   local seed = header.user_data .. table_name
   
   do -- Random number generator
      random.seed(hash(seed),hashstream(seed))
      for counter = 1,number_of_notes do
	 local len_base_table = #(base_table)
	 if len_base_table == 0 then len_base_table = 1 end
	 local index = random.random(1,len_base_table) 
	 result[counter] = base_table[index] 
      end
   end
   write_log("music_functions.lua","quantize " .. table_name,result)
   return result
end

-- Bel Canto function
-- raises or lower successive notes by an octave to get them within a perfect fifth
-- of previous note. Fails at extremes of instrument range. The *normalize* parameter
-- if set to yes raises or lowers the first note to within a perfect fifth of the middle note
-- of the instrument. Otherwise the first note stays the same and everything after is altered.

belcanto = function(belcanto_string,calculated_scale_degrees,belcanto_baserange,belcanto_number_of_notes,normalize)
   local bel_number = calculated_scale_degrees.P5 --(math.floor(belcanto_baserange/2))-1 print(bel_number)
   local mid_number = (math.floor(header.lowrange + header.highrange)/2)
   
   if normalize == "yes" then start = 1 ; belcanto_string[0] = mid_number else start = 2 end 
   for counter = start,belcanto_number_of_notes do
      local secondnote = tonumber(belcanto_string[counter]) ; local firstnote=tonumber(belcanto_string[counter-1]) 
      while secondnote > firstnote + bel_number do secondnote = secondnote - belcanto_baserange end 
      while secondnote < firstnote - bel_number do secondnote = secondnote + belcanto_baserange end
      if secondnote > header.highrange then secondnote = secondnote - belcanto_baserange end
      if secondnote < header.lowrange then secondnote = secondnote + belcanto_baserange end
      
      belcanto_string[counter] = secondnote 
   end
   
   return(belcanto_string)
end --function

generate_panchromatic_scale = function(range,number)
   local pitch = {} ; local duration = {} ; local velocity = {}
   local start = 0 ; local stop = 0
   local octave = header.tuning_octave
   
   if range == "full" then
      start = header.lowrange ; stop = header.highrange
   elseif range == "octave" then
      start = 69 ; stop = 69 + octave
   else 
      print("Either full or octave") ; os.exit()
   end

   local increment = 1
   for counter = start,stop do
      pitch[increment] = counter
      velocity[increment] = 127
      duration[increment] = 1
      increment = increment + 1
   end

   local new_number = increment - 1

   return pitch,velocity,duration,new_number
end
