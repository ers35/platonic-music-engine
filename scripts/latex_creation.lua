    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

latex_preamble = function(font)
   io.write("Generating LaTeX score ...\r") ; io.flush()
   local latex = "\\documentclass[11pt]{memoir}"
   latex = latex .. "\n\\usepackage{tikz}"
   latex = latex .. "\n\\usepackage{textcomp}"
   latex = latex .. "\n\\usepackage[top=5mm,left=10mm,right=10mm,bottom=5mm]{geometry}"
   latex = latex .. "\n\\usepackage{"..font.."}"
   latex = latex .. "\n\\usepackage{microtype}"
   latex = latex .. "\n\\usepackage{longtable}"
   latex = latex .. "\n\\usepackage{amsmath}"
   latex = latex .. "\n\\usepackage{xfrac}"
   latex = latex .. "\n\\usepackage{fouriernc}"
   latex = latex .. "\n\\usepackage{relsize}"
   latex = latex .. "\n\\setlength{\\parindent}{0pt}"
   latex = latex .. "\n\\nonzeroparskip"
   return latex
end

latex_title = function(dedication,name,instrument,poet,composer,arranger,algorithm,meter,piece,temperament_name,pretty_system)
   local pitch,temperament = table.unpack(temperament_name:split",")  
   if temperament then
      if pitch == "=" then piece = temperament.."-EDO"
      elseif pitch == "$" then piece = temperament.."-cET"
      else piece = just_intonation_ratio_list[temperament].pretty_name .. " built from " .. string.upper(pitch)
      end
   elseif pitch then piece = pitch
      else piece = ""
   end
   if pretty_system then
      piece = piece .."\\\\using "..pretty_system
   end
   
   local title =    "\n\\begin{document}"
   title = title .. "\n\\begin{tabular}"
   title = title .. "\n{"
   title = title .. "\n@{}"
   title = title .. "\n>{\\raggedright}p{.33333\\textwidth}"
   title = title .. "\n@{}"
   title = title .. "\n>{\\centering}p{.33333\\textwidth}"
   title = title .. "\n@{}"
   title = title .. "\n>{\\raggedleft\\arraybackslash}p{.33333\\textwidth}"
   title = title .. "\n@{}"
   title = title .. "\n}"
   title = title .. "\n"
   title = title .. "\n& (For "..dedication..") & \\\\"
   title = title .. "\n& \\LARGE\\bfseries\\strut "..name.." & \\\\"
   title = title .. "\n& \\large\\bfseries\\strut "..algorithm.." & \\\\"
   title = title .. "\n"..poet.."& \\large\\bfseries "..instrument.." & ".. composer .. "\\\\"
   title = title .. "\n"..meter.."&& "..arranger .. "\\\\"
   title = title .. "\n"..piece.."&& \\\\"
   title = title .. "\n\\end{tabular}"
   return title
end 

-- Create LaTeX file given the LaTeX file and the name for the output file
create_latex = function(score,name)
   output_latex_name = io.open(name..".tex","w")
   output_latex_name:write(score)
   output_latex_name:close()
   os.execute("pdflatex" .. " "..name.." > latex.txt")
   io.write("Generating LaTeX score ... done.\n")
   return
end

-- Create LaTeX footer for copyright notice
latex_footer = function()
   local today = os.date("\\textcopyright{} %A %d %B %Y %I:%M:%S %p %Z %z")
   local footer = "\n\\vfill\\centering "..today.." \n"
   local footer = footer .. "This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License\n"
   return footer
end

