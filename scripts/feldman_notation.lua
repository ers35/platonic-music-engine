    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Feldman Graphic Notation:
-- Takes the pitches, velocities, and durations along with the number of notes, and the instrument range,
-- and creates a Feldman-style graph notation. 
-- The *volume* parameter adds a volume graphic that wasn't originally in Feldman's approach

midi2feldman = function(notes,duration,velocity,feldman_instrument,feldman_number_of_notes,volume,tuning_name,pretty_system,tempo_number)
   local feldman_pitch = {}
   local feldman_velocity = {}
   local low_number = header.lowrange
   local high_number = header.highrange
   
   local dedication = header.dedication ; local title = "Music"
   local poet = "Morton Feldman's notation" ; local composer = "David Bellows"
   local arranger = "Platonic Music Engine"
   local font = "newcent" ; local algorithm = "Feldman Graphic Notation"
   local meter = round(60/tempo_number,3) .. " second(s) per bar line" ; local piece = ""

   local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z") ; local paper_size="letter"
   local Feldman_instrument = feldman_instrument:gsub("%a", string.upper, 1)
   tuning_name = tuning_name .. " tuning"
 
   local preamble = latex_preamble(font)

   local latex_name = (Feldman_instrument.."_Feldman_Graph") ; latex_name = latex_name:gsub("%s+", "_")

   local latex_title = latex_title(dedication,title,Feldman_instrument,poet,composer,arranger,algorithm,meter,piece,tuning_name,header.reference_pitch_name)
   
   local pic_environment = "\n\n\\begin{tikzpicture}\n"

   local feldman_lilypond_score = preamble..latex_title..pic_environment

   local horizontal = 0 
   
   local low_third_pitch = ((high_number - low_number) * (1/3)) + low_number 
   local high_third_pitch = ((high_number - low_number) * (2/3)) + low_number 
   local low_third_velocity = 42 
   local high_third_velocity = 85

   for counter = 1,feldman_number_of_notes do
      if notes[counter] <= low_third_pitch then
	 feldman_pitch[counter] = 1
      elseif notes[counter] <= high_third_pitch then
	 feldman_pitch[counter] = 2
      else
	 feldman_pitch[counter] = 3
      end


      if volume == "yes" then
	 if velocity[counter] <= low_third_velocity then
	    feldman_velocity[counter] = "gray!20!white"
	 elseif velocity[counter] <= high_third_velocity then
	    feldman_velocity[counter] = "black!40!white"
	 else
	    feldman_velocity[counter] = "black!70!white"
	 end
      else
	 feldman_velocity[counter] = "gray!50!white"
      end
   end

   for counter = 1,feldman_number_of_notes do 
      local y_position = feldman_pitch[counter] 
      second_horizontal = (duration[counter]) + horizontal
      second_position = y_position + 1
      if tonumber(velocity[counter]) == 0 then
	 feldman_lilypond_score = feldman_lilypond_score.."\\fill [fill=white] ("..tonumber(horizontal+.1)..","..y_position..") rectangle ("..second_horizontal..","..second_position..");\n"
	 feldman_velocity[counter] = "rest"
      else
	 feldman_lilypond_score = feldman_lilypond_score.."\\filldraw [fill="..feldman_velocity[counter].."] ("..horizontal..","..y_position..") rectangle ("..second_horizontal..",".. second_position..");\n"
      end
      
      horizontal = second_horizontal 

      if counter == feldman_number_of_notes then -- Prints final bar line
	 feldman_lilypond_score = feldman_lilypond_score .."\\draw [line width = 1.0mm, black] ("..second_horizontal+.05 ..",1) -- ("..second_horizontal+.05 ..",4);\n"
      end

      if horizontal > 13 then --13.55 then
	 feldman_lilypond_score = feldman_lilypond_score .."\n\\draw (0,1) -- ("..horizontal ..",1);\n\\draw (0,4) -- ("..horizontal..",4);\n"
	 for counter = 0,horizontal do
	    feldman_lilypond_score = feldman_lilypond_score .."\\draw [dashed,gray] ("..counter..",1) -- ("..counter..",4);\n"
	 end
	 
	 horizontal = 0 

	 feldman_lilypond_score = feldman_lilypond_score .."\n\\end{tikzpicture}\n\\vspace{1cm}\\\\\n\\begin{tikzpicture}\n"
      end

   end
   -- This is for the last staff, it does not get included in the section above for very good reasons which I can no longer recall

   if horizontal ~= 0 then 
      feldman_lilypond_score = feldman_lilypond_score .."\n\\draw (0,1) -- ("..horizontal..",1);\n\\draw (0,4) -- ("..horizontal..",4);\n"
      for counter = 0,horizontal do
      	 feldman_lilypond_score = feldman_lilypond_score .."\\draw [dashed,gray] ("..counter..",1) -- ("..counter..",4);\n"
      end
   end

   feldman_lilypond_score = feldman_lilypond_score .. "\n\\end{tikzpicture}"

   local footer = latex_footer()
   feldman_lilypond_score = feldman_lilypond_score .. footer .. "\n\\end{document}"
   
   write_log("feldman_notation.lua","feldman_pitch",feldman_pitch)
   write_log("feldman_notation.lua","feldman_velocity",feldman_velocity)
   
   return feldman_lilypond_score,latex_name

end -- function

