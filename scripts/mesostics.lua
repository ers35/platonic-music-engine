-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

musical_mesostic = function(note,velocity,spine,repeat_motif,format,number_of_notes,calculated_scale_degrees)
   require("scripts/musical_mesostic_tables")

   local low = header.lowrange
   local high = header.highrange
   local tuning_octave = header.tuning_octave
   
   local lilypond_spot = {}
   local original_number_of_notes = number_of_notes
   local average = round((low + high)/2) 
   local scale_adjust = 69 + header.tuning_rotate
   
   while scale_adjust < average do
      scale_adjust = scale_adjust + tuning_octave
   end

   while scale_adjust > average do
      scale_adjust = scale_adjust - tuning_octave
   end

   local seed = musical_mesostic_seed[spine].motif 
   seed = seed:split(",") 

   for counter = 1,#seed do seed[counter] = tonumber(calculated_scale_degrees[seed[counter]] + scale_adjust) end 

   local motif_counter = 1 ; local lilypond_counter = 1
   for counter = 1,#note do
      if note[counter] == seed[motif_counter] then
	 velocity[counter] = velocity_dynamic_list.ff
	 motif_counter = motif_counter + 1
	 --[[if counter ~=1 then]] lilypond_spot[lilypond_counter] = counter ; lilypond_counter = lilypond_counter + 1 --end
      end
      if motif_counter == #seed + 1 and repeat_motif == "no" then number_of_notes = counter + math.ceil(counter/motif_counter) ; break end
      if motif_counter == #seed + 1 and repeat_motif == "yes" then motif_counter = 1 end
   end

   if number_of_notes > original_number_of_notes then number_of_notes = original_number_of_notes end
   
   local red_spot = shallowcopy(lilypond_spot)

   if format == "mesostic" then
      for counter = 1,#lilypond_spot do
   	 if counter < #lilypond_spot then counter_plus_one = lilypond_spot[counter + 1]
   	 else counter_plus_one = number_of_notes
   	 end
   	 lilypond_spot[counter] = tonumber(math.floor((lilypond_spot[counter] + counter_plus_one) / 2)) 
      end
   end

   write_log("mesostics.lua","seed",seed)
   write_log("mesostics.lua","note",note)
   write_log("mesostics.lua","lilypond_spot",lilypond_spot)
   write_log("mesostics.lua","red_spot",red_spot)
   return velocity,lilypond_spot,number_of_notes,red_spot
end

lilypond_mesostic_formatting = function(colorize,format,instrument,lily_note_table_1,lily_note_table_3,red_spot,lilypond_spot)
   if colorize == "yes" then	 
      local lilypond_table_1 = lily_note_table_1:split(" ")
      for counter = 1,#red_spot do
	 if format == "acrostic" then

	    lilypond_table_1[red_spot[counter]] = " \\red_note \\bar\"\"\\break " .. lilypond_table_1[red_spot[counter]] 
	 elseif format == "mesostic" then
	    lilypond_table_1[red_spot[counter]] = " \\red_note " .. lilypond_table_1[red_spot[counter]]
	    lilypond_table_1[lilypond_spot[counter]] = lilypond_table_1[lilypond_spot[counter]] .. " \\bar\"\"\\break "
	 else
	    lilypond_table_1[red_spot[counter]] = " \\red_note " .. lilypond_table_1[red_spot[counter]]
	 end
      end
      
      lily_note_table_1 = table.concat(lilypond_table_1," ")

      if midi_instrument_list[instrument].second_clef ~= "0" then
	 local lilypond_table_3 = lily_note_table_3:split(" ")
	 for counter = 1,#red_spot do
	    if format == "acrostic" then
	       lilypond_table_3[red_spot[counter]] = " \\red_note " .. lilypond_table_3[red_spot[counter]] 
	    elseif format == "mesostic" then
	       lilypond_table_3[red_spot[counter]] = " \\red_note " .. lilypond_table_3[red_spot[counter]]
	       lilypond_table_3[lilypond_spot[counter]] = lilypond_table_3[lilypond_spot[counter]]
	    else
	       lilypond_table_3[red_spot[counter]] = " \\red_note " .. lilypond_table_3[red_spot[counter]]
	    end	     
	 end
	 lily_note_table_3 = table.concat(lilypond_table_3," ")
      end
   end    
   return lily_note_table_1,lily_note_table_3
end
