    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Twelve tone method plus serialize style algorithms
-- *twelve_tone* takes your pitch_table and serializes it using the pattern specified
-- in "command", returning notes, velocities, and durations. Commands available are:
-- P = prime, R = retrograde, I = inversion, RI = retrograde-inversion. That followed by a
-- scale degree (P1, m7, etc) or a number indicates the transposition in degrees or half-steps
-- (plus or minus), eg: P:5 means the original tone row transposed up 5 half-steps while RI:-P5
-- transposes the retrograde-inversion down a perfect fifth number of half-steps.
--
-- *serialize* only functions on the passed parameter (velocity or duration)
-- manipulating as per the "command" and returns that one parameter. Serialize does not
-- allow for transpositions in the "command" as I don't think this can be interpreted
-- in such a way that makes objective sense to everyone.

twelve_tone = function(key,notes,velocity,duration,calculated_scale_degrees,tempo,number_of_notes,command,allow_repeat)
   local low = header.lowrange
   local high = header.highrange
   local tuning_octave = header.tuning_octave

   local twelve_tone_notes = {} ; local check_string = {} ; local tmp_notes = {} ; local old_note = tuning_octave + 1 -- 13
   local twelve_tone_durations = {} ; local twelve_tone_velocities = {} ; local twelve_tone_bend = {}
   local prime_notes = {} ; local prime_durations = {} ; local prime_velocities = {} ; local prime_bend = {}
   local retro_notes = {} ; local retro_durations = {} ; local retro_velocities = {} ; local retro_bend = {}
   local inverted_notes = {} ; local inverted_durations = {} ; local inverted_velocities = {} ; local inverted_bend = {}
   local retroinverted_notes = {} ; local retroinverted_durations = {} ; local retroinverted_velocities = {} ; local retroinverted_bend = {}
   local twelve_tone_score_notes = {} ; local twelve_tone_score_durations = {} ; local twelve_tone_score_velocities = {} ; local twelve_tone_score_bend = {}
   
   local negative_transposition = 0
   if tuning_octave < 2 then
      tuning_octave = 2
   else
      tuning_octave = tuning_octave + 1
   end
   local average = math.ceil((low + high) /2)
   local restore = average - (average % tuning_octave--[[12]])

   for counter = 1,tuning_octave  --[[11]] do
      check_string[counter] = counter
   end
   for counter = 1,number_of_notes do -- the following modulo hack (again) works for 0 and 1 edo but not 0 < edo < .2
      while notes[counter] >= tuning_octave do -- it's more complicated than that, >= works for 0 and 1 but not anything else
      	 notes[counter] = notes[counter] - tuning_octave
      end
      tmp_notes[counter] = notes[counter]
--      tmp_notes[counter] = notes[counter] % tuning_octave + 1 -- THIS is the key for 0 & 1 edo it should be 0 
   end 
   metacounter = 1 
   for counter = 1,number_of_notes do 
      for subcounter = 0,tuning_octave -1 --[[11]] do 
	 if tmp_notes[counter] == check_string[subcounter] then
	    old_note = tmp_notes[counter] 
	    table.remove(check_string,subcounter) 
	    twelve_tone_notes[metacounter] = tmp_notes[counter]
	    twelve_tone_durations[metacounter] = duration[counter]
	    twelve_tone_velocities[metacounter] = velocity[counter] 
	    metacounter = metacounter + 1
	    break 
	 end

	 -- Check for repeats!
	 if allow_repeat == "yes" and tmp_notes[counter] == old_note then
	    table.insert(check_string,math.ceil(old_note))
	    table.sort(check_string)
	 else 	  
	 end
	 old_note = -1 
      end 
      twelve_tone_number_of_notes = #twelve_tone_notes
   end 

   write_log("twelve_tone.lua","tone row",twelve_tone_notes)

   local response = ""
   if #check_string == 1 then response = "Full 12 Tone Row!" else response = "Not a full row" end
   write_log("twelve_tone.lua","check for size of row",response)

-- Here we create the Prime, Retrograde, Inverted, and Retrograde-Inverted versions of the tone row
   -- Prime
   prime_notes = shallowcopy(twelve_tone_notes)
   prime_durations = shallowcopy(duration) ; prime_velocities = shallowcopy(velocity) ; prime_bend = shallowcopy(bend)
   -- Retrograde
   metacounter = 1
   for counter = twelve_tone_number_of_notes,1,-1 do
      retro_notes[metacounter] = twelve_tone_notes[counter]
      retro_durations[metacounter] = twelve_tone_durations[counter]
      retro_velocities[metacounter] = twelve_tone_velocities[counter]
      metacounter = metacounter + 1
   end
   write_log("twelve_tone.lua","retrograde notes",retro_notes)
   
   -- Inversion
   for counter = 1,twelve_tone_number_of_notes do
      inverted_notes[counter] = (tuning_octave --[[-1]]) - twelve_tone_notes[counter] -- This -1 seemed to work before but also without it? 10/4/16
      inverted_durations[counter] = twelve_tone_durations[counter]
      inverted_velocities[counter] = twelve_tone_velocities[counter]
   end
   write_log("twelve_tone.lua","inverted notes",inverted_notes)
   
   -- Retrograde-inversion
   for counter = 1,twelve_tone_number_of_notes do
      retroinverted_notes[counter] = (tuning_octave ) - retro_notes[counter]
      retroinverted_durations[counter] = retro_durations[counter]
      retroinverted_velocities[counter] = retro_velocities[counter]
      metacounter = metacounter + 1
   end
   write_log("twelve_tone.lua","retrograde-inversion notes",retroinverted_notes)
   
   -- Now apply the manipulated rows as per the commands passed.
   local twelve_tone_commands = command:split(",")
   for counter = 0,#twelve_tone_commands-1 do
      for subcounter = 1,twelve_tone_number_of_notes do
	 local position = (counter * twelve_tone_number_of_notes) + subcounter 
         local actual_command,transposition = table.unpack(twelve_tone_commands[counter+1]:split(":")) 

	 if string.find(transposition,"-") then 
	    negative_transposition = "yes"
	    transposition = string.gsub(transposition,"-","")
	 else
	    negative_transposition = "no"
	 end
	 
	 if tonumber(transposition) then
	    transposition = tonumber(transposition) 
	 else
	    transposition = calculated_scale_degrees[transposition]
	 end

	 if negative_transposition == "yes" then
	    transposition = -1 * transposition
	 end
	          
	 if actual_command == "P" then
	    twelve_tone_score_notes[position] = tonumber(prime_notes[subcounter])+transposition + restore
	    twelve_tone_score_durations[position] = tonumber(prime_durations[subcounter])
	    twelve_tone_score_velocities[position] = tonumber(prime_velocities[subcounter])
	 end
	 if actual_command == "R" then
	    twelve_tone_score_notes[position] = retro_notes[subcounter]+transposition + restore
	    twelve_tone_score_durations[position] = tonumber(retro_durations[subcounter])
	    twelve_tone_score_velocities[position] = tonumber(retro_velocities[subcounter])
	 end
	 if actual_command == "I" then
	    twelve_tone_score_notes[position] = inverted_notes[subcounter]+transposition + restore
	    twelve_tone_score_durations[position] = tonumber(inverted_durations[subcounter])
	    twelve_tone_score_velocities[position] = tonumber(inverted_velocities[subcounter])
	 end
	 if actual_command == "RI" then
	    twelve_tone_score_notes[position] = retroinverted_notes[subcounter]+transposition + restore
	    twelve_tone_score_durations[position] = tonumber(retroinverted_durations[subcounter])
	    twelve_tone_score_velocities[position] = tonumber(retroinverted_velocities[subcounter])       
	 end
      end
   end
   twelve_tone_number_of_notes = twelve_tone_number_of_notes * #twelve_tone_commands
   return twelve_tone_score_notes,twelve_tone_score_durations,twelve_tone_score_velocities,twelve_tone_number_of_notes
end -- function twelve-tone       
    
serialize = function(serialize_table,serial_number_of_notes,family,serialize_command)
   local result = {} ; local prime_result = {} ; local retro_result = {}
   local inverted_result = {} ; local retroinverted_result = {} ; local serial_results = {}
   local sorted_prime = {} ; local check_string = {}

   -- tmp_string = tostring(serialize_list[family]) 
   -- check_string = tmp_string:split(",") 
 
   if family == "velocity" then
      velocity_list = get_keys_from_table(velocity_dynamic_list) 
      for counter = 1,#velocity_list do
	 check_string[counter] = velocity_dynamic_list[velocity_list[counter]] 
      end
   end
   if family == "duration" then
      duration_list = get_keys_from_table(lilypond_duration_list)
      for counter = 1,#duration_list do check_string[counter] = duration_list[counter] 
      end
   end      

local metacounter = 1
   for counter = 1,serial_number_of_notes do
      for subcounter = 1,#check_string do 
	 if serialize_table[counter] == check_string[subcounter] then 
	 table.remove(check_string,subcounter)
	 result[metacounter] = serialize_table[counter] 
	 metacounter = metacounter + 1
	 end
      end

   end

   actual_command = serialize_command:split(",") 
   prime_result = result 

-- Here we create the Prime, Retorgrade, Inverted, and Retrograde-Inverted versions of the row   
   -- Prime
   for counter = 1,#prime_result do sorted_prime[counter] = prime_result[counter] end 
   table.sort(sorted_prime)
   local write_family = family .. " prime"
   write_log("twelve_tone.lua",write_family,prime_result)
   
   -- Retrograde
   metacounter = 1
   for counter = #prime_result,1,-1 do
      retro_result[metacounter] = prime_result[counter] 
      metacounter = metacounter + 1
   end
   write_log("twelve_tone.lua",family .." retrograde",retro_result)
   
   -- Inversion
   for counter = 1,#result do
      position = (#result + 1) - counter
      for subcounter = 1,#result do
	 if prime_result[counter] == sorted_prime[subcounter] then
	    inverted_flag = tonumber(subcounter)
	 end
      end
      inverted_position = (#result + 1) - inverted_flag
      inverted_result[counter] = sorted_prime[inverted_position] ; 
   end
   write_log("twelve_tone.lua",family .." inverted",inverted_result)
   
   -- Retrograde-inversion
   metacounter = 1
   for counter = #result,1,-1 do
      retroinverted_result[metacounter] = inverted_result[counter]
      metacounter = metacounter + 1
   end
   write_log("twelve_tone.lua",family .." retrograde_inversion",retroinverted_result)
   
   metacounter = 1
   
   twelve_tone_counter = math.ceil(twelve_tone_number_of_notes/#result)
   for counter = 0,(twelve_tone_counter) - 1 do
      for subcounter = 1,#result do
	 position = (counter * #result) + subcounter 
                  
	 if actual_command[metacounter] == "P" then serial_results[position] = prime_result[subcounter] 
	 end
	 if actual_command[metacounter] == "R" then serial_results[position] = retro_result[subcounter]
	 end
         if actual_command[metacounter] == "I" then serial_results[position] = inverted_result[subcounter]
	 end
         if actual_command[metacounter] == "RI" then serial_results[position] = retroinverted_result[subcounter]
	 end
      end
      metacounter = metacounter + 1
      if metacounter > #actual_command then metacounter = 1 end
   end
  
   return serial_results
end -- function serialze

