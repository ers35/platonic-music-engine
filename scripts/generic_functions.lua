    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Various non-music related functions   

-- Should we create a log?
function create_log(yes_or_no)
   if yes_or_no == "yes" then
      algorithm = string.gsub(header.algorithm," ","_")
      log_file = io.open(algorithm .. ".log","w")
      header.write_log = "yes"
   else
      header.write_log = "no"
   end
   return
end

-- Write to log
function write_log(section,variable_name,variable)
   local file_name = string.gsub(header.algorithm," ","_") .. ".log"
   local skip = 1
   local increment = 1
   if header.write_log == "yes" then
      local log_file = io.open(file_name, "a")
      log_file:write(section .. "\n")
      log_file:write("Variable name = " .. variable_name .. "\n")
      
      if type(variable) == "table" then
	 if #variable > 500 then skip = 10 ^ (math.floor(math.log(#variable,10))-1 ) end
	 for i,v in pairs(variable) do
	    if increment%skip == 0 then
	       log_file:write(i .. ": " .. v .. "\t")
	    end
	    increment = increment + 1
	 end
      else
	 log_file:write(variable)
      end
      log_file:write("\n\n")
      log_file:close()
   end
end

-- String split 
function string:split( inSplitPattern, outResults )
   if not outResults then
      outResults = { } -- the default split delimiter is set here, it's a space. Should I change it to a comma?
   end
   local theStart = 1
   local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   while theSplitStart do
      table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
      theStart = theSplitEnd + 1
      theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
   end
   table.insert( outResults, string.sub( self, theStart ) )
   return outResults
end

-- String split plus unpack
function bifurcate(string_to_split,delimiter)
   local resulta,resultb = table.unpack(string_to_split:split(delimiter))
   return resulta,resultb
end

-- Round
function round(num, idp) 
   local mult = 10^(idp or 0)
   return math.floor(num * mult + 0.5) / mult
end

-- Copy table
function shallowcopy(orig)
   local orig_type = type(orig)
   local copy
   if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in pairs(orig) do
	 copy[orig_key] = orig_value
      end
   else -- number, string, boolean, etc
      copy = orig
   end
   return copy
end

-- Hash function (djb2)
function hashstream(word) 
   local result = 5381
   for i=1, #word do
      -- result = bit32.lshift(result,5) + result + word:byte(i) -- Lua 5.2
      result = (result << 5) + result + word:byte(i) -- Lua 5.3 
      -- result = bit.lshift(result,5) + result + word:byte(i) -- LuaJit
   end 

   return result
end

-- Hash function (StringHash) http://wowwiki.wikia.com/wiki/USERAPI_StringHash
function hash(word)
   local counter = 1
   for i = 1,#word,3 do
      counter = math.fmod(counter*8161,4294967279) + -- 2^32 - 17: Prime!
	 (string.byte(word,i)*16776193) + 
	 ((string.byte(word,i+1) or (#word-i+256))*8372226) +
	 ((string.byte(word,i+2) or (#word-i+256))*3932164)
   end
   result = math.fmod(counter, 4294967291) -- 2^32 - 5: Prime (and different from the prime in the loop) 
   return result
end
    

-- Count the number of characters in a string (http://amix.dk/blog/post/19462)
function character_count(string, character) 
   if not string then
      return 0
   end

   local count = 0 
   local byte_character = string.byte(character)
   for count = 1, #string do
      if string.byte(string, i) == byte_character then
	 result = result + 1 
      end 
   end 
   return result
end

-- Get keys from table
function get_keys_from_table(source)
   local result = {}
   local counter = 0

   for key,_ in pairs(source) do
      counter = counter +1
      result[counter] = key
   end
   table.sort(result)
   return result
end

-- Binary search

function binary_search(match_value,low,high,tmp_table)
   search_table = shallowcopy(tmp_table) 
   local result
   local range = high 
   -- local low = 1 ; local high = range+1 ; local mid = 0
   high = high + 1 ; local mid = 0
   mid = math.floor((low + high)/2) 

   local num_searches = 1
   while high >= low do 
      num_searches = num_searches + 1
      
      if search_table[mid] == match_value then break end
      if search_table[mid] < match_value then low = mid + 1 end
      if search_table[mid] > match_value then high = mid -1 end
      mid = math.floor((low + high)/2)
      if mid > range then -- Do we even need this section? Apparently yes. But why?
      	 mid = mid -1 ; high = high - 1 ; low = low - 1
      	 break
      end
   end 

   -- if high < low then high = low end -- THIS IS EVIL!!! On 9 Nov 2016 I determined we need it again. Nope it breaks the scales. Fuck
   if not search_table[high] then search_table[high] = 1 end -- This might be the replacement we need?
   
   if search_table[mid] == match_value then rotate = mid
   elseif math.abs(match_value - search_table[low]) < math.abs(search_table[high] - match_value) then -- only HIGH makes *any* sense no RANGE!!!
      rotate = low
   else
      rotate = high
   end
   header.percent = search_table[rotate]
   header.low = search_table[low]
   header.mid = mid
   
   return rotate
end


      
