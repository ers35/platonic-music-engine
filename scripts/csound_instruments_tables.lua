    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- MIDI and Audio tables
-- MIDI instrument list
-- values = instrument #, low range, high range, primary clef, second clef if needed
midi_instrument_list={}
midi_instrument_list={
   ["Grand Piano"]={midi_instrument = 0, low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava_treble="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short) (opening-clef . treble)",ottava_bass="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short) (opening-clef . bass))"},
   ["Piano"]= {midi_instrument =0,low_range=27.500,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava_treble="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short) (opening-clef . treble))",ottava_bass="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short) (opening-clef . bass))"},
   ["Bright Piano"]= {midi_instrument =1,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Electric Piano"]= {midi_instrument =2,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Honky-Tonk Piano"]= {midi_instrument =3,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Electric Piano 1"]= {midi_instrument =4,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Electric Piano 2"]= {midi_instrument =5,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Harpsichord"]= {midi_instrument =6,low_range=43654,high_range=1396900,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Clavinet"]={midi_instrument =7,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Celesta"]={midi_instrument =8,low_range=27.5,high_range=4186.009,lilypond_clef="treble",second_clef="bass",ottava="\\ottavate #'(3 . 7) #' (-3 . -6) #'((name-style . short))"},
   ["Glockenspiel"]={midi_instrument =9,low_range=783.000,high_range=4186.009,lilypond_clef="treble_15",second_clef="0"},
   ["Music Box"]={midi_instrument =10,low_range=830.6100,high_range=2637.000,lilypond_clef="treble",second_clef="0"},
   ["Vibraphone"]={midi_instrument =11,low_range=174000,high_range=1400000,lilypond_clef="treble",second_clef="0"},
   ["Marimba"] ={midi_instrument =12,low_range=174.000,high_range=1400.000,lilypond_clef="treble",second_clef="0"}, 
   ["Xylophone"] ={midi_instrument =13,low_range=261.000,high_range=420.0000,lilypond_clef="treble",second_clef="0"},
   ["Tubular Bells"]={midi_instrument =14,low_range=261630,high_range=698460,lilypond_clef="treble",second_clef="0"},
   ["Dulcimer"] ={midi_instrument =15,low_range=63,high_range=100,lilypond_clef="treble",second_clef="0"},
   ["Drawbar Organ"]={midi_instrument =16,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Percussive Organ"]={midi_instrument =17,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Rock Organ"]= {midi_instrument =18,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Church Organ"]={midi_instrument =19,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Organ"]={midi_instrument =19,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Reed Organ"]={midi_instrument =20,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Accordian"]={midi_instrument =21,low_range=29,high_range=89,lilypond_clef="treble",second_clef="bass"},
   ["Harmonica"]={midi_instrument =22,low_range=61,high_range=71,lilypond_clef="treble",second_clef="0"},
   ["Tango Accordian"]={midi_instrument =23,low_range=61,high_range=71,lilypond_clef="treble",second_clef="0"},
   ["Classical Guitar"]={midi_instrument =24,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Steel String Guitar"]={midi_instrument =25,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Jazz Guitar"]={midi_instrument =26,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Electric Guitar"]={midi_instrument =27,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Electric Guitar Muted"]={midi_instrument =28,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Overdriven Guitar"]={midi_instrument =29,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Distortion Guitar"]={midi_instrument =30,low_range=82,high_range=988,lilypond_clef="treble_8",second_clef="0"},
   ["Guitar Harmonics"]={midi_instrument =31,low_range=164,high_range=1975,lilypond_clef="treble_8",second_clef="0"},
   ["Acoustic Bass"]={midi_instrument =32,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Electric Bass Finger"]={midi_instrument =33,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Electric Bass Pick"]={midi_instrument =34,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Stand Up Bass"]={midi_instrument =35,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Slap Bass 1"]={midi_instrument =36,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Slap Bass 2"]={midi_instrument =37,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Synth Bass 1"]={midi_instrument =38,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Synth Bass 2"]={midi_instrument =39,low_range=28,high_range=73,lilypond_clef="base_8",second_clef="0"},
   ["Violin"]={midi_instrument =40,low_range=196,high_range=1977,lilypond_clef="treble",second_clef="0",ottava_treble="\\ottavate #'(4 . 7) #' (-3 . -6) #'((name-style . short) (opening-clef . treble))"},
   ["Viola"]={midi_instrument =41,low_range=48,high_range=91,lilypond_clef="alto",second_clef="0"},
   ["Cello"]={midi_instrument =42,low_range=65,high_range=880,lilypond_clef="bass",second_clef="0",ottava_treble="\\ottavate #'(4 . 7) #' (-4 . -7) #'((opening-clef . bass) (name-style . short))"},
   ["Double Bass"]={midi_instrument =43,low_range=28,high_range=67,lilypond_clef="bass_8",second_clef="0"},
   ["Tremolo String"]={midi_instrument =44,low_range=28,high_range=103,lilypond_clef="treble",second_clef="0"},
   ["Pizzicato Strings"]={midi_instrument =45,low_range=28,high_range=103,lilypond_clef="treble",second_clef="0"},
   ["Orchestral Harp"]={midi_instrument =46,low_range=24,high_range=103,lilypond_clef="treble",second_clef="bass"},
   ["Timpani"]={midi_instrument =47,low_range=40,high_range=55,lilypond_clef="treble",second_clef="0"},
   ["Strings 1"]={midi_instrument =48,low_range=28,high_range=103,lilypond_clef="treble",second_clef="bass"},
   ["Strings 2"]={midi_instrument =49,low_range=28,high_range=103,lilypond_clef="treble",second_clef="bass"},
   ["Synth Strings 1"]={midi_instrument =50,low_range=28,high_range=103,lilypond_clef="treble",second_clef="bass"},
   ["Synth Strings 2"]={midi_instrument =51,low_range=28,high_range=103,lilypond_clef="treble",second_clef="bass"},
   ["Choir Aahs"]={midi_instrument =52,low_range=38,high_range=100,lilypond_clef="treble",second_clef="bass"},
   ["Voice Oohs"]={midi_instrument =53,low_range=38,high_range=100,lilypond_clef="treble",second_clef="bass"},
   ["Synth Voice"]={midi_instrument =54,low_range=38,high_range=100,lilypond_clef="treble",second_clef="bass"},
   ["Orchestra Hit"]={midi_instrument =55,low_range=30,high_range=100,lilypond_clef="treble",second_clef="bass"},
   ["Trumpet"]={midi_instrument =56,low_range=54,high_range=84,lilypond_clef="treble",second_clef="0"},
   ["Trombone"]={midi_instrument =57,low_range=40,high_range=72},
   ["Tuba"]={midi_instrument =58,low_range=28,high_range=58},
   ["Muted Trumpet"]={midi_instrument =59,low_range=55,high_range=82},
   ["French Horn"]={midi_instrument =60,low_range=34,high_range=77},
   ["Brass Section"]={midi_instrument =61,low_range=28,high_range=82},
   ["Synth Brass 1"]={midi_instrument =62,low_range=28,high_range=82},
   ["Synth Brass 2"]={midi_instrument =63,low_range=28,high_range=82},
   ["Soprano Sax"]={midi_instrument =64,low_range=60,high_range=80},
   ["Sax"]={midi_instrument =64,low_range=60,high_range=80},
   ["Alto Sax"]={midi_instrument =65,low_range=55,high_range=75},
   ["Tenor Sax"]={midi_instrument =66,low_range=50,high_range=70},
   ["Baritone Sax"]={midi_instrument =67,low_range=45,high_range=65},
   ["Oboe"]={midi_instrument =68,low_range=58,high_range=91,lilypond_clef="treble",second_clef="0"},
   ["English Horn"]={midi_instrument =69,low_range=52,high_range=81,lilypond_clef="treble_5",second_clef="0"},
   ["Bassoon"]={midi_instrument =70,low_range=34,high_range=75,lilypond_clef="treble",second_clef="bass"},
   ["Clarinet"]={midi_instrument =71,low_range=50,high_range=94,lilypond_clef="treble",second_clef="0"},
   ["Piccolo"]={midi_instrument =72,low_range=74,high_range=102,lilypond_clef="treble^8",second_clef="0"},
   ["Flute"]={midi_instrument =73,low_range=261.625,high_range=2093.004,lilypond_clef="treble",second_clef="0",ottava=""},
   ["Recorder"]={midi_instrument =74,low_range=74,high_range=102,lilypond_clef="treble",second_clef="0"},
   ["Pan Flute"]={midi_instrument =75,low_range=74,high_range=102,lilypond_clef="treble",second_clef="0"},
   ["Blown Bottle"]={midi_instrument =76,low_range=50,high_range=70,lilypond_clef="treble",second_clef="0"},
   ["Shakuhachi"]={midi_instrument =77,low_range=50,high_range=70,lilypond_clef="treble",second_clef="0"},
   ["Whistle"]={midi_instrument =78,low_range=70,high_range=90,lilypond_clef="treble",second_clef="0"},
   ["Ocarina"]={midi_instrument =79,low_range=70,high_range=90,lilypond_clef="treble",second_clef="0"},
   ["Square"]={midi_instrument =80,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Sawtooth"]={midi_instrument =81,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Calliope"]={midi_instrument =82,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Chiff"]={midi_instrument =83,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Charang"]={midi_instrument =84,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Voice"]={midi_instrument =85,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Fifths"]={midi_instrument =86,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Bass + lead"]={midi_instrument =87,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["New Age"]={midi_instrument =88,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Warm"]={midi_instrument =89,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Polysynth"]={midi_instrument =90,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Choir"]={midi_instrument =91,low_range=0,high_range=127,lilypond_clef="treble",second_clef="0"},
   ["Soprano Voice"] = {midi_instrument = 52,low_range = 60,high_range = 84,lilypond_clef="treble",second_clef = "0"},
   ["Mezzo Soprano Voice"] = {midi_instrument = 52,low_range = 57,high_range = 81,lilypond_clef="treble",second_clef = "0"},
   ["Alto Voice"] = {midi_instrument = 52,low_range = 53,high_range = 77,lilypond_clef = "treble",second_clef = "0"},
   ["Tenor Voice"] = {midi_instrument = 52,low_range = 48,high_range = 69,lilypond_clef = "treble_8",second_clef = "0"},
   ["Baritone Voice"] = {midi_instrument = 52,low_range = 41,high_range = 65,lilypond_clef="bass",second_clef = "0"},
   ["Bass Voice"] = {midi_instrument = 52,low_range = 40,high_range = 64,lilypond_clef = "bass",second_clef = "0"},
   ["Bowed"]={midi_instrument =92,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Metallic"]={midi_instrument =93,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Halo"]={midi_instrument =94,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Sweep"]={midi_instrument =95,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Rain"]={midi_instrument =96,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Soundtrack"]={midi_instrument =97,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Crystal"]={midi_instrument =98,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Atmosphere"]={midi_instrument =99,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Brightness"]={midi_instrument =100,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Goblins"]={midi_instrument =101,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Echoes"]={midi_instrument =102,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Sci-Fi"]={midi_instrument =103,low_range=0,high_range=127,lilypond_clef="treble",second_clef="bass"},
   ["Sitar"]={midi_instrument =104,low_range=48,high_range=96,lilypond_clef="treble",second_clef="0"},
   ["Banjo"]={midi_instrument =105,low_range=48,high_range=91,lilypond_clef="treble_8",second_clef="0"},
   ["Shamisen"]={midi_instrument =106,low_range=63,high_range=100},
   ["Koto"]={midi_instrument =107,low_range=63,high_range=100},
   ["Kalimba"]={midi_instrument =108,low_range=63,high_range=100},
   ["Bag Pipe"]={midi_instrument =109,low_range=63,high_range=100},
   ["Fiddle"]={midi_instrument =110,low_range=55,high_range=103},
   ["Shanai"]={midi_instrument =111,low_range=63,high_range=100},
   ["Tinkle Bell"]={midi_instrument =112,low_range=90,high_range=110},
   ["Agogo"]={midi_instrument =113,low_range=80,high_range=100},
   ["Steel Drums"]={midi_instrument =114,low_range=40,high_range=60},
   ["Woodblock"]={midi_instrument =115,low_range=60,high_range=61},
   ["Taiko"]={midi_instrument =116,low_range=60,high_range=72},
   ["Melodic Tom"]={midi_instrument =117,low_range=60,high_range=71},
   ["Synth Drum"]={midi_instrument =118,low_range=0,high_range=127},
   ["Reverse Cymbal"]={midi_instrument =119,low_range=80,high_range=81},
   ["Guitar Fret"]={midi_instrument =120,low_range=63,high_range=100},
   ["Breath"]={midi_instrument =121,low_range=70,high_range=71},
   ["Seashore"]={midi_instrument =122,low_range=0,high_range=127},
   ["Bird"]={midi_instrument =123,low_range=70,high_range=127},
   ["Telephone"]={midi_instrument =124,low_range=60,high_range=71},
   ["Helicopter"]={midi_instrument =125,low_range=60,high_range=71},
   ["Applause"]={midi_instrument =126,low_range=60,high_range=71},
   ["Gunshot"]={midi_instrument =127,low_range=71,high_range=71},
}

csound_instrument_table = {}
csound_instrument_table = {
   ["Timer"] = {csound_orchestra1 = ";toscil3 \tamp, \tfreq, \nasig \toscil3 \tp4, \tp5 \naL, aR \tpan2 \tasig, \tp6",
		csound_orchestra2 = "",
		f = "",
		outs1 = "aL",outs2 = "aR"},
   ["SineWave"] = {csound_orchestra1 = ";\toscil3 \tamp, \tfreq \nasig \toscil3 \tp4, \tp5",
		   csound_orchestra2 = "\naL, aR \tpan2 \tasig, \tp6",
		   f = "",
		   outs1 = "aL",outs2 = "aR"},
   ["Sawtooth"] = {csound_orchestra1 = ";\tvco \tamp, \tfreq, \tiwave, \tpwidth, function #  \nasig \tvco \tp4, \tp5, \t1, \t.5, \t2",
		   csound_orchestra2 = "\naL, aR \tpan2 \tasig, \tp6",
		   f = "0 65536 10 1",
		   outs1 ="aL",outs2 = "aR"},
   ["Square"] = {csound_orchestra1 = ";\tvco \tamp, \tfreq, \tiwave, \tpwidth, function # \nasig \tvco \tp4, \tp5, \t2, \t.5, \t3",
		 csound_orchestra2 = "\naL, aR \tpan2 \tasig, \tp6",
		 f = "0 65536 10 1",
		 outs1 = "aL",outs2 = "aR"},
   ["Triangle"] = {csound_orchestra1 = ";\tvco \tamp, \tfreq, \tiwave, \tpwidth, function # \nasig \tvco \tp4, \tp5, \t3, \t.5, \t4",
		   csound_orchestra2 = "\naL, aR \tpan2 \tasig, \tp6",
		   f = "0 65536 10 1",
		   outs1 = "aL",outs2 = "aR"},
   ["MIDI"] = {csound_orchestra1 = "inum = 69+12*log2(p5/440) \nivel = p4/127 \nkamp linsegr 1, 1, 1, .1, 0 \nkamp = kamp/15000 \n\na1,a2 sfplay3 ivel, inum, kamp*ivel, p5, p7, 1",
		csound_orchestra2 = "",
		f = "",
		outs1 = "a1",outs2 = "a2"},		
}


