    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

one_note_om = function(pitch,duration,velocity,instrument,number_of_notes,tuning_name)
   local avg_pitch = 0
   local avg_duration = 0
   local avg_velocity = 0

   for counter = 1,number_of_notes do
      avg_pitch = round(((avg_pitch * (counter -1)) + pitch[counter])/counter)
      avg_duration = ((avg_duration * (counter - 1)) + duration[counter])/counter
      avg_velocity = ((avg_velocity * (counter - 1)) + velocity[counter])/counter
   end

   pitch = {} ; duration = {} ; velocity = {}
   pitch[1] = avg_pitch
   duration[1] = avg_duration
   velocity[1] = avg_velocity

   local dedication = header.dedication ; local title = "Music" ; 
   local composer = "David Bellows" ; local arranger = "Platonic Music Engine"
   local font = "newcent" ; local poet = "" ; local algorithm = "One Note"
   local meter = "" ; local piece = ""
   
   local preamble = latex_preamble(font)
   preamble = preamble .. "\n\\usepackage{shapepar}"
   local temperament_name = tuning_name ; local pretty_system = header.reference_pitch_name
   local latex_title = latex_title(dedication,title,instrument,poet,composer,arranger,algorithm,meter,piece,temperament_name,pretty_system)
   
   local score = preamble .. latex_title

   local score = score .. "\n\\begin{vplace}[.7]\\hfill\\circlepar{Given infinite time, infinite iterations, all notes converge on to \\textit{One Note.} This is your note after "..number_of_notes.." iterations.}\\hfill\n\\end{vplace}"

   local footer = latex_footer()
   
   local score = score .. footer .. "\n\\end{document}"
   
   return pitch,duration,velocity,score
end -- function one_note_om
