    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Lilypond Header
lilypond2header = function(instrument,tuning_name,composer,arranger,poet,meter,piece,algorithm,pretty_system)
   io.write("Generating Lilypond score ...\r") ; io.flush()
   local title = "Music" ; local dedication = header.dedication ; local subtitle = algorithm 
   local subsubtitle = "" ; local today = os.date("© %A %d %B %Y %I:%M:%S %p %Z %z")
   
   local tmp_tuning_name,tmp_name = bifurcate(tuning_name,"-")
   tmp_tuning_name = string.gsub(tmp_tuning_name,"s",'" \\sharp "')

   if tmp_tuning_name ~= "f" and tmp_tuning_name ~= 'f" \\sharp "' then
      tmp_tuning_name = string.gsub(tmp_tuning_name,"f",'" \\flat "')
   end

   tuning_name = tmp_tuning_name .. "-" .. tmp_name .. " tuning"
   piece = "\\markup {\\column {\\line {\"".. tuning_name .."\"}\n\t\t\t\t  \\line { using \""..pretty_system.."\"}}}"
   
   local lilypond_header = 
      "\\version \""..
      header.lilypond_version ..
      "\" \n\n\\header { \n \tdedication = \"(For ".. dedication..
      ")\" \n \ttitle = \"".. title..
      "\" \n \tsubtitle = \"".. subtitle..
      "\" \n \tsubsubtitle = \"".. subsubtitle..
      "\" \n \tinstrument = \"".. instrument.. 
      "\" \n \tpoet = \"".. poet.. 
      "\" \n \tcomposer = \"".. composer.. 
      "\" \n \tarranger = \"".. arranger..
      "\" \n \tmeter = \"".. meter .. 
      "\" \n \tpiece = " .. piece ..   
      "\n \ttagline = \"\" \n \tcopyright = \\markup {\\column \\center-align {\\line {"..today.."} \n\t\t\t\t\t\t\\line {\"This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.\"}}}\n}"..
      "\n\n\\include \"scripts/merge-rests.ly\""..
      "\n\\include \"scripts/auto-ottava.ly\""..
      "\n\\language \"english\""..
      "\n\\pointAndClickOff"
   
   if header.note_scheme == "simple" then lilypond_header = lilypond_header.."\n\\include \"scripts/enharmonic.ly\"" end

   return lilypond_header
end

-- Generate Lilypond notes from audio frequencies
generate_lilypond_notes_from_frequencies = function(instrument, number_of_notes, tuning_name, scale, scale_degrees, pitch_table, freq_table, duration_table, velocity_table,rest_or_nil,avoid_collision) 
   local lily_note = {} 
   local lilypond_pitches = {}
   local lilypond_notes = {}
   local lilypond_notes_1 = {}
   local lilypond_notes_3 = {}
   local score_1 = {}
   local score_3 = {}
   local table_to_search = ""
   local lilypond_key = ""
   local lily_freq = 0
   local upper_limit = 0
   local active_vel = "" ; local active_vel_1 = "" ; local active_vel_3 = ""
   local middle_c = 261
   local upper_clef, lower_clef = midi_instrument_list[instrument].lilypond_clef,midi_instrument_list[instrument].second_clef
   local tmp_vel_table = get_keys_from_table(lilypond_velocity_list)
   local tmp_dur_table = get_keys_from_table(lilypond_duration_list)
   local rest_or_space = rest_or_nil
   
   local force_up = "no"
   local force_down = "no"
   local active_chord = "no"
   local result = 0
   local repeat_ratio = header.repeat_ratio
   local base_freq = header.reference_frequency 
   while base_freq > 8.175 do 
      base_freq = base_freq / repeat_ratio
   end 
  
   if header.note_scheme == "simple" then
      lilypond_ratio_table = enharmonic_lilypond_ratio_table
      lilypond_note_table = enharmonic_lilypond_note_table
      lilypond_tuning_table_rotate = enharmonic_lilypond_tuning_table_rotate
      upper_limit = 12
   elseif header.note_scheme == "complex" then
      lilypond_ratio_table = nonenharmonic_lilypond_ratio_table
      lilypond_note_table = nonenharmonic_lilypond_note_table
      lilypond_tuning_table_rotate = nonenharmonic_lilypond_tuning_table_rotate
      upper_limit = 49
   end

   local rotate_key = table.unpack(tuning_name:split("-")) 
   local rotate_amount = lilypond_tuning_table_rotate[rotate_key] 
   for counter = 1,rotate_amount do
      local tmp = table.remove(lilypond_note_table,1)
      table.insert(lilypond_note_table,tmp)
   end  

   -- -- The following rotates the notes based on the scale but counterintuitively is not needed? The preceeding code based on the tuning
   -- -- actually does the trick. I am confused but whatevs. 10/2/16
   -- local rotate_amount = lilypond_note_table_rotate[root]-- - header.tuning_rotate) -- + header.tuning_rotate)%12 print(rotate_amount)
   -- for counter = 1,rotate_amount do
   --    local tmp = table.remove(lilypond_note_table,1)
   --    table.insert(lilypond_note_table,tmp)
   -- end
  
   for counter = 1,number_of_notes do
      result = freq_table[counter] ; octave = 1
      while result > base_freq * repeat_ratio do
   	 result = result / repeat_ratio 
      end
      if result/base_freq >= 2 then result = result/repeat_ratio end
      ratio = tonumber(result/base_freq)

      lily_note = binary_search(ratio,1,upper_limit,lilypond_ratio_table)

      local octave_lookup,_ = math.modf(math.log(freq_table[counter] --[[+base_freq]],2)) -- 11 is a magic number that seems to work with EDOs. Sigh.

      lilypond_pitches[counter] = lilypond_note_table[lily_note] .. lilypond_octave_table[octave_lookup]

      tmp_dur = lilypond_duration_list[duration_table[counter]] 

      -- The following allows for duration values not in the lilypond_duration_list, like fractional stuff
      -- it does this with its own linear brute-force search. Could I reuse the binary search? Anyhoo, notice the difference
      -- between subcounter and counter.
      if not tmp_dur then
	 for subcounter = 1,#tmp_dur_table do
	    if tonumber(duration_table[counter]) == tmp_dur_table[subcounter] then
	       tmp_dur = lilypond_duration_list[tmp_dur_table[subcounter]] 
	    elseif tonumber(duration_table[counter]) > tmp_dur_table[subcounter] and tonumber(duration_table[counter]) < tmp_dur_table[subcounter + 1] then       
	       if math.abs(tonumber(duration_table[counter]) - tmp_dur_table[subcounter]) < math.abs(tonumber(duration_table[counter]) - tmp_dur_table[subcounter+1]) then 
		  tmp_dur = lilypond_duration_list[tmp_dur_table[subcounter]] 
	       else
		  tmp_dur = lilypond_duration_list[tmp_dur_table[subcounter + 1]] 
	       end
	    end
	 end	 	 
      end
      
      local tmp_vel = lilypond_velocity_list[velocity_table[counter]] 

      -- The following allows for velocity values not in the lilypond_velocity_list, like fractional stuff
      -- it does this with its own linear brute-force search. Could I reuse the binary search? Anyhoo, notice the difference
      -- between subcounter and counter.
      if not tmp_vel then
	 for subcounter = 1,#tmp_vel_table do
	    if tonumber(velocity_table[counter]) == tmp_vel_table[subcounter] then
	       tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter]] 
	    elseif tonumber(velocity_table[counter]) > tmp_vel_table[subcounter] and tonumber(velocity_table[counter]) < tmp_vel_table[subcounter + 1] then       
	       if math.abs(tonumber(velocity_table[counter]) - tmp_vel_table[subcounter]) < math.abs(tonumber(velocity_table[counter]) - tmp_vel_table[subcounter+1]) then 
		  tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter]] 
	       else
		  tmp_vel = lilypond_velocity_list[tmp_vel_table[subcounter + 1]] 
	       end
	    end
	 end	 	 
      end 
      if tmp_vel == "r" then
	 if avoid_collision == "yes" then
	    lilypond_notes[counter] = lilypond_pitches[counter] .. tmp_dur .. "\\rest"
	 else
	    lilypond_notes[counter] = "r" .. tmp_dur
	 end
      else
	 lilypond_notes[counter] = lilypond_pitches[counter] .. tmp_dur 
      end
      
      if lower_clef == "0" then
	 if tmp_vel ~= active_vel then
	    if tmp_vel ~= "r" then
	       lilypond_notes_1[counter] = lilypond_notes[counter] .. tmp_vel
	       active_vel = tmp_vel
	    else
	       lilypond_notes_1[counter] = lilypond_notes[counter]
	    end
	 else
	    lilypond_notes_1[counter] = lilypond_notes[counter]
	 end
      else
	 if freq_table[counter] > middle_c or force_up == "yes" then
	    if tmp_vel ~= active_vel_1 then
	       if tmp_vel ~= "r" then
		  lilypond_notes_1[counter] = lilypond_notes[counter] .. tmp_vel
		  active_vel_1 = tmp_vel
	       else
		  lilypond_notes_1[counter] = lilypond_notes[counter]
	       end
	       lilypond_notes_3[counter] = rest_or_space--[["r"]] .. tmp_dur
	    else
	       lilypond_notes_1[counter] = lilypond_notes[counter]	         
	       lilypond_notes_3[counter] = rest_or_space--[["r"]] .. tmp_dur 
	    end
	    
	 else
	    if tmp_vel ~= active_vel_3 then
	       if tmp_vel ~= "r" then
		  lilypond_notes_3[counter] = lilypond_notes[counter] .. tmp_vel
		  active_vel_3 = tmp_vel
	       else
		  lilypond_notes_3[counter] = lilypond_notes[counter]
	       end
	       lilypond_notes_1[counter] = rest_or_space--[["r"]] .. tmp_dur 
	    else
	       lilypond_notes_1[counter] = rest_or_space--[["r"]] .. tmp_dur 
	       lilypond_notes_3[counter] = lilypond_notes[counter] 
	    end
	 end -- upper vs lower clef
      end -- single vs double clefs
   end -- for loop for creating pitches

   local key_lily,mode_lily = table.unpack(scale:split("-"))

   if mode_lily == "major" then
      lilypond_key = key_lily .. " \\" .. "major"
   elseif mode_lily == "minor" or mode_lily == "harmonic minor" then
      lilypond_key = key_lily .. " \\" .. "minor"
   else
      lilypond_key = "c \\major"
   end
   
   write_log("Lilypond creation","lily_note",lilypond_notes)
   write_log("Lilypond creation","lily_note_1",lilypond_notes_1)
   write_log("Lilypond creation","lily_note_3",lilypond_notes_3)

   score_1 = table.concat(lilypond_notes_1," ")
   if down_clef ~= "0" then score_2 = table.concat(lilypond_notes_3," ") end
   return score_1,score_2,lilypond_key

end

    -- Generate Lilypond variables
lilypond2variables = function(instrument,key,time,tempo_word,tempo_number,score_one,score_two,score_three,score_four,score_five,score_six)
   local equal = "="
   local upper_clef = midi_instrument_list[instrument].lilypond_clef
   local down_clef = midi_instrument_list[instrument].second_clef
   
   if string.sub(tempo_word,1,1) == "#" then tempo_word = ""
      local whole,fraction = math.modf(tempo_number)
      if fraction ~= 0 then
	 tempo_number = whole
	 equal = "=" -- Need to make this into approximate sign. Might be complicated.
      end
   end

   local score_info = "\n\nscore_info = {\\time "..time.." \\tempo \""..tempo_word.."\" 4 " ..equal.." "..tempo_number.."}"
   local score = "" ; final_score = ""

   score = score .. score_info
   score = score.."\n\nvoice_one = {\\key "..key.." "..score_one
   if score_three ~= nil then
      score = score .."}\n\nvoice_three = {"..score_three
   end
   if score_five ~= nil then
      score = score .."}\n\nvoice_five = {"..score_five
   end
   if down_clef ~= "0" then 
      score = score.."}\n\nvoice_two = {\\key "..key.." "..score_two
      if score_three ~= nil then
	 score = score .. "}\n\nvoice_four = {"..score_four
      end
      if score_five ~= nil then
	 score = score .. "}\n\nvoice_six = {"..score_six
      end
   end
   
   final_score = score.." \\bar \"|.\"}"
   
   return final_score,score_info
end

-- Generate Lilypond footer
lilypond2footer = function(score_info,instrument,voices,score_lyrics,use_ragged_right,no_indent)
   local lilypond_lyrics = "" ; local lyrics = "" ; local ragged_right = ""
   local temperament_range = header.tuning_octave
   local paper_size = header.paper
   local ottava_treble,ottava_bass = midi_instrument_list[instrument].ottava_treble,midi_instrument_list[instrument].ottava_bass
   if ottava_treble == nil then ottava_treble = "" end
   if ottava_bass == nil then ottava_bass = "" end
   local down_clef = midi_instrument_list[instrument].second_clef 
   local naturalize_music = ""
   if no_indent == "yes" then lily_indent = "score = 0" else lily_indent = "" end

   local upper_clef = midi_instrument_list[instrument].lilypond_clef
   local lower_clef = midi_instrument_list[instrument].second_clef
   
   if use_ragged_right == "yes" then
      ragged_right = "ragged-right = ##t"
   end

   if temperament_range then
      if header.note_scheme == "simple" then naturalize_music = "\n\t\t\t\\naturalizeMusic " end
   end

   layout_addition = "\\context { \\Voice \\remove \"Note_heads_engraver\" \\consists \"Completion_heads_engraver\" \\remove \"Rest_engraver\" \\consists \"Completion_rest_engraver\"}"
   
   if no_indent == "yes" then layout_addition = layout_addition .. "indent = 0" end

   if #score_lyrics ~= 0 then
      for counter = 1,#score_lyrics do
	 lilypond_lyrics = lilypond_lyrics..score_lyrics[counter].." "
      end
   end
   
   if #score_lyrics ~= 0 then
      lyrics = "\\new Lyrics \\lyricsto \"first\" {"..lilypond_lyrics.."}\n"
   end

   lilypond_footer =
      "\n\n\\paper {#(set-paper-size \""..
      paper_size..
      "\") "..lily_indent.."}\n\n\\score { \n \t\\new PianoStaff << \n"..
      "\t\t\\new Staff = \"upper\" << \n\t\t\t\\score_info \\clef "..upper_clef.." "..ottava_treble.." "..naturalize_music.."\\voice_one "
   if voices >= 2 then
      lilypond_footer = lilypond_footer .. "\\\\ "..naturalize_music.."\\voice_three "
   end
   if voices == 3 then
      lilypond_footer = lilypond_footer .. "\\\\ "..naturalize_music.."\\voice_five " 
   end
   lilypond_footer = lilypond_footer .. ">> \n"
   
   if down_clef ~= "0" then
      lilypond_footer = lilypond_footer .. 
	 "\t\t\\new Staff = \"lower\" << \\clef " .. lower_clef .. " " .. ottava_bass .. " " .. naturalize_music .. "\\voice_two "
      if voices >= 2 then
	 lilypond_footer = lilypond_footer .. "\\\\ "..naturalize_music.."\\voice_four "
      end
      if voices == 3 then
	 lilypond_footer = lilypond_footer .. "\\\\ "..naturalize_music.."\\voice_six "
      end
      lilypond_footer = lilypond_footer .. ">> \n"
   end
   lilypond_footer = lilypond_footer .. "\t\t\t>> \n"
   lilypond_footer = lilypond_footer .. "\n\t\\layout{ragged-bottom = ##t " .. ragged_right .. " " .. layout_addition .. "} \n}"

   return lilypond_footer
end -- end Lilypond Score function

-- Generate pdf from Lilypond file
generate_lilypond_pdf = function(Instrument,score,algorithm)
   Instrument = string.gsub(Instrument," ","_")
   algorithm = string.gsub(algorithm," ","_")
   output_lilypond_score = io.open(Instrument.."_"..algorithm..".ly","w")
   output_lilypond_score:write(score)
   output_lilypond_score:close()

   local file_name = (Instrument.."_"..algorithm..".ly")

   os.execute("lilypond "..file_name.." 2> lilypond.txt")
   io.write("Generating Lilypond score ... done.\n")
   return
end -- Generate pdf from Lilypond file
	 
