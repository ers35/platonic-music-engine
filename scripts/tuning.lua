    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

reference_pitch = function(concert_tuning_system)
   local tuned_to = 0 ; local system = ""
   if tonumber(string.sub(concert_tuning_system,1,1)) then 
      tuned_to = tonumber(concert_tuning_system) 
      system = "custom reference pitch (A4 = "..tuned_to..")"         
   else
      tuned_to = concert_pitch_system_list[concert_tuning_system].tuning   
      system = concert_pitch_system_list[concert_tuning_system].name
   end
   write_log("tuning.lua","reference pitch",tuned_to)
   return tuned_to,system
end

get_tuning_range = function(scale)
   local range = 0
   local tmp_table = {} 
   local method
   local interval
   
   local starting_pitch,tmp = bifurcate(scale,"-") 
   local num_div,tmp2 = bifurcate(tmp,"/") 
   if tmp2 then
      method,interval = bifurcate(tmp2,":")
   end
   
   if method == "ED" or method == "!ED" or method == "EDO" then 
      range = num_div
   elseif method =="CET" then range = math.ceil(1200/num_div)
   else  
      local tuning = tuning_ratio_list[num_div].lua 
      tmp_table = tuning:split(",") 
      range = #tmp_table 
   end 
   return range,starting_pitch,num_div,method,interval
end

generate_audio_frequency_table = function(tuning,reference)
   local ratio_octave_table = {}
   local audio_freq = {}

   local rotate_number = 0
   local rotate_degree_value = 0
   local rotate = 0 -- This is the value that we're ultimately looking for ; set to 0 for equal-division tunings
   local tave = 0
   local divisions = 1 -- dummy value so that when the EDO stuff takes over everything else will work. Sigh.
   local degrees = ""
   local not_equal = "no" -- variable so we don't have to check for = and $ after this first time
   local mode
   local reference_number = 69
   local initial_reference_freq = 440
   
   -- 2 September 2016
   -- The following math.ceil bit is necessary for fractional tunings (eg, pi-edo, 1.1) to work.
   -- Otherwise the tuning_range is fractional and it throws off the line:
   -- reference_freq = initial_reference_freq * (1/ratio_octave_table[tuning_range - rotate])
   -- should probably investigage further?

   local full_tuning_range,starting_pitch,num_div_or_scale,method,interval = get_tuning_range(tuning) 
   local tuning_range = math.ceil(full_tuning_range)

   ratio_octave_table[0] = 1 ; ratio_octave_table[1] = 3  -- This is for 0-EDO to work. Sigh.
   if method ~= "ED" and method ~= "!ED" and method ~="CET" and method ~= "EDO" then
      not_equal = "yes" ; mode = num_div_or_scale
      local tmp_table = tuning_ratio_list[mode].lua:split(",")

      for counter = 1,tuning_range do
	 tmp_value = tmp_table[counter]
	 if string.find(tmp_table[counter],"cent") then
	    tmp_value = string.gsub(tmp_value,"cents","") ; tmp_value = string.gsub(tmp_value,"cent","")  
	    tmp_value = load("return " .. tmp_value)()
	    ratio_octave_table[counter] = (2^((tmp_value)/1200))
	 else
	    tmp_value = load("return " .. tmp_value)()
	    ratio_octave_table[counter] = tmp_value 
	 end
      end     
      -- local rotate_degree = distance_of_A_to_key[starting_pitch] 
      -- rotate_degree_value = platonic_degree[rotate_degree] 
      -- reference_freq = initial_reference_freq * (1/ratio_octave_table[tuning_range - rotate]) print(reference_freq)      
   elseif method == "ED" or method == "!ED" or method == "EDO" then 
      divisions = tonumber(num_div_or_scale)
      if divisions > 0 and divisions < .001 then divisions = .001 end
      if not interval then interval = "P8" end
      tave = platonic_degree[interval] 
      local increment = 1

      if tonumber(divisions) ~= 0 then -- This is what makes 0-EDO work. Sigh. I feel like it should be more elegant or something.

	 for counter = 1,tuning_range do 
	    ratio_octave_table[counter] = tonumber(tave)^(counter/tonumber(divisions)) 
	    increment = increment + 1
	 end 
     
	 if method == "!ED" then
	    repeat_ratio = tave
	    ratio_octave_table[#ratio_octave_table] = tave
	 end

	 while ratio_octave_table[increment - 1] < 2 do -- had this at 4 (two octaves) but it screws up using scales, d'oh
	    -- This is complicated but will try. Right now ratio_octave_table just has the ratios based on our EDI. increment 
	    -- holds the next value to be computed to fill up our 2:1 octave. tmp_value is our modified modulo calculation that
	    -- makes sure that we squeeze in our fractional !EDI value. That's the tricky bit.
	    local tmp_value = (increment - tuning_range)  
	    while tmp_value > tuning_range do tmp_value = tmp_value - tuning_range end 
	    ratio_octave_table[increment] = ratio_octave_table[increment - tmp_value] * (ratio_octave_table[tmp_value]) 
	    increment = increment + 1  
	 end 
	 tuning_range = increment -1 
      end
   elseif method == "CET" then
      for counter = 1,tuning_range do
	 mode = num_div_or_scale
	 ratio_octave_table[counter] = 2^((mode*counter)/1200)
      end
      
   end   

   local rotate_degree = distance_of_A_to_key[starting_pitch] 
   rotate_degree_value = platonic_degree[rotate_degree] 
   tuning_range = tonumber(tuning_range) 
   if not_equal == "yes" then
      -- local tmp_ratio_octave_table = {} -- this is for ratios < 1
      -- do -- this is for ratios less than 1
      -- 	 local tmp_tuning_range = tuning_range
      -- 	 local tmp_ratio_octave_table = shallowcopy(ratio_octave_table)
      -- 	 for i,v in pairs(tmp_ratio_octave_table) do
      -- 	    if v < 1 then table.remove(tmp_ratio_octave_table,i)
      -- 	       tmp_tuning_range = tmp_tuning_range -1
      -- 	    end
      -- 	 end
      -- --end
      -- 	 print(rotate_degree_value,tuning_range)
      -- 	 rotate = binary_search(rotate_degree_value,1,tmp_tuning_range,tmp_ratio_octave_table)
      -- 	 reference_freq = initial_reference_freq * (1/tmp_ratio_octave_table[tmp_tuning_range - rotate]) 
      -- end
      -- do -- sort ratio octave table (this is for ratios of less than 1)
      -- 	 local tmp_table = {}generate_lilypond_notes_from_frequencies
      -- 	 for counter = 0,#ratio_octave_table do print(ratio_octave_table[counter])
      -- 	    tmp_table[counter + 1] = ratio_octave_table[counter]
      -- 	 end
      -- 	 table.sort(tmp_table)
      -- 	 for counter = 0,#ratio_octave_table do
      -- 	    ratio_octave_table[counter] = tmp_table[counter + 1] print(ratio_octave_table[counter])
      -- 	 end
      -- end

      rotate = binary_search(rotate_degree_value,1,tuning_range,ratio_octave_table) 
      
      reference_freq = initial_reference_freq * (2/ratio_octave_table[tuning_range - rotate]) 
      
   else  
      rotate = binary_search(rotate_degree_value,0,tuning_range,ratio_octave_table)
      reference_freq = equal_base_frequencies[starting_pitch] 
   end

   header.reference_frequency = reference_freq
   repeat_ratio = load("return "..ratio_octave_table[#ratio_octave_table])() 
   header.repeat_ratio = repeat_ratio
   header.tuning_rotate = rotate 
   reference_number = reference_number + rotate -- - tuning_range
 
   -- do -- Made major changes elsewhere that I think work, needs more testing here 9 Nov 2016
   --    -- Check to see if our base frequency lines up with our reference pitch
   --    -- With certain tunings there might not be the symmetry between the inverted interval.
   --    -- In other words, going from A to C and calling that a minor 3rd might not result
   --    -- in the C to A being a major 6th. This code moves that C until the A is a major sixth
   --    -- in order to preserve our A4=X-Hz tuning. Or something. 
   --    local calibrate = distance_of_key_to_A[starting_pitch] -- print("calibrate = ",calibrate)
   --    local calibrate_value = platonic_degree[calibrate] -- print("calibrate_value = ",calibrate_value)
   --    local calibrate_freq = (reference_freq * calibrate_value)  -- print("cal_freq = ",calibrate_freq)
   --    local calibrate_ratio = (calibrate_freq/(initial_reference_freq*2)) -- print("cal_ratio = ",calibrate_ratio)

   --    if calibrate_ratio > 1.01 or calibrate_ratio < .99 then 
   -- 	 delta_freq = (initial_reference_freq*2)/calibrate_freq -- print("delta=",delta_freq)
   -- 	 reference_freq = reference_freq * delta_freq 
   -- 	 -- rotate = round(rotate * delta_freq)     
   -- 	 -- local reference_number = 69 + rotate - tuning_range  
   -- 	 write_log("Calibrated reference_freq","calibrate_ratio",calibrate_ratio)
   -- 	 header.reference_frequency = header.reference_frequency * 1/calibrate_ratio
   --    end 
   -- end

   do -- Create one octave of the audio frequencies
      local increment = 0 
      for counter = reference_number,reference_number + tuning_range do
	 audio_freq[counter] = reference_freq * ratio_octave_table[increment]
	 increment = increment + 1
      end     
   end

   reference_number = reference_number + tuning_range
   reference_freq = reference_freq * repeat_ratio
   
   if tonumber(divisions) ~= 0 then
      do -- Create all the octaves above the original by multiplying by repeat_ratio
	 while reference_freq <= 22050 do
	    for subcounter = 0,tuning_range do
	       audio_freq[reference_number + subcounter] = audio_freq[reference_number - tuning_range + subcounter] * repeat_ratio 
	    end
	    reference_freq = reference_freq * repeat_ratio
	    reference_number = reference_number + tuning_range
	 end
      end
   end
   local high_audio_freq_num = reference_number 
   reference_number = 69 + rotate - (1 * tuning_range ) 
   reference_freq = initial_reference_freq / repeat_ratio

   if tonumber(divisions) >= .2 then -- weird hack of 0-EDO and other really small EDOs
      do -- Create all the octaves below the original by dividing by repeat_ratio
	 while reference_freq >= 5.000 do -- not sure about this number; could probably do better here
	    for subcounter = 0,tuning_range do 
	       audio_freq[reference_number + subcounter] = audio_freq[reference_number + tuning_range + subcounter] / repeat_ratio
	    end
	    reference_freq = reference_freq / repeat_ratio
	    reference_number = reference_number - tuning_range 
	 end
      end
   elseif tonumber(divisions) == 0 then
      audio_freq[reference_number -1] = 0
   else
      audio_freq[reference_number] = 0
   end

   header.rotate = rotate
   local low_audio_freq_num = reference_number + tuning_range
   
   write_log("tuning.lua:generate_audio_frequency_tables","audio_freq",audio_freq)
   write_log("tuning.lua:generate_audio_frequency_tables","ratio_octave_table",ratio_octave_table)
   return audio_freq,ratio_octave_table,tuning_range,low_audio_freq_num,high_audio_freq_num
   end

generate_relative_scale_degrees = function(ratio_table,scale)

   local scale_degrees = {}
   local platonic_table = get_keys_from_table(platonic_degree)

   local key,mode = bifurcate(scale,"-") 
   
   local increment = 0 
   local tuning_range = #ratio_table
   if tuning_range == 0 then tuning_range = 1 end
   for counter = 1,#platonic_table do 
      local degree = platonic_table[counter] 
      value = platonic_degree[platonic_table[counter]] 
      result = binary_search(value,1,tuning_range,ratio_table) 
      scale_degrees[degree] = result 
      increment = increment + 1
   end

   if mode == "major" then
      for degree,value in pairs(major_scale_degrees) do 
	 scale_degrees[degree] = scale_degrees[value]
      end
   end

   if mode == "minor" or mode == "harmonic minor" then
      for degree,value in pairs(minor_scale_degrees) do 
	 scale_degrees[degree] = scale_degrees[value]
      end
   end
   write_log("tuning.lua","generate_relative_scale_degrees",scale_degrees)
   return scale_degrees
end

