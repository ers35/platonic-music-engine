--[[-----------------------
--
-- PCG random number generator
--
-- Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- For additional information about the PCG random number generation scheme,
-- including its license and other licensing options, visit
--
--    http://www.pcg-random.org
--
--]]-----------------------
-- ported kindly by /u/KarbonKitty 
-- with features added just because she's such a nice person!
-- Thanks!
--

local P = {}

local time = os.time

if _REQUIREDNAME == nil then
    bitmapReader = P
else
    _G[_REQUIREDNAME] = P
end

_ENV = P

local pcg = {}
-- pcg.state = 0 -- Proposed bug fix 10/11/16 by me
-- pcg.inc = 0
local bitMask = 2 ^ 32 - 1

local function rand(  )
    local oldstate = pcg.state
    pcg.state = (oldstate * 6364136223846793005) + (pcg.inc | 1)
    local xorshifted = ( ((oldstate >> 18) ~ oldstate) >> 27 ) & bitMask
    local rot = oldstate >> 59
    return ((xorshifted >> rot) | (xorshifted << ((-rot) & 31))) & bitMask
end

local function init( seed, stream )
   pcg.state = 0 -- Proposed bug fix 10/11/16 by me
   pcg.inc = 0
   local shortBitMask = 2 ^ 16 - 1
   local startState = seed or (time() & shortBitMask)
   local startInc = stream or 1
   pcg.inc = (startInc << 1) | 1
   rand()
   pcg.state = pcg.state + startState
   rand()
end

local function randomNumber( num1, num2 )
    local upper = (num2 or num1) + 1
    local lower = 0
    if num2 then lower = num1 else lower = 1 end
    local bound = upper - lower
    local treshold = (2^32 - bound) % bound
    local bit32
    while true do
        bit32 = rand()
        if bit32 >= treshold then
            return (bit32 % bound) + lower
        end
    end
end

local function countLeadingZeroes( x )
    if x == 0 then
        return 64
    end

    x = x | (x >> 1)
    x = x | (x >> 2)
    x = x | (x >> 4)
    x = x | (x >> 8)
    x = x | (x >> 16)
    x = x | (x >> 32)
    x = x + 1
    i = 0
    while x ~= 0 do
        if (x & 1) == 1 then
            return i
        end
        x = x >> 1
        i = i + 1
    end
    return 0
end

local function chooseNotWeighted( tab )
    return tab[ randomNumber( #tab ) ]
end

local function nextDouble( )
    local exponent = -32
    local significand = rand()

    while ( significand == 0 ) do
        exponent = exponent - 32
        significand = rand()
    end

    local shift = countLeadingZeroes( significand )
    if shift ~= 0 then
        exponent = exponent - shift
        significand = significand << shift
        local abc = rand() >> (32 - shift)
        significand = significand | abc
    end

    significand = significand | 1

    local exp = 2^exponent

    return significand * exp
end

P.randomDouble = nextDouble
P.random32bit = rand
P.random = randomNumber
P.choose = chooseNotWeighted
P.seed = init

--init()

return P
