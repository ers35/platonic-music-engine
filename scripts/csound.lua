    -- Platonic Music Engine -- manipulate music in interesting ways.
    -- Copyright (C) 2015 David Bellows davebellows@gmail.com

    -- This program is free software: you can redistribute it and/or modify
    -- it under the terms of the GNU Affero General Public License as
    -- published by the Free Software Foundation, either version 3 of the
    -- License, or (at your option) any later version.

    -- This program is distributed in the hope that it will be useful,
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    -- GNU Affero General Public License for more details.

    -- You should have received a copy of the GNU Affero General Public License
    -- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This creates a Csound file and audio file from that

generate_start_table = function(duration_table)
   local start_table = {} ; local tmp_dur = 0 ; local active_dur = 0
   start_table[1] = 0
   for counter = 2,#duration_table do
      if duration_table[counter - 1] == "." then
	 tmp_dur = active_dur
	 start_table[counter] = start_table[counter - 1]
      else
	 tmp_dur = duration_table[counter - 1]
	 active_dur = tmp_dur
      end
      
      if duration_table[counter] ~= "." then
	 start_table[counter] = start_table[counter - 1] + tmp_dur
      else
	 start_table[counter] = start_table[counter -1]
      end
   end
   return start_table
end

create_csound_preamble = function(algorithm,style,sa_author,time_of_creation)
   local preamble =      ";; For (".. header.dedication ..")\n"
   preamble = preamble ..";; Csound Music File\n"
   preamble = preamble ..";;\n"
   preamble = preamble ..";; "..algorithm.." "..style.."\n"
   preamble = preamble ..";; (algorithm created by "..sa_author..")\n"
   preamble = preamble ..";; Platonic Music Engine\n"
   preamble = preamble ..";;\n"
   preamble = preamble ..";; created on ".. header.time_of_creation .."\n"
   preamble = preamble ..";; \n\n"
   preamble = preamble .."<CsoundSynthesizer>\n"
   preamble = preamble .."<CsOptions>\n"
   preamble = preamble .."-odac\n"
   preamble = preamble .."</CsOptions>\n"
   preamble = preamble .."<CsInstruments>\n\n"
   preamble = preamble .."sr = 44100\n"
   preamble = preamble .."; ksmps = 441\n"
   preamble = preamble .."nchnls = 2\n"
   preamble = preamble .."0dbfs  = 1\n"

   return preamble
end

create_csound_orchestra = function(instrument,num)
   local orchestra = ""
   orchestra = orchestra .."\ninstr "..instrument.."\n"
   orchestra = orchestra ..csound_instrument_table[instrument].csound_orchestra1..csound_instrument_table[instrument].csound_orchestra2.."\n"  
   orchestra = orchestra .."outs ".. csound_instrument_table[instrument].outs1..","..csound_instrument_table[instrument].outs2.."\n"
   orchestra = orchestra .."endin\n"

   return orchestra
end

create_csound_score_function = function(instrument,instrument_num)
   local score = "" 
   local f_command = csound_instrument_table[instrument].f
   if f_command ~= "" then
      score = "f"..instrument_num.." ".. f_command .. "\n" 
   end 

   return score
end

create_csound_score_ins = function(low,instrument,voices_num,ins_num,ins_name,amplitude,frequency,duration,start,pan,midi_ins)
   local score = ""
   write_log("csound.lua","frequency_table",frequency)
   score = "; p1 p2 p3 p4 p5 p6 p7\n"
   score = score .."; ins# start duration amplitude frequency pan MIDI_instrument\n"
   local tmp_dur = ""
   for counter = low, voices_num do
      local tmp_dur = duration[counter]
      if tmp_dur ~= "." then 
	 tmp_dur = string.format("%.13f",duration[counter]) 
      end

      score = score .. "i \"" .. ins_name[ins_num[counter]] .. "\" " .. string.format("%.23f",start[counter]) .. " " .. tmp_dur .. " " .. amplitude[counter] .. " " .. frequency[counter] .. " " .. pan[counter].. " " .. midi_ins[counter].."\n"
   end

   return score
end

finish_csound_score_ins = function(score)
   score = score .. "\ne \n\n</CsScore>\n</CsoundSynthesizer>\n\n"
   score = score ..";;Copyright "..os.date("%Y") .. " David Bellows\n"
   score = score ..";;This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License\n"

   return score
end

create_finish_orchestra = function(tempo)
   local finish = ""
   finish = finish .."\n</CsInstruments>\n"
   finish = finish .."<CsScore>\n\n"
   finish = finish .."t 0 " .. tempo .. " ; tempo\n"
   return finish
end
  
create_csound_file = function(csound_file,algorithm,style,instrument)
   if style ~="" then
      algorithm = string.gsub(algorithm, "%s+", "_")
      instrument = string.gsub(instrument, "%s+", "_")
      style = string.gsub(style, "%s+", "_")
      style = "_"..style
   else
      algorithm = string.gsub(algorithm, "%s+", "_")
      instrument = string.gsub(instrument, "%s+", "_") .. "_"
      style = ""
   end
   local csound_file_name = instrument .. algorithm .. style--..".csd"
   output_csound_file = io.open(csound_file_name..".csd","w")
   output_csound_file:write(csound_file)
   output_csound_file:close()

   return csound_file_name
end

create_csound_audio_file = function(file,style,sa_author,wave,flac,ogg,tag)
   if wave == "yes" then
      io.write("Generating .wav audio file ...\n")
      os.execute("csound " .. file ..".csd --format=wav -o "..file..".wav --heartbeat=3 -O csound.log")
      io.write("\ndone.\n")
   end

   if flac == "yes" then
      io.write("Generating .flac audio file ...\n")
      os.execute("csound " .. file ..".csd --format=flac -o "..file..".flac --heartbeat=3 -O csound.log")
      io.write("\ndone.\n")
   end

   if ogg == "yes" then
      if tag == "no" then
	 io.write("Generating .ogg audio file ...\n")
	 os.execute("csound " .. file ..".csd --ogg -o "..file..".ogg --heartbeat=3 -O csound.log")
      else
	 io.write("Generating .ogg audio file with tags ...\n")
	 sa_author = string.gsub(sa_author,"%s+","_")
	 style = string.gsub(style,"%s+","_")
	 local tmp = string.gsub(header.dedication,"%s","_") 
	 local dedication = "For:" .. tmp 
	 time_of_creation = os.date("%Y") 
	 os.execute("csound "..file..".csd --ogg -o "..file..".ogg -+id_artist="..sa_author.." -+id_scopyright=5 -+id_software=Platonic_Music_Engine -+id_title="..style.." -+id_comment="..dedication.." -+id_date="..time_of_creation.." --heartbeat=3 -O csound.log")
      end
      io.write("\ndone.\n")
   end
   
   return
end

      


