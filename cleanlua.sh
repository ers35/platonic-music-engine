#!/bin/bash
# Deletes all the files that the software
# generates. Use with caution. You have
# been warned. 

rm ./*.ly*
rm ./*.pdf*
rm ./*.aux*
rm ./*.log*
rm ./*.tex*
rm ./*.flac*
rm ./*.mp3*
rm ./*.ogg*
rm ./*.csd*
rm ./*.wav*
rm lilypond.txt
rm latex.txt
