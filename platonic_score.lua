-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Platonic_Score"

-- local command = "" -- full interactive mode
-- local command = "r" -- random, non-interactive
local command = "Pythagoras"

local generate_log = "yes"

local wave = "no" ; local flac = "no" ; local ogg = "no" ; local ogg_tag = "yes"

local command_line_options = {...}
command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac = get_args(command_line_options,command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac)

local generate_lilypond = "no"
local generate_feldman = "no"
local generate_pattern_15 = "no"
local generate_pattern_35 = "no" 

create_log(generate_log)
header.user_data,header.dedication = get_input(command)

-- Now starts the style algorithm

require("scripts/csound_instruments_tables")
local sonic_events = 15

local audio_len = 20
-- local audio_len = "max"

local algorithm = header.algorithm

-- local form = "Horizontal"
local form = "Vertical"

local style = "Standard" -- Standard
-- local style = "Time Lord Version" -- Start at a certain time
local style = "God-Eye View" -- Compress to fit within audio_len

local instrument = ""

local tempo = 60
if audio_len ~= "max" then
   audio_len = (tempo/60) * audio_len
end

local csound_preamble = ""
local csound_orchestra = ""
local csound_score = ""
local csound_file_name = ""
local style_algorithm_author = "David Bellows"

if audio_len == "max" then
   if form == "Vertical" and style ~="Time Lord Version" then audio_len = 2*((2^32)-1) end
end

local amplitude_table = {} ; local frequency_table = {} ; local start_table = {} ; local duration_table = {} ; local ins_num_table = {} ; local ins_name_table = {} ; midi_ins_num_table = {}

amplitude_table = platonic_generator("amplitude_table",-1,-1,sonic_events,"no") -- -1,-1 means a random num between 0 & 1
frequency_table = platonic_generator("frequency_table",0,136,sonic_events,"no")
duration_table = platonic_generator("duration_table",0,2^32-1,sonic_events,"no") 
start_table = platonic_generator("start_table",0,2^32-1,sonic_events,"no")
ins_num_table = platonic_generator("ins_num_table",1,4,sonic_events,"yes")
pan_table = platonic_generator("pan_table",-1,-1,sonic_events,"no")

do -- create timer
   amplitude_table[0] = 0 ; frequency_table[0] = 0 ; duration_table[0] = (2^32) *2 ; start_table[0] = 0 ; ins_num_table[0] = 0 ; pan_table[0] = .5 ; midi_ins_num_table[0] = 0
end

for counter = 1, sonic_events do 
   frequency_table[counter] = (2^((frequency_table[counter]-69)/12)) * 440 
   amplitude_table[counter] = (amplitude_table[counter] ) * (1/sonic_events) -- Simplistic compression algorithm
   midi_ins_num_table[counter] = 0 -- dummy variable, doesn't matter
end

if form == "Horizontal" then
   start_table[1] = 0
   for counter = 2,sonic_events do
      start_table[counter] = start_table[counter -1] + duration_table[counter-1] 
   end
   if audio_len == "max" then audio_len = start_table[sonic_events] + duration_table[sonic_events] end
end

do -- Time Lord
   if style == "Time Lord Version" then
      random.seed(os.time())
      local voice_to_use = random.random(1,sonic_events) 
      local new_start = start_table[voice_to_use]
      for counter = 1, sonic_events do
	 if start_table[counter] >= new_start then 
	    start_table[counter] = start_table[counter] - new_start 
	 else
	    if start_table[counter] + duration_table[counter] > new_start then
	       duration_table[counter] = start_table[counter] + duration_table[counter] - new_start
	       start_table[counter] = 0
	    else
	       start_table[counter],duration_table[counter] = 0,0
	    end
	    
	 end	 	       
      end      
   end
end

do -- God's Eye View
   if style == "God-Eye View" then
      local god_max = 0
      if form == "Vertical" then god_max = (2^32)-1
      else god_max = start_table[sonic_events] + duration_table[sonic_events]
      end

      local scale = 0
      if form == "Vertical" then
	 scale = (audio_len/2) / god_max
      else scale = (audio_len) / god_max
      end
      
      for counter = 1,sonic_events do
	 start_table[counter] = start_table[counter] * scale
	 duration_table[counter] = duration_table[counter] * scale
      end
      duration_table[0] = audio_len
   end
end

do -- Record just for the audio length
   if style ~= "God-Eye View" then
      for counter = 1,sonic_events do
	 if start_table[counter] > audio_len then
	    start_table[counter],duration_table[counter] = 0,0
	 else
	    local current_duration = start_table[counter] + duration_table[counter]
	    if current_duration > audio_len then
	       duration_table[counter] = audio_len - start_table[counter]
	    end
	 end
      end
   end
   duration_table[0] = audio_len
end


do -- Create Csound
   require("scripts/csound") ; require("scripts/csound_instruments_tables")
   do -- Csound preamble
      csound_preamble = create_csound_preamble(algorithm,style,style_algorithm_author,time_of_creation)
   end

   local start_of_table = 0
   do -- create specific Orchestra instrument
      if form == "Vertical" then 
	 orchestra_instrument = "Timer" ; instrument_num = 0 ; ins_name_table[instrument_num] = orchestra_instrument
	 csound_orchestra0 = create_csound_orchestra(orchestra_instrument,instrument_num)
	 csound_score_function0 = create_csound_score_function(orchestra_instrument,instrument_num)
	 start_of_table = 0
      else csound_orchestra0 = "" ; csound_score_function0 = "" ; start_of_table = 1
      end
   end
   
   do -- create specific Orchestra instrument
      orchestra_instrument = "SineWave" ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)
   end
   
   do -- create specific Orchestra instrument
      orchestra_instrument = "Sawtooth" ; instrument_num = 2 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra2 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function2 = create_csound_score_function(orchestra_instrument,instrument_num)
   end

   do -- create specific Orchestra instrument
      orchestra_instrument = "Square" ; instrument_num = 3 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra3 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function3 = create_csound_score_function(orchestra_instrument,instrument_num)
   end

   do -- create specific Orchestra instrument
      orchestra_instrument = "Triangle" ; instrument_num = 4 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra4 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function4 = create_csound_score_function(orchestra_instrument,instrument_num)
   end

   csound_score_ins = create_csound_score_ins(start_of_table,orchestra_instrument,sonic_events,ins_num_table,ins_name_table,amplitude_table,frequency_table,duration_table,start_table,pan_table,midi_ins_num_table)

   csound_score_ins = finish_csound_score_ins(csound_score_ins)

   csound_orchestra = csound_orchestra0 .. csound_orchestra1 .. csound_orchestra2 .. csound_orchestra3 .. csound_orchestra4

   csound_score = csound_score_function0 .. csound_score_function1 .. csound_score_function2 .. csound_score_function3 .. csound_score_function4 .. "\n" .. csound_score_ins 

   local orchestra_finish = create_finish_orchestra(tempo)
   csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
   csound_file_name = create_csound_file(csound_file,algorithm,style,instrument)
end

do -- Create Audio
   create_csound_audio_file(csound_file_name,style,style_algorithm_author,wave,flac,ogg,ogg_tag)
end

do -- create Feldman score
   -- require("scripts/feldman_notation")
   -- local volume = "no" ; local fudgefactor = feldman_fudge.platonic -- regular, platonic 
   -- local lowrange = -1130427 ; local highrange = 950340
   -- feldman_score,latexname = midi2feldman(note,duration,velocity,instrument_name,number_of_notes,volume,fudgefactor,lowrange,highrange,temperament_name,pretty_system)
   -- create_latex(feldman_score,latexname)
end

do -- Create MIDI Score
   -- require("scripts/midi_score")
   -- midi_score,latexname = midi2midi_score(temperament_name,note,audio_freq,duration,tempo_number,velocity,bend,instrument_name,algorithm,number_of_notes,midi_pitch_tuned_to,ratio_list,ratio_table,calculate,pretty_system)
   -- create_latex(midi_score,latexname)
end 

