%****** Begin preamble ***********
\documentclass[extrafontsizes,oneside,12pt]{memoir} 
% openany = chapter on every page (not just right page)
% oneside eliminates different margins on each page (left v. right)

\input{style/preamble.tex}

% ****** paragraph indentation: eliminating it *********
%\usepackage{parskip}
\usepackage{lilyglyphs}
\usepackage{alltt}
% ****** TODO
%\usepackage{luatodonotes}

% ********** Uncomment to get rid of extra space after period
\frenchspacing
\newcommand{\release}{Pythagoras}
\title{Style Algorithms Manual for the Platonic Music Engine \\ alpha release \textit{\release}}
\author{David Bellows}
\date{\today}

%********* End preamble

\newcommand{\pme}{\textit{PME}}
\begin{document}
\thispagestyle{empty}

\pagestyle{companion}
\makeheadrule{companion}{0pt}{0pt}

\newcommand*{\fixedsharp}[1][]{%
\setkeys{lilyDesignOptions}{scale=1.5,raise=1.7}%
\lilyPrint[#1]{\hspace{.5pt}\lilyGetGlyph{accidentals.sharp}}%
}

\newcommand*{\fixedflat}[1][]{%
\setkeys{lilyDesignOptions}{scale=1.5,raise=1.7}%
\lilyPrint[#1]{\hspace{1.5pt}\lilyGetGlyph{accidentals.flat}}%
}

\includepdf[noautoscale=true]{logo}

\frontmatter

\maketitle
\newpage
\input{frontmatter.tex}

\newpage

\chapter*{Introduction}

The \textit{Platonic Music Engine} is an interesting beast. Unlike most programs you use it is not a single program made up of a bunch parts. In fact it's only the parts. But those parts can be assembled together to do some interesting things! Those interesting things are what this manual is about.

The \pme{} is a collection of function calls spread across a number of files. By themselves they don't do much. Or they do but it's of little value. But if you arrange them together in a certain order and do some extra processing on the results, you can generate some music based on any musical or artistic idea that has ever existed or will ever exist.

I say ``based on'' but really many of them are more of superficial simulations of the original inspiration.

Within the \pme{} project are several such collections of function calls and extra processing intended to simulate, or in some instances create, a specific musical idea. These things are called \textit{Style Algorithms} and are thus the subject of this manual.

This manual assumes you have read through the \textit{Tutorial} and have a basic familiarity with editing \pme{} files and producing results.

It's also worth noting that this manual is \textit{not} about designing your own SAs. Such a tutorial would indeed be useful and will happen some day. But for now this, the \textit{Tutorial} and the \textit{Reference Manual} is all we've got.

\newpage

\tableofcontents

\mainmatter

\chapter{Praeludium I by JS Bach}

\epigraph{\textit{This chapter describes how to use the file \texttt{praeludium\_I.lua}.}}

This Style Algorithm is based on the very popular and well known \textit{No.1 Prelude in C Major} from JS Bach's \textit{Well-Tempered Clavier} (BWV 846).

I try to make the code in all the Style Algorithms follow the same pattern. Partly out of necessity (certain functions have to be called before other ones) but also in order to make them easier to use (if you understand one you hopefully will be able to understand the others). The first major difference that occurs is with the \texttt{number\_of\_notes}.

You'll see that we start with setting a number of measures instead of notes. Since Bach's piece clearly follows various patterns built around measures I think this makes sense. Note that you must indicate at least 4 measures (otherwise the software will crash). This includes three measures of the main body, a kind of cadenza and then the final closing chord. So 5 measures really. 

I consider the 4 measure minimum requirement a bug. We should be able to create versions with 1 measure of the main idea and then the two others. Plus the number should equal the \textit{total} number of measures generated and not the total minus 1. Or it should equal the total following the basic pattern. Something, anything better than what it currently does.

After this the SA generates the \texttt{number\_of\_notes} it needs for each of the three voices to be used. One of the peculiarities of how the \pme{} handles sheet music is that it treats every line of music as a voice. I won't go into it here (see the \textit{Reference Manual} for more info) but looking at the original score the lowest notes have to be their own voice, the ones that occur in the lower staff above the previous notes are in their own voice and then, of course, the notes in the upper staff comprise their own voice.

It all has to do with keeping the audio and sheet music output manageable.

We then set the \texttt{key} to \texttt{``c-major''} and the tuning to \texttt{``c-Werckmeister 1''}. The key is obvious (from the original). The tuning perhaps less so. The \textit{Well-Tempered} part of the name of the work \textit{apparently} comes from Bach's experiments with alternate tunings for whatever keyboard instrument he was using. This isn't the place for a long discussion on the point but a very general consensus seems to be that Bach did indeed compose this music at least in part to demonstrate some new tuning. Bach was friends with theoretician Werckmeister so it makes sense to assume it was one of his tunings. We have three of them available in the \pme. I just chose the first.

But of course you don't have to preserve any of those features! That's the beauty of the \pme{}---total freedom to manipulate the music however you want. I just set those as the default for the reasons stated above.

Nothing much more of interest till we get to the \textit{Velocity} section. It's pretty common Baroque practice to change up how you play repeats, like add ornamentation or at least change the volume. We don't have the ability to add ornamentation\footnote{Yet. But holy crap that'd be cool!} but we can at least \textit{suggest} probabilistically that the \pme{} try to do so.

The \textit{Duration} section will be overwritten by the rest of the SA so it doesn't really matter what we do here. In fact we could leave it out entirely and the SA would still work but I keep it in there for consistency's sake.

And then finally we call the function that generates this specific musical idea. But at this point you really have all the information you need to successfully use this SA. The rest of this chapter just goes a bit deeper into some of the technical things going on.

\section{generate\_prelude}

After some variable and table declarations we borrow some ideas from the \textit{bel canto} Style Algorithm and take all the pitches we generated before, squeeze them down to the middle range of our instrument and divide them into groups of five notes.

The idea is that the first pitch goes to the that first lower note in the lower staff. The second pitch goes to the first note above the previous one also in the lower staff. And then the remaining three make up the first three notes in the upper staff. All five of those notes together make up the entirety of the content for the first measure.

The lowest two voices get repeated and the upper three are used to perform that recognizable pattern from Bach's original.

The function then just repeats the thematic material, pulls the next set of five notes, shapes them as above, repeats, and then continues to repeat the cycle as long as needed.

The final two measures are constructed differently.

Overall this is a very brute-forcish method of creating these notes and it could be a lot cleaner. 

At the very end we make sure all of our notes fit within our instrument's range. This is such a useful function that it should be expanded and made available for all Style Algorithms.

All of the data we've generated then gets returned to the main file.

\section{generating output}

Once again another hack---increasing the number of notes used by three to take into account the big chord we did at the end.

The code from here on out is exactly the same as seen with the \textit{Simple Melody} SA\footnote{from the \textit{Tutorial}.} except that the Csound \textit{score} part is tripled. This is because each voice we generate becomes its own chunk in our Csound file.

I designed the Csound code to operate in this manner so that we can generate as many voices as we want. Each voice gets assigned to its own score variable and then they are all combined at the end and written to a Csound file.

\subsection{sheet music}

The Lilypond section operates in a similar manner as the Csound section does above with respect to generating the data for each voice separately. I think this is good modular design. Maybe? At least it was easier for me to do than any other option.

One thing of note here is that we set \texttt{rest\_or\_space} to \texttt{``s''} (for \textit{space}). Otherwise there would be a whole mess of rests printed out for any instrument that uses two staves (like the piano). For instruments with one staff it shouldn't make a difference.

You can try experimenting with the \texttt{avoid\_collisions} setting if you have problems with rests colliding with other elements. It's not a great tool but sometimes it helps.

It's also interesting that the order in which the Lilypond notes are generated is different than the order they are sent for final processing. This has to do with how Lilypond handles voices and stem direction. According to recent activity on the mail list this very thing might be changing in the future so we might have to alter this a bit.

\subsection{graphical scores}

A big feature missing in the \pme{} is that the graphic notation systems can only handle one voice. I would suggest choosing the upper voice just to keep it somewhat interesting. I already have a plan that will allow \textit{Pattern 15} and \textit{Pattern 35} to handle multiple voices in the near future.\footnote{The \textit{Feldman Graph} system will be trickier and maybe even impossible.}

\chapter{platonic score}

\epigraph{\textit{This chapter describes how to use the file \texttt{platonic\_score.lua}.}}

The \textit{Platonic Score} is an interesting holdover from the previous version of the \pme{} that has mutated way beyond what it was.

The idea behind it is to use the full possible range of values available in the underlying technology. Previously this was with MIDI. Now it's with Csound. Csound is far more expansive than MIDI. This means the ``full possible range of values'' is a lot more to wrangle.

So the original version was one voice, a melody, in 196,000-EDO with durations ranging from 1,000ths of a second to three seconds (depending on the tempo), with 127 divisions of volume from total silence to as loud as possible.

With Csound we are not limited to one voice and thus allow for tens of thousands. We can use somewhere around 400 million-EDO,\footnote{Csound itself is capable of like 11 quadrillion-EDO. It's the \pme{} that has lower limits.} with 4 billion gradations of volume, and durations from 10 millionth of a second to 3 years (depending on the tempo).\footnote{Again, Csound can generate a single note lasting 3 billion years but the \pme{} can't generate numbers like that. Sadly.}

All of these vast increases in range and capability means that I have expanded on the \textit{Platonic Score} for this release of the \pme.

Right off the bat you'll notice that we no longer specify \texttt{number\_of\_notes} but instead use \texttt{sonic\_events}. Same results, really, except that we do not necessarily play these in order in one voice for one melody using one instrument. The name change will make sense later. This can be any number but once you get into the thousands the effects aren't quite what you might expect. But it's fun to play with a wide range of numbers.

We also specify the \texttt{audio\_len}. This is either the time in seconds (assuming a tempo of 60bpm) or \texttt{``max''} (for \textit{maximum}) which is defined later based on further decisions.

The \texttt{form} variable has two options: \texttt{``horizontal''} or \texttt{``vertical''}. If you choose \textit{horizontal} then all your sonic events will play one after the other (mimicking the original \textit{Platonic Score}). \textit{Vertical} means they all play at the same time. Or more accurately the entire piece is given a total length of twice the longest sonic event's duration. And when sonic events begin (and thus end) during that total length is determined by the \pme. This means that when the piece begins there might not be any sound at all, for example.

The \texttt{style} variable has three options:

\texttt{``Standard''} uses the full the range of all the possible values (audio frequency, duration, and amplitude (volume)) which means an individual note can last 136 years or so.\footnote{At slower tempi this can reach into the thousands of years.} Whether your computer can manage this is another story. Plus whether you chose \textit{vertical} or \textit{horizontal} above makes a big difference. If \textit{vertical} then the entire piece will last at most around 272 years. But if you choose \textit{horizontal} then you're looking at a high end of 136 years \times{} the number of \textit{sonic events.} Fortunately you don't have to listen to the entire work. If you choose an \texttt{audio\_len} of less than \texttt{``max''} and more in line with a normal human lifespan, the \pme{} will truncate the piece at that time position. Unless you have like 10,000 sonic events\footnote{That's a guess. I'm sure the math could be done by \textit{someone.}} the odds are against actually hearing anything within like a day's worth of music. Just sayin'.

The \texttt{Time Lord Version} keeps everything the same as \texttt{Standard} but handles the \texttt{audio\_len} differently. In the \texttt{Standard} version the \pme{} generates its audio starting from the beginning of the piece regardless of what is happening. The \texttt{Time Lord Version} begins generating the .csd/audio files beginning from where it knows that a sonic event has just started. So even if you choose the horizontal option at \textit{max} length, the \texttt{Time Lord Version} will make sure you get to hear something. Interestingly, the specific sonic event that you are travelling in time to hear is chosen at random. Really random. Not \pme{} random. Why? Because the underlying data is still the same \pme{}-stuff, it's just which part we're listening to that is random. Make sense?

Why the name? The name was chosen like this. Imagine you are something called, oh, I don't know, a \textit{Time Lord}. And you have some crazy device that allows you to travel through time. But this device does not let you control it. Instead it puts you where it thinks you need to be. Like, say, at the \textit{beginning of a \textbf{sonic event!!}}

Probably the most interesting from a conventional point of view, is the \texttt{God-Eye View} option.\footnote{The non-standard spelling has to do with file name conventions and my lack of desire to create yet another parser for such things.} It takes its inspiration from the Christian Bible in the second epistle attributed to Peter chapter 3 verse 8:

\begin{quote}
But do not ignore this one fact, beloved, that with the Lord one day is like a thousand years, and a thousand years are like one day.
\end{quote}

\noindent{}Which parallels a verse from the Hebrew Bible in Psalms chapter 90 verse 4:

\begin{quote}
For in Your sight a thousand years are like yesterday that has passed, like a watch of the night.
\end{quote}

The idea is instead of listening for 287 years to hear the entire \textit{Platonic Score} it is compressed down to whatever amount of time you set in \texttt{audio\_len}. All the start times and durations are scaled to fit within that length of time. So a \textit{God's Eye View}, if you will. So if you choose 20 seconds then you'll hear the entire 287 year piece in 20 seconds. All the sounds are the same it's just that they start sooner and last less time.

After choosing any of these options, you can set the tempo. The default is \texttt{``60''} which allows \texttt{audio\_len} to be the equivalent of seconds. You can change this, of course, and the \pme{} will adjust \texttt{audio\_len} accordingly to ensure that you still get the same length of time.

A few other variables are set/declared and then the processing begins.

Unlike most other Style Algorithms all the processing is done in this file. It probably shouldn't be that way. Sorry. I'll get around to fixing that some day.

I'm not going to go through all of this here. But one thing worth noting is that there is a \textit{timer} instrument created that is used for vertical pieces. It ensures that the whole piece will be as long as it's supposed to be.

\section{generating output}

Once again we see the flexibility of the Csound function. In the \textit{Praeludium 1} SA we generated three different scores for one instrument. In this SA we generate four different instruments that use only one score.

The instruments were chosen to resemble the one instrument used in the old version of this SA, a sine wave. I understand little to very little about sound synthesis and I just chose four easy to implement synthesized instruments built into Csound.

But it's the modular action taking place that is of note and how everything, again, is just combined in the end and it works.

As of now there is no graphical output available for this Style Algorithm (other than the .csd file). Adapting any of the existing notation functions to work with the \textit{Platonic Score} will be non-trivial and not something I'm going to be tackling anytime soon.

\chapter{musical mesostics}

\epigraph{\textit{This chapter describes how to use the file \texttt{musical\_mesostics.lua}.}}

A \textit{mesostic} is a kind of acrostic poem except where with acrostics the first letter of each line spells out a message, it's a letter in the middle that spells out the message.

American composer John Cage developed this form of writing poetry in the late 1960s. As time went on he modified the technique to be a means of deriving a text from an existing one. He would select a \textit{spine} (the message) and would then go through another text (like a book) and pull out the words that had the letters he needed for his spine. His methods grew complicated and restrictive in time and eventually he used a computer to do the work (thus freeing the results from his own ego, his own memories, and his likes and dislikes).

Here is an example of the earlier, consciously composed form on the name \textit{Marcel [Duchamp]:}

\begin{alltt}
             a utility a\textbf{M}ong 
                      sw\textbf{A}llows
                 is thei\textbf{R}
                    musi\textbf{C}.
                      th\textbf{E}y produce it mid-air
             to avoid co\textbf{L}liding.
\end{alltt}

And one derived from an existing text (\textit{Finnegans Wake} by James Joyce) using the spine \textit{James [Joyce]:}

\begin{alltt}
           whase on the \textbf{J}oint
                      wh\textbf{A}se
                     foa\textbf{M}ous
                     old\textbf{E}
                       a\textbf{S} you

\end{alltt}

Given his status as a composer, I'm a little surprised that neither he, nor anyone I've heard of, ever created a musical analogue of his mesostic process. The most obvious parallel would be to take a work from an existing composer, say Beethoven's \textit{Sonata Pathetique}, and then search through it to find the notes of, say, Bach's \textit{Prelude 1} from the \textit{Well-Tempered Clavier}. Right? I mean, it's obvious.

But perhaps that's why \textit{he} didn't do it. But anyone else?

Anyway, I decided to make a Style Algorithm based on his mesostics. I didn't want to use the method above as it doesn't really leave any room for the \pme{} to do anything of interest other than just search one piece of music for another.

Another approach would have been to use existing works (like Beethoven) and then search for a \pme{} generated melody within it. But this would have been of little interest to the user.

Instead I went with the approach you'll see here. I let the \pme{} generate the source material in its own way and then search for whatever melody the user wants. To make the spine obvious (like in the text examples above), I have the background music play softly (\lilyDynamics{pppp}) and the spine melody play loudly (\lilyDynamics{ff}). The sheet music also prints out the spine note in red along with a couple of other stylistic options we'll discuss later.

First thing you'll notice is that I specify by default a lot of notes (300). You need a large amount of source material if you want to stand a good chance of finding your spine (though there are other ways to accomplish this).

When it comes to the scale you'll probably want to use a chromatic or panchromatic scale in order to be sure that all the notes in your melody are possible to find. If you're careful you don't need to do this but by default it's wise.

The default value for velocity is \lilyDynamics{pppp}. I suggest you stick with this or something else of low volume in order to generate a noticeable contrast with the spine.

For the duration I just have 16th notes. I think this produces better results but feel free to add more variety to it.

There are several options to set for this SA. The first, \texttt{repeat\_motif}, if set to \texttt{``yes''} will go through the entire source score finding as many examples of the spine as it can. Otherwise it will end after finding the first one.

The \texttt{colorize} option tells the printed score to color the spine notes red if set to \texttt{``yes''}.

The \texttt{format} options provides three different ways to format the sheet music. The best one is \texttt{``acrostic''} which puts each spine note at the beginning of its own line of music. The \texttt{``mesostic''} choice makes a poor attempt at recreating the prose version by putting the spine note in the middle of its own line. It does this poorly and it's more like each spine note is somewhere on its own line almost approximating the middle. \texttt{``Standard''} uses no special formatting and prints out the score normally.

\section{constructing a spine}

Let's look at what goes into adding a new spine/melody for use in the \pme.

Open up the \texttt{musical\_mesostic\_tables.lua} file in the \texttt{scipts} directory. Scroll down a bit and you'll see the entries. The entry name is easy, it's the name we're giving to the spine, like \textit{Beethoven's Fifth} or, if I get around to it, \textit{Happy Birthday}. The final value \texttt{composer} is also obvious.

It's the \texttt{motif} bit that needs explanation.

First, ignore the Beethoven ones as they are too on the nose. The Bach and Mozart ones are how it should be done from here on out. You'll notice that there is no key information and since they are constructed in terms of intervals this means they are infinitely transposable. Since there is no base key you do not have to make sure that \texttt{P1} (the perfect unison or tonic) matches up with whatever key the piece was originally in. In fact you make \texttt{P1} equivalent to the \texttt{lowest} pitch in the melody. I know, this goes against how we music theory types think of music but it's what adds flexibility to the routine.

Part of this need for flexibility is how we choose the starting note for the instrument. It's very simple and brutish. We add the pitch numbers for the high and low range of the instruments and divide by two and that becomes \texttt{P1}. Obviously this will be different for every instrument which is why we don't even both trying to preserve keys.

A short-coming to be corrected in the next release is that we only support one octave of calculated scale degrees (\texttt{P1} -- \texttt{P8}) so you have to make sure your melody fits entirely within one octave.

And then just fill in the values in the order to appear in the melody and you're done.

\section{generating output}

If you noticed in the file \texttt{musical\_mesostics.lua} there are two strange values that get returned: \texttt{lilypond\_spot} and \texttt{red\_spot}. These do not get used in generating the Csound file---everything there happens as normal.

It's when we get to the Lilypond section that these values are used. There is a new command inserted into our Lilypond file that creates the command for converting a pitch to having a red note head. But then a little later there is an entirely new Lilypond function that lives in \texttt{scripts/mesostics}. Those two variables hold where new lines are to be started and which pitches receive the coloring. For the \texttt{acrostic} style these point to the same note. For the \texttt{mesostic} style these points differ.

This SA shows off the flexibility of the Lilypond generating routine. We can insert some processing in-between the standard flow in order to achieve certain effects.

After this the \textit{Pattern 15} and \textit{Pattern 35} are completely unchanged and perform exactly as expected.

\chapter{one note}

\epigraph{\textit{This chapter describes how to use the file \texttt{one\_note.lua}.}}

There was a discussion on Reddit concerning definitions of \textit{rhythm} and \textit{melody}. Some proposals were made. I thought they were a bit restrictive and to make my point idly wondered what would happen to those definitions if we had a piece of music that was just one note played one time.

It was an interesting thought experiment but I had no desire to pursue it as an actual piece of music.

And then in a conversation with my composer friend \href{http://www.mxcollins.com}{Michael Collins}, the subject of form came up and again I wondered what a piece comprising but a single note played once would do to his definition of form.

I still had no desire to write such a work myself as it was obvious (given a universe with Cage's \textit{4'33''}) and I assumed that someone, somewhere, sometime, must have written such a piece (Fluxus I'm looking at you).

But then I began to wonder how I would compose such a work for the \textit{Platonic Music Engine.} That presented an interesting challenge.

The obvious and uninteresting approach would be just to set \texttt{number\_of\_notes} to \texttt{``1''} and be done with it. I figured I could do better.

So instead I decided to generate a ton of notes using a crazy \textit{n-}EDO and average all of it together. The pitches, durations, and dynamics of a boat-load of notes averaged. And this is what we have.

A decent understanding of math will tell you that if you do this within a fixed range using random numbers, the average should, in time, converge onto one particular value. And that's what you see happening here.

If you only generate two or three notes then the result could be anywhere. But as that number increases the odds are that it will approach a specific note. But given the finite constraints of the \pme{} you still won't get the exact same note even with 100,000 notes. For example here are the audio frequencies of three runs with the generated averages using randomly generated input values:

\begin{verbatim}
           336.57348430245
           338.36785618219
           338.83021307374
      Avg: 337.923851187
\end{verbatim}

The distance between the highest and lowest values is about 11.5 cents. The average person can hear differences as low as 4 or 5 cents. A standard semitone or half-step is (like from C to C\fixedsharp) is 100 cents. So the difference is really small but is just noticeable. But that's using a 1 million-EDO tuning. Using standard 12-EDO:

\begin{verbatim}
           311.12698372208
           391.99543598175
           311.12698372208
      Avg: 338.083134477
\end{verbatim}

Here we see a much bigger difference but that's because the distance between individual notes is much greater. Now the distance between the lowest and highest is 400 cents. That's a major third!

But let's up the trials by one order of magnitude using our original tuning. With 1 million notes:

\begin{verbatim}
           339.15188792543
           339.28402984391
           339.80769518755
      Avg: 339.414537653
\end{verbatim}

Now the difference between the highest and lowest is 3.4 cents which is a difference that can't be heard by most people. The convergence is more clear now.

And with 12-EDO:

\begin{verbatim}
           349.22823143301
           369.99442271164
           369.99442271164
      Avg: 363.072358953
\end{verbatim}

And again we can see the results converging. The difference between the lowest and highest note is exactly 100 cents or our semitone. This is what we would expect given that we are using a 12-EDO tuning with a panchromatic scale.

I could go on and on with more data and more examples and generate actually significant results but I think the point is made. With enough trials the values are converging onto a single note.

Why do I keep harping on this convergence? Because \ldots

\section{generating the one note score}

We have at our disposal a specific graphical score just for this Style Algorithm. It prints a big circle in the middle of the page (a note head without a stem) with a rather poetic bit of prose talking about how given infinite time and infinite iterations all notes converge onto \textit{One Note.} That doesn't really mean anything\footnote{Or what it means literally is nonsensical.} but it \textit{sounds} deep. That's all we go for, being superficially similar to profundity.

\section{time}

It is worth noting that while using millions of notes and million-EDOs insure a better convergence, it also takes a significant amount of time. Like dozens of seconds or even minutes. If you use figures that are too big it might even freeze up. So be careful and ramp things up slowly.

\chapter{serialism}
\epigraph{\textit{This chapter describes how to use the file \texttt{serial.lua}.}}

Serialism is an approach to composition that stands in stark contrast to the conventional methods in use in Western culture based on harmony and tonality. Though elements of it aren't all that different.

The basic idea is this, you create a set of notes and then you keep repeating it, preserving the set, but transforming it. Like transposing it up or down a certain number of steps. Or playing it backwards. Or inverted (where C to G, up 7 semitones, becomes C to F, down 7 semitones). Or backwards, inverted, and transposed all at the same time.

When you think of conventional music this isn't all that foreign. For example in Beethoven's famous fifth symphony the opening motif (da da da dummmmm) comes back in different instruments, inverted, transposed, and augmented (lengthened in time).

This isn't to say that serialism = Beethoven but just to point out that the use of sets of notes in some manner has always been with us.

Serialism became best known early on through the creation of a specific method called the \textit{Twelve Tone Method} by Arnold Schoenberg. The constraining rule he used was that you couldn't use the same note (excepting immediate repetition) of the chromatic scale (the standard 12 notes in use in Western music) before you play all the other notes. So you create an initial arrangement of those twelve notes, called a \textit{tone row}, which you then use throughout the rest of the piece.

And just like Beethoven did with his famous motif, you can play your created tone row transposed, retrograde (backwards), inverted, or retrograde-inverted. The big point of twelve tone music, though, is to avoid the tonal center of conventional Western music which establishes the tonal center through \textit{voice leading} and partially through sheer volume by having certain intervals occur more often than others. So if you were to look at a graph of the notes in a piece of Beethoven you see that that the tonic and dominant occur more often than any other notes and you would also see certain patterns in their movement.

Twelve tone music, on the other hand, tries to smooth out the distribution of the notes thus undercutting one of the consequences of conventional tonality.

I could go on but presumably you either already know all this or can find a far better primer than this.

We can consider Schoenberg's Twelve Tone Method as a subset of serialism. Where Schoenberg's method concerned only the pitches, serialism (in many of its forms---there are many) not only manipulates sets of notes but applies the same process to duration and dynamics.\footnote{And other things like timbre or register or whatever elements the composer can think up to be treated as a set of values.}

It turns out that the standard forms of transformation (transposition, retrograde, inversion, and retrograde-inversion) are very amenable to computer manipulation. It's like all mathy and stuff.

This Style Algorithm will generate a tone row which then gets manipulated (\textit{serialized}) using the transformations you provide. It can also \textit{serialize} the durations and dynamics if you want. Let's dive in.

The \texttt{number\_of\_notes} is interesting as this SA ignores this number when generating all the transformations you will specify later. But what this value is used for is the initial set of notes from which the SA derives your tone row. We'll go more into this in a moment but the idea is the more notes you specify here the more likely the SA will create a tone row that uses all the notes of your scale.

I specify a panchromatic scale by default which would be more in line with Schoenberg but honestly using other scales works nicely as well. A blues scale is one I find particularly compelling.

Likewise I chose standard Western tuning of 12-EDO. A Just Intonation tuning kind of compromises the ``equality'' of the pitches but so what? And of course you can go crazy with like 137.3-EDO if you want.

You shouldn't really do anything with emphasizing certain pitches or octaves as this information will get largely washed out when the SA computes its tone row. If you allow repetition (see below) then emphasizing certain scale degrees can make it more likely that those pitches get repeated in your tone row, so that's something. But the \texttt{octave\_recipe} stuff is completely unnecessary. I've left all of these out by default but feel free to play with the \texttt{scale\_recipe} if you want.

Contrariwise, indicating \texttt{velocity\_recipe} and \texttt{duration\_recipe} does make sense. This is like setting the scale for pitches.

And now begins the fun part, specifying the transformations we're going to use.

In the first section on serializing pitches we specify the \texttt{serialize\_recipe} variable. This is the sequence of transformations we are instructing the \pme{} to perform upon our tone row. The letters are as follows:
\\\\
\begin{tabular}{r c l}
  P&=&Prime\\
  R&=&Retrograde\\
  I&=&Inversion\\
  RI&=&Retrograde-Inversion\\
\end{tabular}
\\\\
\texttt{Prime} is what we use for the initial tone row.

The syntax of the commands are \texttt{T:x} where \textit{T} is the transformation command in the table above and the \texttt{x} is the transposition in calculated scale degrees or actual steps (using a number) either up or down (with a negative sign prepended to the interval/number). You can create a recipe of any length and the \pme{} will create as many notes as needed.

So from the example in the file:
\\\\
\begin{tabular}{r c l}
  P:P1&=&Prime (tone row) in its root position\\
  P:-1&=&Prime lowered one panchromatic step\\
  I:P1&=&Inverted tone row in the root position\\
  R:P1&=&Retrograde tone row in the root position\\
  RI:P8&=&Retrograde-inversion of the tone row transposed up an octave\\
  P:-M7&=&Prime transposed down a major seventh\\
\end{tabular}
\\\\
Hopefully the commands are intuitive. Especially if you are already familiar with twelve tone theory.

It's interesting that I allow the user to specify the transformation commands. We could have the \pme{} generate the commands as well---and perhaps we/someone will add that functionality someday---but I like this level of interaction.

We also have to set the variable \texttt{allow\_repeat} to indicate if you allow for immediate repetition of pitches in our tone row. After sending all of our commands to the \texttt{twelve\_tone} function we update our number of notes to reflect the pitches we have generated. The \texttt{velocity\_table} and \texttt{duration\_table} also have their values generated such that they track their original pitches. 

You can then serialize either or both of \texttt{velocity\_table} and \texttt{duration\_table}. You  use similar commands except there is no transposition. So it's just the four letters (\texttt{P, R, I, RI}). Whichever of these you do not serialize will keep their same initial values as determined by serializing the pitches.

\section{calculating the tone row}

It's worth looking at how the tone row is calculated.

So let's say the \pme{} has generated the notes C, E, E, E, C, and G in the normal way that the \pme{} generates pitches.

The \pme{} goes through this set of notes and finds a tone row such that each note is only used once.

If you have set \texttt{allow\_repeat} to \texttt{``no''} then when constructing the tone row the second and third E's and second C will be ignored resulting in the tone row: C E G.

If you have set \texttt{allow\_repeat} to \texttt{``yes''} then the \pme{} will allow that second and third E to be part of the tone row since they immediately follow the first but will ignore the second C since it is not an \textit{immediate} repetition in the source material, resulting in: C E E E G.

I know our definition of \textit{immediate repetition} doesn't really coincide perfectly with Schoenberg's invention but hopefully it makes sense to you as a method. It is entirely at the mercy of the random nature of the \pme{} while still being under the influence of the user's initial settings.

\section{generating output}

Generating standard sheet music or any of the graphical scores is entirely straightforward and follows normal procedure. Nothing to comment on.

\backmatter

%********** Index, notice there is no \chapter business here *******************
%\phantomsection % This is needed otherwise the hyperlink to this section is wrong
%\addcontentsline{toc}{chapter}{Index}
%\printindex
% * ******** If you use Acknowledgments then you'll want to use the alternate colophon.tex altcolophon.tex ********
%\section*{Acknowledgments} But not in the way you might have been asking. 

\input{style/colophon.tex}
%\input{altcolophon.tex}

\end{document}
