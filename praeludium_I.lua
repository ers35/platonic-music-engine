-- Platonic Music Engine -- manipulate music in interesting ways.
-- Copyright (C) 2015 David Bellows davebellows@gmail.com

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

require("scripts/header")   
header.algorithm = "Prealudium I"

local pitch_table = {} ; local velocity_table = {} ; local amplitude_table = {}
local frequency_table_1 = {} ; local frequency_table_2 = {} ; local frequency_table_3 = {}
local duration_table = {} ; local pan_table = {} ; local csound_frequency_table = {}
local start_table_1 = {} ; local start_table_2 = {} ; local start_table_3 = {}
local csound_frequency_table_1 = {} ; local csound_frequency_table_2 = {} ; local csound_frequency_table_3 = {} 
local ins_name_table = {} ; local ins_num_table = {} ; local midi_ins_num_table = {} 

local midi_ins_number = 0

-- local command = "" -- full interactive mode
-- local command = "r" -- random, non-interactive
local command = "Pythagoras"

local generate_log = "yes"

local generate_lilypond = "no"
local generate_feldman = "no"
local generate_pattern_15 = "no"
local generate_pattern_35 = "no" ; local enforce_nine = "no"
local wave = "no" ; local flac = "no" ; local ogg = "no" ; local ogg_tag = "yes"

local command_line_options = {...}
command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac = get_args(command_line_options,command,generate_log,generate_lilypond,generate_feldman,generate_pattern_15,generate_pattern_35,ogg,flac)

create_log(generate_log)
header.user_data,header.dedication = get_input(command)

local number_of_measures= 4 -- Must be at least 4
local number_of_notes_1 = 14 * number_of_measures +1--+ 30
local number_of_notes_2 = 4 * (number_of_measures -1) +3--+ 4
local number_of_notes_3 = 2 * (number_of_measures -1) +2
local number_of_notes_total = number_of_notes_1 + number_of_notes_2 + number_of_notes_3 +15

local csound_instrument = "MIDI" ; local sound_font = "fluid.sf2"  
local instrument = "Piano"
local algorithm = header.algorithm
local style = "" -- 
local style_algorithm_author = "David Bellows"

local key = "c-major"
local tuning_name = "c-Werckmeister 1"--Western Standard"
local system = "western iso"
header.reference_pitch,header.reference_pitch_name = reference_pitch(system)

local tempo_word = "adagio"
local tempo_number = tempo_value(tempo_word) 

do -- Generate audio frequencies
   audio_freq_table,ratio_table,header.tuning_octave,low_freq,high_freq = generate_audio_frequency_table(tuning_name)
end

do -- Calculate degrees for tuning
   calculated_scale_degrees = generate_relative_scale_degrees(ratio_table,key)
end

local scale_octave = get_scale_octave(key,calculated_scale_degrees) 
midi_ins_number,header.lowrange,header.highrange = instrument_range(instrument,audio_freq_table,low_freq,high_freq) 

do -- Pitch Table
   local scale_recipe = "P1:4,M3:2,P5:2"
   local scale_fill = 1
   local octave_recipe = "all"
   local base_table = {}
   base_table = preparse_pitches(audio_freq_table,calculated_scale_degrees, key, scale_octave, octave_recipe, scale_recipe, scale_fill)
   pitch_table = quantize("pitch_table",base_table,number_of_notes_total)
end

do -- Velocity Table
   local velocity_recipe = "ff,pp"
   local base_table = {}
   base_table = preparse_velocities(velocity_recipe)
   velocity_table = quantize("velocity_table",base_table,number_of_notes_1)
end

do -- Duration Table
   local duration_recipe = "all"
   local base_table = {}
   base_table = preparse_durations(duration_recipe)
   duration_table = quantize("duration_table",base_table,number_of_notes_1)
end

require("scripts/prelude_I_Bach")
pitch_table_1,pitch_table_2,pitch_table_3,velocity_table_1,velocity_table_2,velocity_table_3,duration_table_1,duration_table_2,duration_table_3 = generate_prelude(generated_scale_degrees,pitch_table,velocity_table,duration_table,number_of_notes_1,number_of_notes_2,number_of_notes_3)

number_of_notes_1 = number_of_notes_1 + 3

for counter = 1,number_of_notes_1 do 
   frequency_table_1[counter] = audio_freq_table[pitch_table_1[counter]]
   frequency_table_2[counter] = audio_freq_table[pitch_table_2[counter]]
   frequency_table_3[counter] = audio_freq_table[pitch_table_3[counter]]

   if frequency_table_1[counter] ~= nil then 
      csound_frequency_table_1[counter] = frequency_table_1[counter] * (header.reference_pitch/440)
   end
   if frequency_table_2[counter] ~= nil then
      csound_frequency_table_2[counter] = frequency_table_2[counter] * (header.reference_pitch/440)
   end
   if frequency_table_3[counter] ~= nil then
      csound_frequency_table_3[counter] = frequency_table_3[counter] * (header.reference_pitch/440)
   end
   pan_table[counter] = .5
   ins_num_table[counter] = 1
   midi_ins_num_table[counter] = midi_ins_number
end

start_table_1 = generate_start_table(duration_table_1)
start_table_2 = generate_start_table(duration_table_2)
start_table_3 = generate_start_table(duration_table_3)

do -- Create Csound
   do -- Csound preamble
      csound_preamble = create_csound_preamble(algorithm,style,style_algorithm_author,time_of_creation)
      if sound_font ~= "" then
	 csound_preamble = csound_preamble .. "\nisf sfload  \"soundfonts/"..sound_font.."\" \nsfpassign   0, isf\n "
      end	     
   end

   local start_of_table = 1
   ins_num_table[1] = 1

   do -- create specific Orchestra instrument
      orchestra_instrument = csound_instrument ; instrument_num = 1 ; ins_name_table[instrument_num] = orchestra_instrument
      csound_orchestra1 = create_csound_orchestra(orchestra_instrument,instrument_num)
      csound_score_function1 = create_csound_score_function(orchestra_instrument,instrument_num)
   end

      csound_score_ins_1 = create_csound_score_ins(start_of_table, orchestra_instrument, number_of_notes_1, ins_num_table, ins_name_table, velocity_table_1, csound_frequency_table_1, duration_table_1, start_table_1, pan_table, midi_ins_num_table)

      csound_score_ins_2 = create_csound_score_ins(start_of_table, orchestra_instrument, number_of_notes_2, ins_num_table, ins_name_table, velocity_table_2, csound_frequency_table_2, duration_table_2, start_table_2, pan_table, midi_ins_num_table)
      
      csound_score_ins_3 = create_csound_score_ins(start_of_table, orchestra_instrument, number_of_notes_3, ins_num_table, ins_name_table, velocity_table_3, csound_frequency_table_3, duration_table_3, start_table_3, pan_table, midi_ins_num_table)

      csound_orchestra = csound_orchestra1

      csound_score_ins = csound_score_ins_1 .. csound_score_ins_2 .. csound_score_ins_3

      csound_score_ins = finish_csound_score_ins(csound_score_ins)
      
      csound_score = csound_score_function1 .. csound_score_function1 .. "\n" .. csound_score_ins

      local orchestra_finish = create_finish_orchestra(tempo_number)

      csound_file = csound_preamble .. csound_orchestra ..orchestra_finish .. csound_score
      csound_file_name = create_csound_file(csound_file,algorithm,style,instrument)
end

do -- Create Audio
   create_csound_audio_file(csound_file_name,style,style_algorithm_author,wave,flac,ogg,ogg_tag)
end

if generate_lilypond == "yes" then -- Generate Lilypond notation
   number_of_notes_1 = number_of_notes_1 -2 -- hack needed because we can't handle chords yet
   
   require("scripts/lilypond_creation") ; require("scripts/lilypond_creation_tables")
   local adapter = style_algorithm_author ; local pme = "Platonic Music Engine" ; local original_composer = "JS Bach"
   local algorithm = algorithm ; local original_title = "Praeludium I C-Major" 

   local time = "4/4" ; local voices = 1 ; local lyrics = {} ; local ragged_right = "no" ; local no_indent = "no"
   local rest_or_space = "s" ; local avoid_collisions = "no"
   header.note_scheme = "simple"
   -- header.note_scheme = "complex"

   local header = lilypond2header(instrument,tuning_name,adapter,pme,original_composer,original_title,additional_info,algorithm,header.reference_pitch_name)

   local lily_note_table_1,lily_note_table_2,lily_key = generate_lilypond_notes_from_frequencies(instrument,number_of_notes_1,tuning_name,key,calculated_scale_degrees,pitch_table_1,frequency_table_1,duration_table_1,velocity_table_1,rest_or_space,avoid_collisions)

   local lily_note_table_5,lily_note_table_6,lily_key = generate_lilypond_notes_from_frequencies(instrument,number_of_notes_2,tuning_name,key,calculated_scale_degrees,pitch_table_2,frequency_table_2,duration_table_2,velocity_table_2,rest_or_space,avoid_collisions)

   local lily_note_table_3,lily_note_table_4,lily_key = generate_lilypond_notes_from_frequencies(instrument,number_of_notes_3,tuning_name,key,calculated_scale_degrees,pitch_table_3,frequency_table_3,duration_table_3,velocity_table_3,rest_or_space,avoid_collisions)


   local lilypond_score,score_info = lilypond2variables(instrument,lily_key,time,tempo_word,tempo_number,lily_note_table_1,lily_note_table_2,lily_note_table_3,lily_note_table_4,lily_note_table_5,lily_note_table_6)
   local voices = 3
   local footer = lilypond2footer(score_info,instrument,voices,lyrics,remove_dynamics,no_indent)
   
   local complete_lilypond_score = header..lilypond_score..footer

   generate_lilypond_pdf(instrument,complete_lilypond_score,algorithm)
end      

if generate_feldman == "yes" then -- Feldman Graph Notation
   require("scripts/feldman_notation")
   local volume = "yes" 

   local feldman_score,latexname = midi2feldman(pitch_table_1,duration_table_1,velocity_table_1,instrument,number_of_notes_1,volume,tuning_name,system,tempo_number)
   create_latex(feldman_score,latexname)
end

if generate_pattern_15 == "yes" then -- Pattern 15 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_15") 
   
   pattern_15_lilypond_score_1,latexname = 
      midi2pattern_15(pitch_table_1,duration_table_1,instrument,number_of_notes_1)
   
   pattern_15_lilypond_score_2,latexname = 
      midi2pattern_15(pitch_table_2,duration_table_2,instrument,number_of_notes_2)

   pattern_15_lilypond_score = pattern_15_lilypond_score_1 --..  pattern_15_lilypond_score_2
   
   create_latex(pattern_15_lilypond_score,latexname)
   
end      

if generate_pattern_35 == "yes" then -- Pattern 35 Robert Kirkpatrick
   require("scripts/kirkpatrick/pattern_35")
   pattern_35_score,latexname = midi2pattern_35(pitch_table_3,duration_table_3,velocity_table_3,number_of_notes_3,instrument)
   
   create_latex(pattern_35_score,latexname)
end
